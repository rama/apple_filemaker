trigger LeadConvertTrigger on Lead (before insert, before update, after update) {

	if(Trigger.IsBefore) {
		//Set the currency of lead based on country code		
		for(Lead l : Trigger.new) {
			if(l.CountryCode == 'CA') {
				l.CurrencyIsoCode = 'CAD';
			}
			else if(l.CountryCode == 'BR') {
				l.CurrencyIsoCode = 'BRL';
			}
			else {
				l.CurrencyIsoCode = 'USD';
			}
		}
	}	
	
	//placeholder logic for enabling the user to continue with selection of an existing account that he/she does not have edit permission to
	/*Database.LeadConvert[] leadCollectionArray = new Database.LeadConvert[]{};
	for(Lead mylead : trigger.new){
	    if((myLead.isConverted == false) && (mylead.Ready_to_Convert__c == true)){
	        Database.LeadConvert lc = new database.LeadConvert();
	        Id accId = lc.getAccountId();
	        system.debug();
	        lc.setLeadId(mylead.Id);
	        lc.setConvertedStatus('Converted');
	        lc.setDoNotCreateOpportunity(true);
	        leadCollectionArray.add(lc);
	    }
	}
	Database.LeadConvertResult[] lcr = Database.convertLead(leadCollectionArray,false);*/
	
	//Populate Opportunity fields after lead convert
	if(Trigger.isAfter && Trigger.isUpdate) {
		Map<Id, Id> leadToContactIds = new Map<Id, Id>();
		Map<Id, Id> leadToAccountIds = new Map<Id, Id>();
		Map<Id, Id> leadToOpportunityIds = new Map<Id, Id>();
		for(Lead newLead : trigger.new){ 
			Lead oldLead = trigger.oldMap.get(newLead.Id);
			if (oldLead.isConverted == false && newLead.isConverted == true && newLead.ConvertedOpportunityId != null) {
		        // if a new contact was created
		        if (newLead.ConvertedContactId != null) {
		        	leadToContactIds.put(newLead.Id, newLead.ConvertedContactId);
		        }
		        // if a new account was created
		        if (newLead.ConvertedAccountId != null) {
		        	leadToAccountIds.put(newLead.Id, newLead.ConvertedAccountId);
		        }
		        // if a new opportunity was created
		        if (newLead.ConvertedOpportunityId != null) {
		        	leadToOpportunityIds.put(newLead.ConvertedOpportunityId, newLead.Id);
		        }
		    }
		}
		
		List<Account> accounts = [select Id, CreatedDate from Account where Id IN :leadToAccountIds.values()];
		Map<Id, Datetime> accountIdToCreatedDateMap = new Map<Id, Datetime>();
		for(Account acc : accounts) {
			accountIdToCreatedDateMap.put(acc.Id, acc.CreatedDate);
		} 
		
		List<Opportunity> opps = [Select Id, CreatedDate from Opportunity where Id IN :leadToOpportunityIds.keySet()];
		for(Opportunity opp : opps) {
 			Id leadId = leadToOpportunityIds.get(opp.Id);
 			if(leadId != null) {
 				Id contactId = leadToContactIds.get(leadId); 
 				if(contactId != null) {
					opp.Bill_To_Contact__c = contactId;
					opp.License_To_Contact__c = contactId;
					opp.Ship_To_Contact__c = contactId;
 				}
 				
 				Id accountId = leadToAccountIds.get(leadId);
 				Datetime accountCreatedDate = accountIdToCreatedDateMap.get(accountId);
 				if(accountCreatedDate == opp.CreatedDate) {
 					opp.Type = 'New';
 				} else {
 					opp.Type = 'Existing'; 
 				}
 			}
		}
	    update opps;
	}
	
}