trigger SalesOrderLineItemTrigger on Sales_Order_Line__c (after insert, after update) {
	
	Set<Id> oliIds = new Set<Id> ();	
	
	Map<Id,Id> solOliIdMap = new Map<Id,Id>();
	Map<Id, OpportunityLineItem> solOliMap = new Map<Id,OpportunityLineItem> ();
	
	for(Sales_Order_Line__c sl : Trigger.new) {
		boolean isActionNeeded = false;
		if(Trigger.isInsert) {
			//If its an insert, always consider this record to update corresponding oli.
			isActionNeeded = true;
		}
		else if(Trigger.isUpdate) {
			Sales_Order_Line__c oldso = trigger.oldMap.get(sl.Id);
			//if its an update, only consider this record if the opportunity line item Id field is changed
			if(sl.Opportunity_Line_Item_ID__c !=  oldso.Opportunity_Line_Item_ID__c) {
				isActionNeeded = true;
			}
		}
		if(isActionNeeded == true) {
			if(!string.isblank(sl.Opportunity_Line_Item_ID__c)) {
				oliIds.add(sl.Opportunity_Line_Item_ID__c);				
				solOliIdMap.put(sl.Id,sl.Opportunity_Line_Item_ID__c);
			}
		}
	}
	
	Map<Id,OpportunityLineItem> oliMap = new Map<Id,OpportunityLineItem>([SELECT Id, Sales_Order_Line__c from OpportunityLineItem where Id in : oliIds]);
		
	List<OpportunityLineItem> olis = new List<OpportunityLineItem>();
	//for each Sales order line, set the sales_order_line__c field on the corresponding opportunitylineitem
	for(Id slId : solOliIdMap.keyset()) {
		Id oliId = solOliIdMap.get(slId);
		if(oliId != null && oliMap.containsKey(oliId)) {
			OpportunityLineItem oli = oliMap.get(oliId);
			if(oli != null) {
				oli.Sales_Order_Line__c = slId;
				olis.add(oli);
			}
		}
	}
	
	
	//update all opportunity line items to reflect the right values in sales_Order_Line__c field
	update olis;
	
}