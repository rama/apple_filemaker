trigger Task on Task (after insert) {

	// ---------------------------------------------------------------------------------
	// Maintain First Activity Date on Lead Activities.
	// ---------------------------------------------------------------------------------

	if (Trigger.isInsert && Trigger.isAfter) {

		Set<Id> leadIds = new Set<Id>();

		for (Task task : Trigger.new) {
			if (task.WhoId != null && String.valueOf(task.WhoId).startsWith('00Q')) {
				leadIds.add(task.WhoId);
			}
		}
		
		List<Lead> leads = [select Id from Lead where Id in :leadIds and First_Activity_Date__c = null];
		
		for (Lead lead : leads) {
			lead.First_Activity_Date__c = Date.today();
		}

        update leads;
    }

}