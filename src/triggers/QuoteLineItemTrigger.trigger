trigger QuoteLineItemTrigger on QuoteLineItem (before insert, after insert, before update, after update, after delete) {

	// ---------------------------------------------------------------------------
	//  Calculate tax for line items.
	// ---------------------------------------------------------------------------
	
	// Set the Calculating Tax in the before trigger. 
	if ((Trigger.isInsert || Trigger.isUpdate) && Trigger.isBefore) {
		
		List<QuoteLineItem> quoteLineItemsToProcess = new List<QuoteLineItem>();
		Set<Id> quoteIds = new Set<Id>();
		
		for (QuoteLineItem quoteLineItem : Trigger.new) {
			// If new or the Sales Price changes.
			if (Trigger.isInsert || quoteLineItem.UnitPrice != Trigger.oldMap.get(quoteLineItem.Id).UnitPrice) {
				quoteLineItemsToProcess.add(quoteLineItem);
				quoteIds.add(quoteLineItem.QuoteId);
			}
		}

		// Only find Quotes that are for the US and are tax exempt.
		Map<Id, Quote> quotes = new Map<Id, Quote>([select Id from Quote where Id in :quoteIds and Tax_Exempt__c != true and ShippingCountryCode = 'US']);
		
		Set<Id> quoteLineItemIds = new Set<Id>();
		
		for (QuoteLineItem quoteLineItem : quoteLineItemsToProcess) {
			if (quotes.containsKey(quoteLineItem.QuoteId)) {
				quoteLineItem.Calculating_Tax__c = true;
			}
		}

	}
	
	if((Trigger.isInsert || Trigger.isUpdate) && Trigger.isAfter) {
		Set<Id> quoteIds = new Set<Id>();
		for(QuoteLineItem qli : Trigger.new) {
			//update quote's tax after insert or after delete or if the tax__c field is changed in the update
			if(Trigger.isInsert || Trigger.isDelete || (Trigger.isUpdate && qli.Tax__c != Trigger.oldMap.get(qli.Id).Tax__c)) {
				quoteIds.add(qli.QuoteId);
			}
		}
		
		//build a map of quotIds with their corresponding quotelineitems
		Map<Id,List<QuoteLineItem>> quoteToLineITemListMap = new Map<Id,List<QuoteLineItem>>();
		List<QuoteLineItem> qlis = [select Id, Tax__c, QuoteId from QuoteLineITem where QuoteId in :quoteIds];
		if(qlis != null && qlis.size() > 0) {
			for(QuoteLineItem qli : qlis) {
				if(qli.Tax__c != null) {
					if(quoteToLineItemListMap.containsKey(qli.QuoteId)) {
						List<QuoteLineItem> lineitems = quoteToLineItemListMap.get(qli.QuoteId);
						lineitems.add(qli);
						quoteToLineItemListMap.put(qli.QuoteId, lineitems);
					}
					else {
						List<QuoteLineItem> lineitems = new List<QuoteLineItem>();
						lineitems.add(qli);
						quoteToLineItemListMap.put(qli.QuoteId, lineitems);
					}
				}
			}
		}
		
		List<Quote> quotesToUpdate = new List<Quote>();
		if(quoteToLineItemListMap.size() > 0) {
			for(Id quoteId : quoteToLineITemListMap.keyset()) {
				List<QuoteLineItem> lineitems = quoteToLineItemListMap.get(quoteId);
				if(lineitems != null && lineitems.size() > 0) {
					decimal taxSum = 0.0;
					//compute the sum of tax on all quotelineitems for this quote
					for(QuoteLineItem qli : lineitems) {						
						if(qli.Tax__c != null) {
							taxSum += qli.Tax__c;
						}
					}
					if(taxSum > 0) {
						quotesToUpdate.add(new Quote(Id = quoteId, Tax = taxSum));
					}
				}
			}
		}
		if(quotesToUpdate.size() > 0) {
			update quotesToUpdate;
		}
		
	}
	
	// Invoke the tax service in the after trigger.
	if ((Trigger.isInsert || Trigger.isUpdate) && Trigger.isAfter) {
		
		List<QuoteLineItem> quoteLineItemsToProcess = new List<QuoteLineItem>();
		Set<Id> quoteIds = new Set<Id>();
		
		for (QuoteLineItem quoteLineItem : Trigger.new) {
			// If new or the Sales Price changes.
			if (Trigger.isInsert || quoteLineItem.UnitPrice != Trigger.oldMap.get(quoteLineItem.Id).UnitPrice) {
				quoteLineItemsToProcess.add(quoteLineItem);
				quoteIds.add(quoteLineItem.QuoteId);
			}
		}
		
		// Only find Quotes that are for the US and are tax exempt.
		Map<Id, Quote> quotes = new Map<Id, Quote>([select Id from Quote where Id in :quoteIds and Tax_Exempt__c != true and ShippingCountryCode = 'US']);
		
		Set<Id> quoteLineItemIds = new Set<Id>();
		
		for (QuoteLineItem quoteLineItem : quoteLineItemsToProcess) {
			if (quotes.containsKey(quoteLineItem.QuoteId)) {
				quoteLineItemIds.add(quoteLineItem.Id);
			}
		}
		
		if (quoteLineItemIds.size() > 0) {
			Util.getTaxes(quoteLineItemIds);
		}

	}



if ((Trigger.isInsert || Trigger.isUpdate) && Trigger.isBefore) {

  List<Id> qliIds = new List<Id>();
  for(QuoteLineItem qli : trigger.new) {
    qliIds.add(qli.Id);
  }

  Map<Id, List<QuoteLineItem>> quoteToQuoteLineItems = new Map<Id, List<QuoteLineItem>>();
  Map<Id, QuoteLineItem> fullQuoteLineItemMap = new Map<Id, QuoteLineItem>();
  
  List<QuoteLineItem> quoteLineItems = [Select Id, QuoteId, Quantity, MAS_Maintenance_Contract_Number__c, MAS_Contract_Number__c, Sales_Order_Line__r.Quantity__c, 
                                        MAS_Contract_End_Date__c, PricebookEntry.Product2.Description,  
										PricebookEntry.UnitPrice, PricebookEntry.Product2.Program_Code__c, PricebookEntry.Product2.Filemaker_External_ID__c, 
										PricebookEntry.Product2.Deferred_Revenue_Code__c, ListPrice, UnitPrice, TotalPrice, Prorate_Price__c, Product_Type__c,
										Package_Format__c, PC__c from QuoteLineItem where Id in :qliIds];

  List<Id> qIds = new List<Id>();

  for(QuoteLineItem qli : quoteLineItems) {
  	 qIds.add(qli.QuoteId);
  	 fullQuoteLineItemMap.put(qli.Id, qli);
     if(qli.MAS_Maintenance_Contract_Number__c  != null) {
       System.debug('qli.Quantity : ' + qli.Quantity);
       System.debug('qli.Sales_Order_Line__r.Quantity__c : ' + qli.Sales_Order_Line__r.Quantity__c);
       if(qli.Quantity > qli.Sales_Order_Line__r.Quantity__c) {
          trigger.newMap.get(qli.Id).addError('Quantity on Quote Line Item cannot be greater than Sales Order Line Quantity');
       } 
     } 
  }

  List<QuoteLineItem> allQuoteLineItems = [Select Id, QuoteId, PricebookEntry.Product2.ProductCode from QuoteLineItem where QuoteId in :qIds];

  for(QuoteLineItem qli : allQuoteLineItems) {
  	  List<QuoteLineItem> qlis = quoteToQuoteLineItems.get(qli.QuoteId);
  	  if(qlis == null) {
  	  	qlis = new List<QuoteLineItem>();
  	  }
  	  
      qlis.add(qli);
  	  quoteToQuoteLineItems.put(qli.QuoteId, qlis);
  }
  
  List<Id> quotesToTank = new List<Id>();
  for(Id qId : quoteToQuoteLineItems.keySet()) {
  	List<QuoteLineItem> qlis = quoteToQuoteLineItems.get(qId);
  	boolean foundDuplicate = false;
  	List<String> productCodes = new List<String>();
  	System.debug('qlis %%% : ' + qlis);
  	for(QuoteLineItem qli : qlis) {
  		String productCode = qli.PricebookEntry.Product2.ProductCode;
  		if(Util.contains(productCodes, productCode)) {
  			System.debug('productCodes : ' + productCodes + ' productCode : ' + productCode);
  		  	foundDuplicate = true;
  		  	break;
  		} 
  		
		productCodes.add(productCode);
	    System.debug('productCodes after adding : ' + productCodes); 
  	}
  	
  	if(foundDuplicate) {
  		System.debug('Found duplicate %%% ' + qId); 
  		quotesToTank.add(qId);
  	}
  }
  
  System.debug('quotesToTank $$$ : ' + quotesToTank);
  if(!quotesToTank.isEmpty()) {
  	List<Quote> quotesToModify = [select Id from Quote where Id IN : quotesToTank];
  	for(Quote aQuote : quotesToModify) {
  		aQuote.Send_To__c = 'TANK';
  	}
  	update quotesToModify;
  }
  
  //Discount trigger
  if(Trigger.isUpdate) {
	  for(QuoteLineItem qli : trigger.new) {
	  	QuoteLineItem fullQli = fullQuoteLineItemMap.get(qli.Id);

		if((fullQli.Prorate_Price__c == null && fullQli.UnitPrice != fullQli.ListPrice) ||
		    (fullQli.Prorate_Price__c != null && fullQli.UnitPrice != fullQli.Prorate_Price__c)) {

	        Integer programCode = -1000;
	       
	        try {
	           if(qli.PC__c != null) {
	            	programCode = Integer.valueOf(qli.PC__c.substring(1));
	           }
	        } catch(Exception ex) {
	           System.debug(ex.getMessage() + ' ' +  ex.getStackTraceString());
	        }
	
	
		  	qli.DPC__c = 'M' + Util.getDpc(qli, programCode, qli.UnitPrice);
		 }
	  }
  }
}


	if (Trigger.isDelete && Trigger.isAfter) {
		Set<Id> quoteIds = new Set<Id>();
		
		for (QuoteLineItem quoteLineItem : Trigger.old) {
			if (quoteLineItem.Prorate_Price__c != null ) {
				quoteIds.add(quoteLineItem.QuoteId);
			}
		}

		Map<Id, Quote> quotes = new Map<Id, Quote>([select Id from Quote where Id in :quoteIds]);
		Map<Id, Boolean> quoteToQLIFlag = new Map<Id, Boolean>();
		List<QuoteLineItem> qlisForQuotes = [select id, QuoteId from QuoteLineItem where QuoteId IN :quoteIds];
		for(QuoteLineItem qli : qlisForQuotes) {
			quoteToQLIFlag.put(qli.QuoteId, true);
		} 
		
		for(Id qId : quotes.keySet()) {
			if(quoteToQLIFlag.get(qId) == null) {
				quotes.get(qId).Prorated_Maintenance__c = false;
			}
		}
		
		update quotes.values();
	}  
}