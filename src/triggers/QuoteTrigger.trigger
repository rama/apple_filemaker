trigger QuoteTrigger on Quote (before insert, before update) {
  
  //Make sure that trigger is only fired once. Having issues because of this
  
  if(!Test.isRunningTest()) {
	  if(Util.QUOTE_TRIGGER_FIRED) {
	    return;
	  } else {
	    Util.QUOTE_TRIGGER_FIRED = true;    
	  }
  }
  
  System.debug('In quote trigger'); 

  //Create lits and Maps for Quote and Opportunities so we can use them later
  List<Id> quoteIds = new List<Id>();
  Map<Id, Quote> idToOriginalQuoteMap = new Map<Id, Quote>();
  List<Id> oppIds = new List<Id>();
  Map<Id, List<QuoteLineItem>> quoteToQuoteLineItems = new Map<Id, List<QuoteLineItem>>();
  List<Id> contactIds = new List<Id>();

  for(Quote q : trigger.new) {
    quoteIds.add(q.Id);
    idToOriginalQuoteMap.put(q.Id, q);
    oppIds.add(q.OpportunityId);
    contactIds.add(q.Bill_To_Contact__c);
  }
  System.debug('contactIds %%%: ' + contactIds);
  
  Map<Id, Contact> idToContactMap = new Map<Id, Contact>();

  List<Contact> contacts = [select Id, AccountId, MailingStateCode from Contact where Id IN :contactIds];
  for(Contact aContact : contacts) {
      idToContactMap.put(aContact.Id, aContact);
  }          
  
  System.debug('quoteIds ^^^ ' + quoteIds);
  //Get all QuoteLineItems
  List<QuoteLineItem> allQlis = [Select Id, QuoteId, MAS_Contract_End_Date__c, Contract_End_Date__c 
                                 from QuoteLineItem where QuoteId In :quoteIds];
  
  for(QuoteLineItem qli : allQlis) {
    if(!quoteToQuoteLineItems.containsKey(qli.Id)) {
      List<QuoteLineItem> qlis = new List<QuoteLineItem>();
      quoteToQuoteLineItems.put(qli.QuoteId, qlis);
    } else {
      List<QuoteLineItem> qlis = quoteToQuoteLineItems.get(qli.QuoteId);
      qlis.add(qli);
    }
  }
  
  //Requirement: Make sure the Quote Line Items are prorated
  if(trigger.isUpdate) {
      for(Quote q : trigger.new) {
        Quote oldQ = trigger.oldMap.get(q.Id);
        if(oldQ.Status != 'Quote Submitted' && q.Status == 'Quote Submitted') {
            
            List<QuoteLineItem> qlis = quoteToQuoteLineItems.get(q.Id);
            for(QuoteLineItem qli : qlis) {
              
              if(qli.MAS_Contract_End_Date__c != null && qli.Contract_End_Date__c != qli.MAS_Contract_End_Date__c) {
                qli.addError('Maintenance Quote Line Items on this Quote need to be prorated');
               }
            }
         }
       }
  }    
   
   //Get all Quotes with full data
   List<Quote> allQuotes = [Select Id, OpportunityId, eAccept_ID__c, Bill_To_Contact__c, Bill_To_Contact__r.AccountId, Bill_To_Contact__r.MailingStateCode, 
                            License_To_Contact__c, License_To_Contact__r.AccountId, Ship_To_Contact__c from Quote where Id IN : quoteIds];
   
   //Create lists and maps for Accounts, eAccept details
   List<Id> accountIds = new List<Id>();
   Map<Id, Quote> idToQuoteMap = new Map<Id, Quote>();
   Map<Id, Id> quoteIdToAccountIdMap = new Map<Id, Id>();

   List<String> eAcceptIds = new List<String>();
   Map<String, Id> quoteIdToAcceptIdMap = new Map<String, Id>();
   Map<Id, Boolean> eAcceptIdChanged = new Map<Id, Boolean>(); 

   for(Quote aQuote : allQuotes) {
        idToQuoteMap.put(aQuote.Id, aQuote);
   }
 
    //Identify if Bill to Contact or eAcceptId gets changes.
    //If changed record the information and keep it in Map so we can use it later
    if(trigger.isUpdate) {
       for(Quote fullQuote : trigger.new) {
            Quote oldQ = trigger.oldMap.get(fullQuote.Id);
            Quote completeQuote = idToQuoteMap.get(fullQuote.Id);
            Contact c = idToContactMap.get(fullQuote.Bill_To_Contact__c);          
		
            System.debug('fullQuote.Bill_To_Contact__c %%% : ' + fullQuote.Bill_To_Contact__c);
            System.debug('oldQ.Bill_To_Contact__c $$$ : ' + oldQ.Bill_To_Contact__c);
            
            if(oldQ.Bill_To_Contact__c != fullQuote.Bill_To_Contact__c)
            {
                    System.debug('Inside condition tax %%%');
                    System.debug('c.AccountId %%% ' + c.AccountId);
                    accountIds.add(c.AccountId);
                    quoteIdToAccountIdMap.put(c.AccountId, fullQuote.Id);
            }
    
            System.debug('fullQuote.eAccept_ID__c $$$ : ' + fullQuote.eAccept_ID__c);
            System.debug('oldQ.eAccept_ID__c $$$ : ' + oldQ.eAccept_ID__c);
            
            if(fullQuote.eAccept_ID__c != null && oldQ.eAccept_ID__c != fullQuote.eAccept_ID__c) 
            {
                    System.debug('Inside condition eAccept $$$');
                    eAcceptIds.add(fullQuote.eAccept_ID__c);
                    quoteIdToAcceptIdMap.put(fullQuote.eAccept_ID__c, fullQuote.Id);
                    eAcceptIdChanged.put(fullQuote.Id, true);
            }
       }
    } else if(trigger.isInsert) {
       for(Quote fullQuote : trigger.new) {
            Contact c = idToContactMap.get(fullQuote.Bill_To_Contact__c);          
            if(c != null && fullQuote.Bill_To_Contact__c != null) 
            {
                    System.debug('Inside condition tax %%%');
                    accountIds.add(c.AccountId);
                    System.debug('c.AccountId %%% ' + c.AccountId);
                    quoteIdToAccountIdMap.put(c.AccountId, fullQuote.Id);
            }
    
            System.debug('fullQuote.eAccept_ID__c $$$ : ' + fullQuote.eAccept_ID__c);
            
            if(fullQuote.eAccept_ID__c != null)
            {
                    System.debug('Inside condition eAccept $$$');
                    eAcceptIds.add(fullQuote.eAccept_ID__c);
                    quoteIdToAcceptIdMap.put(fullQuote.eAccept_ID__c, fullQuote.Id);
                    eAcceptIdChanged.put(fullQuote.Id, true);
            }
       }
    }
   
   //Get Tax exempt data and store it in Map
   System.debug('tax accountIds %%% : ' + accountIds);
   List<Tax_Exempt__c> taxExempts = [Select Id, Account__c, For_Multi_State_Use__c from Tax_Exempt__c where Account__c IN :accountIds];
   System.debug('taxExempts %%% : ' + taxExempts);
   Map<Id, Boolean> quoteIdToTaxExemptMap = new Map<Id, Boolean>();
   
   //Verify if Quote's Billing State Code is part of Tax exempt state and store the Boolean value
   for(Tax_Exempt__c taxExempt : taxExempts) {
     if(quoteIdToAccountIdMap.containsKey(taxExempt.Account__c)) {
        Id quoteId = quoteIdToAccountIdMap.get(taxExempt.Account__c);
        System.debug('taxExempt.For_Multi_State_Use__c %%% : ' + taxExempt.For_Multi_State_Use__c);
        String stateCode = idToContactMap.get(idToOriginalQuoteMap.get(quoteId).Bill_To_Contact__c).MailingStateCode;
        System.debug('stateCode %%% ' + stateCode);
        if(stateCode != null && taxExempt.For_Multi_State_Use__c.contains(stateCode)) {
            quoteIdToTaxExemptMap.put(quoteId, true);
        }
     } 
   }
   
   
   //Set the result if Tax exempt or not to the Quote
   for(Quote q : trigger.new) {
        if(quoteIdToTaxExemptMap != null && quoteIdToTaxExemptMap.get(q.Id) != null && quoteIdToTaxExemptMap.get(q.Id)) {
            System.debug('setting tax exempt to true %%% ');
            q.Tax_Exempt__c = true;
        } else {
            System.debug('setting tax exempt to false %%% ');
            q.Tax_Exempt__c = false;
        }       
   }
    
    System.debug('eAcceptIds $$$ : ' + eAcceptIds);
    //Get eAccept records based on eAccept Name in Quote
    List<Annual_eAccept__c> eAcceptObjects = [Select Id, Name from Annual_eAccept__c where Name IN :eAcceptIds];
    System.debug('eAcceptObjects $$$ : ' + eAcceptObjects);
    Map<Id, Annual_eAccept__c> quoteIdToAcceptMap = new Map<Id, Annual_eAccept__c>();
    Map<String, Id> eAcceptNameIdMap = new Map<String, Id>();
    for(Annual_eAccept__c eAcceptObject : eAcceptObjects) {
         if(quoteIdToAcceptIdMap.containsKey(eAcceptObject.Name)) {
            Id quoteId = quoteIdToAcceptIdMap.get(eAcceptObject.Name);
            quoteIdToAcceptMap.put(quoteId, eAcceptObject);
            eAcceptNameIdMap.put(eAcceptObject.Name, eAcceptObject.Id);
         } 
    }
    
    System.debug('quoteIdToAcceptMap.keySet() ^^^ : ' + quoteIdToAcceptMap.keySet());
    //Set eAccept id if it's already there
    //If not create a new eAccept record and stamp that id to Quote
    List<Annual_eAccept__c> eAcceptsToCreate = new List<Annual_eAccept__c>();
    for(Quote q : trigger.new) {
        if(Trigger.isUpdate) {
            if(eAcceptIdChanged.get(q.Id) != null && eAcceptIdChanged.get(q.Id)) {
                if(quoteIdToAcceptMap.get(q.Id) != null) {
                    q.eAccept__c = quoteIdToAcceptMap.get(q.Id).Id;
                    System.debug('Inside setting eAccept Id ^^^ ' + q.eAccept__c);
                } else {
                    if(q.eAccept_ID__c != null) {
                        Annual_eAccept__c eAccept = new Annual_eAccept__c();
                        eAccept.Name = q.eAccept_ID__c;
                        //eAccept.Filemaker_External_ID__c  = aQuote.eAccept_ID__c;
                        eAccept.Annual_Quote__c = q.Id;
                        System.debug('q.License_To_Contact__c: ' + q.License_To_Contact__c);
                        eAccept.Licensee_Contact__c = q.License_To_Contact__c;
                        
                        System.debug('q.License_To_Contact__r.AccountId: ' + q.License_To_Contact__r.AccountId);
                        eAccept.Licensee_Account__c = q.License_To_Contact__r.AccountId;
                        eAcceptsToCreate.add(eAccept);
                    }
                }
            }
        } else if(Trigger.isInsert) {
                if(q.eAccept_ID__c != null && eAcceptNameIdMap.get(q.eAccept_ID__c) != null) {
                    q.eAccept__c = eAcceptNameIdMap.get(q.eAccept_ID__c);
                    System.debug('Inside setting eAccept Id ^^^ ' + q.eAccept__c);
                } else {
                    if(q.eAccept_ID__c != null) {
                        Annual_eAccept__c eAccept = new Annual_eAccept__c();
                        eAccept.Name = q.eAccept_ID__c;
                        //eAccept.Filemaker_External_ID__c  = aQuote.eAccept_ID__c;
                        eAccept.Annual_Quote__c = q.Id;
                        System.debug('q.License_To_Contact__c: ' + q.License_To_Contact__c);
                        eAccept.Licensee_Contact__c = q.License_To_Contact__c;
                        
                        System.debug('q.License_To_Contact__r.AccountId: ' + q.License_To_Contact__r.AccountId);
                        eAccept.Licensee_Account__c = q.License_To_Contact__r.AccountId;
                        eAcceptsToCreate.add(eAccept);
                    }
                }
        }       
    }

    //Create eAccept records
    if(eAcceptsToCreate.size() > 0) {
        insert eAcceptsToCreate;
    }

    //Stamp the eAccept id to Quote
    for(Annual_eAccept__c eAccept : eAcceptsToCreate) {
        for(Quote q : trigger.new) {
            if(eAccept.Annual_Quote__c == q.Id) {
                //Save quote
                q.eAccept__c = eAccept.Id;
            }
        }
    }

    System.debug('oppIds *** ' + oppIds);
    //While inserting a new Quote get Bill to contact, License to contact and Ship to contact from Opportunity and stamp it to quote
    if(trigger.isInsert) {
       List<Opportunity> opps = [select Id, Bill_To_Contact__c, License_To_Contact__c, Ship_To_Contact__c from Opportunity where Id IN :oppIds];
       Map<Id, Opportunity> idToOppMap = new Map<Id, Opportunity>();
       for(Opportunity opp : opps) {
         idToOppMap.put(opp.Id, opp);
       }
       
       System.debug('opps *** ' + opps);
       
       for(Quote q : trigger.new) {
         
         Opportunity opp = idToOppMap.get(q.OpportunityId);
         System.debug('opp *** ' + opp);
         if(opp != null) {
             if(q.Bill_To_Contact__c == null) {
              q.Bill_To_Contact__c = opp.Bill_To_Contact__c;
             }
             
             if(q.License_To_Contact__c == null) {
              q.License_To_Contact__c = opp.License_To_Contact__c;
             }
             
             if(q.Ship_To_Contact__c == null) {
              q.Ship_To_Contact__c = opp.Ship_To_Contact__c;
             }
         }
      }    
    }
    
    System.debug('End of Trigger ###');
}