trigger SalesOrderTrigger on Sales_Order__c (before insert, before update, after insert, after update) {
	
	if(trigger.isBefore) {
		Set<Id>  quoteIds = new Set<Id>();
		//Build a list of Quote IDs from Sales order objects that are being  updated
		for(Sales_Order__c so : trigger.new) {
			quoteIds.add(so.Quote__c);
		}
		
		//Build a collection with quote Ids and their respective Quote objects  
		Map<Id, Quote> quoteOppMap =new Map<Id, Quote>([SELECT Id, OpportunityId FROM Quote where Id IN: quoteIds ]);
		
		//build a list opportunity ids from quotes
		Set<Id> oppIds = new Set<Id>();
		for(Id quoteId : quoteOppMap.keyset()) {
			Quote q = quoteOppMap.get(quoteId);
			if(q.OpportunityId != null) {
				oppIds.add(q.OpportunityId);
			}
		}
		
		Map<Id,Opportunity> oppsMap = new Map<Id,Opportunity>([SELECT Id, AccountId from Opportunity where Id in : oppIds]);
		
		//build a list of accountids based on opportunities
		Set<Id> accIds = new Set<Id>();
		for(Id oppId : oppsMap.keyset()) {
			Opportunity opp = oppsMap.get(oppId);
			if(opp.AccountId != null) {
				accIds.add(opp.AccountId);
			}
		}
		
		Map<Id,Account> accMap = new Map<Id,Account>([SELECT Id, District__c from Account where Id in : accIds ]);
		
		Map<Id, Account> oppAccountMap = new Map<Id,Account>();
				
		for(Id oppId :oppsMap.keyset()) {
			Opportunity opp = oppsMap.get(oppId);
			if(opp.AccountId != null && accMap.containsKey(opp.AccountId)) {
				oppAccountMap.put(oppId, accMap.get(opp.AccountId) );
			}			
		}
			
		//For each sales order, if the Quote field is being updated, set the opportunity field to correspond to quote's opportunity
		// and set the account as well.
		Map<Id,String> SOAndPricingType = new Map<Id,String>();
      	Set<string> pricingTypes = new Set<string>();
		for(Sales_Order__c so : Trigger.new) {
			Account corAccount; 
			boolean isActionNeeded = false;
			if(Trigger.isUpdate) {
				Sales_Order__c oldOrder = Trigger.oldMap.get(so.ID);
		        if (so.Quote__c != oldOrder.Quote__c) {
		        	isActionNeeded = true;	        	
		        }
			}
			else if(Trigger.isInsert) {
				isActionNeeded = true;
			}
			if(isActionNeeded == true) {
				Quote q = quoteOppMap.get(so.Quote__c);
				if(q!=null) {
					Id oppId = q.OpportunityId;
					so.Opportunity__c = oppId;
					if(!string.isBlank(oppId)) {					
						corAccount = oppAccountMap.get(oppId);
						if(corAccount != null) {		  			
			  				so.Account__c = corAccount.Id;  		
						}			
					}
				}			
			}
			//select a pricing type for the sales order based on channel, currency code and account details
			string pricingType;
			if(so.CurrencyIsoCode == 'BRL') {
				pricingType = 'Retail';				
			}
			else if(corAccount != null && corAccount.District__c == 'LATAM' && so.CurrencyIsoCode == 'USD') {
				pricingType= 'FBA';
			}
			else if(so.Channel__c == true && (so.CurrencyIsoCode == 'USD' || so.CurrencyIsoCode == 'CAD')) {
				pricingType = 'Distribution';
			} 
			else if(so.Channel__c == false && (so.CurrencyIsoCode == 'USD' || so.CurrencyIsoCode == 'CAD')) {
				pricingType = 'Retail';
			}
			if(!string.isblank(pricingType)) {
		    	SOAndPricingType.put(so.Id,pricingType);
		        pricingTypes.add(pricingType);
		    }			
		}
		//set sales orders pricebook based on currency and pricing type
		if(pricingTypes.size() > 0) {
			List<Pricebook2> pbs = [Select Id, Currency__c, Pricing_Type__c from Pricebook2 where Pricing_Type__c in :pricingTypes and IsActive = true];
	        if(pbs != null) {
		    	for(Sales_Order__c so : Trigger.new) {
		        	string soPricingType = SOAndPricingType.get(so.Id);
		           	if(!string.isblank(soPricingType)) {
			           	for(Pricebook2 pb : pbs) {
			           		if(pb.Pricing_Type__c == soPricingType && pb.Currency__c == so.CurrencyIsoCode) {
			           			so.Price_Book__c = pb.Id;	           			
			           			break;
			           		}
			           	}
		           	}
		           	if(so.Price_Book__c == null) {
		           		system.debug('no matching pricebook entries for sales order ' + so.Id);
		           	}
		    	}
	        }
	        else {
	        	system.debug('There are no matching pricebook entries');
	        } 
		}         
	}
	if(trigger.isAfter) {
		
		Set<Id> oppIds = new Set<Id>();
		if(Trigger.IsUpdate) {
			system.debug('its a sales order update ');
			for(Sales_Order__c so: trigger.new) {
				if(so.Opportunity__c != null && (so.Opportunity__c != Trigger.OldMap.get(so.Id).Opportunity__c) ) {
					oppIds.add(so.Opportunity__c);
				}
			}
		}
		else if(Trigger.IsInsert) {
			system.debug('its a sales order insert');
			for(Sales_Order__c so : trigger.new) {
				if(so.Opportunity__c != null) {
					oppIds.add(so.Opportunity__c);
				}
			}
		}
		
		Map<String,String> mOppIdToQuoteId = new Map<String,String>();
		for(Id oppId : oppIds) {
			mOppIdToQuoteId.put(oppId,null);
		}
		
		//turn syncing off on oppoertunities of quotes related to this sales order.
		// This has to be implemented as a future method call as the SyncedQuoteId field on opportunity is read only when inside a trigger
		if(mOppIdToQuoteId.size() > 0) {
			system.debug('looks like there are a dew oppids in here');
			UpdateQuoteSync.syncQuotes(mOppIdToQuoteId);
		}
	}
}