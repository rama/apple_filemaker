trigger AnnualeAcceptTrigger on Annual_eAccept__c (before insert, before update) {

  List<Id> contactIds = new List<Id>();
  
  for(Annual_eAccept__c eAccept : trigger.new) {
   if(trigger.isUpdate) {	
	   Annual_eAccept__c oldAccept = trigger.oldMap.get(eAccept.Id);
	   if(eAccept.Licensee_Contact__c != null && oldAccept.Licensee_Contact__c != eAccept.Licensee_Contact__c) {
	     contactIds.add(eAccept.Licensee_Contact__c);
	   }
   } else if(trigger.isInsert) {
   	   if(eAccept.Licensee_Contact__c != null) {
	     contactIds.add(eAccept.Licensee_Contact__c);
	   }
   }
  }
  
  List<Contact> contacts = [select Id, AccountId from Contact where Id IN :contactIds];
  Map<Id, Contact> idToContactMap = new Map<Id, Contact>();
  for(Contact aContact : contacts) {
   idToContactMap.put(aContact.Id, aContact);
  }
  
  for(Annual_eAccept__c eAccept : trigger.new) {
   if(trigger.isUpdate) {	
	   Annual_eAccept__c oldAccept = trigger.oldMap.get(eAccept.Id);
	   if(eAccept.Licensee_Contact__c != null && oldAccept.Licensee_Contact__c != eAccept.Licensee_Contact__c) {
	        eAccept.Licensee_Account__c = idToContactMap.get(eAccept.Licensee_Contact__c).AccountId;
	   }
   } else  if(trigger.isInsert) {
   	   if(eAccept.Licensee_Contact__c != null) {
	     eAccept.Licensee_Account__c = idToContactMap.get(eAccept.Licensee_Contact__c).AccountId;
	   }
   }
  }
  
}