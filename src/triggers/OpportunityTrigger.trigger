trigger OpportunityTrigger on Opportunity (before insert, before update, after update) {

	// --------------------------------------------------------------------------------
	// Maintain Account Group on the Opportunity.
	// --------------------------------------------------------------------------------

	if ((Trigger.isInsert || Trigger.isUpdate) && Trigger.IsBefore) {
		
		Set<Id> accountIds = new Set<Id>();
		
		for (Opportunity opportunity : Trigger.new) {
			if (opportunity.AccountId != null) {
				accountIds.add(opportunity.AccountId);
			}
		}

		Map<Id, Account> accounts = new Map<Id, Account>([select Id, Group__c from Account where Id in :accountIds]);
		
		for (Opportunity opportunity : Trigger.new) {
			if (opportunity.AccountId != null) {
				opportunity.Group__c = accounts.get(opportunity.AccountId).Group__c;
			} else {
				// In case this is an update where an Account is being removed from the Opportunity.
				opportunity.Group__c = null;
			}
		}
		
	}



	// --------------------------------------------------------------------------------
	// Maintain Account on Sales Order when it changes on the Opportunity.
	// --------------------------------------------------------------------------------

	if (Trigger.isUpdate && Trigger.IsAfter) {

		List<Sales_Order__c> salesOrders = new List<Sales_Order__c>();

		for (Opportunity opportunity: Trigger.new) {
			if (opportunity.Sales_Order__c != null && opportunity.AccountId != Trigger.oldMap.get(opportunity.Id).AccountId) {
				salesOrders.add(new Sales_Order__c(Id=opportunity.Sales_Order__c, Account__c=opportunity.AccountId));
			}
		}

		update salesOrders;
	}


 	if(Trigger.IsBefore && Trigger.IsUpdate) {
  
  		Set<Id> accountIds = new Set<Id>();
  
  		for (Opportunity opportunity : Trigger.new) {
  			if (opportunity.AccountId != null) {
  				accountIds.add(opportunity.AccountId);
  			}
  		}
  
  		Map<Id, Account> accounts = new Map<Id, Account>([select Id, District__c from Account where Id in :accountIds]);
  
       	Set<Id> oppIds = trigger.NewMap.keyset();      
    
	 	List<Opportunity> opps = [select Id, Account.Group_Code__c, Account.FBA_Status__c, Account.Partner_Renewal_Date__c from Opportunity where Id IN :oppIds];
	    
	    Map<Id, Opportunity> fullOpps = new Map<Id, Opportunity>();
	    Map<Id,Account> oppToAccountMap = new Map<Id,Account>();
	    for(Opportunity opp : opps) {
	    	fullOpps.put(opp.Id, opp);	    	
	        oppToAccountMap.put(opp.Id,accounts.get(opp.AccountId));
	    }
    
      	List<OpportunityPartner> partners = [select id, AccountToId, OpportunityId, Role, IsPrimary from OpportunityPartner
                                          where OpportunityId IN : oppIds];
      
      	List<Id> partnerAccountIds = new List<Id>();
      	Map<Id, List<OpportunityPartner>> oppToOppPartnersMap = new Map<Id, List<OpportunityPartner>>();
      	for(OpportunityPartner partner : partners) {
	        partnerAccountIds.add(partner.AccountToId);
	        List<OpportunityPartner> partnersForOpp = oppToOppPartnersMap.get(partner.OpportunityId);
	        if(partnersForOpp == null) {
	          partnersForOpp = new List<OpportunityPartner>();
	        }
        
	        partnersForOpp.add(partner);
	        oppToOppPartnersMap.put(partner.OpportunityId, partnersForOpp);
      	}
      
	 	List<Account> partnerAccounts = [select Id, FBA_Status__c, Partner_Renewal_Date__c from Account where Id IN :partnerAccountIds];
	    Map<Id, Account> idToPartnerAccountsMap = new Map<Id, Account>();
	    for(Account partnerAccount : partnerAccounts) {
	    	idToPartnerAccountsMap.put(partnerAccount.Id, partnerAccount);
	    }
      
      	Map<Id,String> oppAndPricingType = new Map<Id,String>();
      	Set<string> pricingTypes = new Set<string>();
      	//for each opportunity, determine the pricing type based on currencycode and account fields
      	for(Opportunity opp : trigger.new) {
	        string pricingType;
	
	        Account acc = oppToAccountMap.get(opp.Id);
	        if(acc != null) {
		        if(opp.CurrencyIsoCode == 'BRL') {
		            pricingType = 'Retail';               
		        }       
		        else if(acc.District__c == 'LATAM' && opp.CurrencyIsoCode == 'USD') {
		            pricingType= 'FBA';
		        }
		        else if(opp.Channel__c == true || (opp.FBA_Valid__c == True && opp.FBA_Pricing__c == True)) {
		            pricingType = 'Distribution';
		        } 
		        else if(opp.Channel__c == false && (opp.CurrencyIsoCode == 'USD' || opp.CurrencyIsoCode == 'CAD')) {
		            pricingType = 'Retail';
		        }
		        if(!string.isblank(pricingType)) {
		        	oppAndPricingType.put(opp.Id,pricingType);
		        	pricingTypes.add(pricingType);
		        }
	        }
	        else {
	        	system.debug('account for opportunity ' + opp.Id + 'does not exist in opptoaccountmap');
	        }
        
	        Opportunity fullOpp = fullOpps.get(opp.Id);
	        System.debug('fullOpp.Account.Group_Code__c ### ' + fullOpp.Account.Group_Code__c);
	        System.debug('fullOpp.Account.FBA_Status__c ### ' + fullOpp.Account.FBA_Status__c);
	        System.debug('fullOpp.Account.Partner_Renewal_Date__c ### ' + fullOpp.Account.Partner_Renewal_Date__c);
	        //First make it false
	        opp.FBA_Valid__c = false;
	        
	        //See if it can be true based on conditions
	        if(fullOpp.Account.Group_Code__c == 'FSAX' && 
	           fullOpp.Account.FBA_Status__c == 'Active' &&
	           fullOpp.Account.Partner_Renewal_Date__c > System.today()) {
	           	opp.FBA_Valid__c = true;
	           	continue;
	         }
	           
           OpportunityPartner partnerToProcess = null;
           List<OpportunityPartner> partnersForOpp = oppToOppPartnersMap.get(opp.Id);   
           if(partnersForOpp != null) {
               if(partnersForOpp.size() > 1) {
                 for(OpportunityPartner partner : partnersForOpp) {
                   if(partner.IsPrimary) {
                     partnerToProcess = partner;
                     break;
                   }
                 }
               } else if(partnersForOpp.size() == 1) {
                  partnerToProcess = partnersForOpp.get(0);
               }
           }
       
           if(partnerToProcess == null) {
             continue;
           }
       
     	   System.debug('partnerToProcess ### ' + partnerToProcess);
       
           Account partnerAccount = idToPartnerAccountsMap.get(partnerToProcess.AccountToId);
           System.debug('partnerAccount ### ' + partnerAccount );       
           if(partnerAccount != null && 
              partnerAccount.FBA_Status__c == 'Active' &&
              partnerAccount.Partner_Renewal_Date__c > System.today()) {
              System.debug('setting opp FBA_Valid__c to true');
              opp.FBA_Valid__c = true;
           }           
        }              

      
      	//Update opportunity's pricebook based on currency and pricing type
      	List<Pricebook2> pbs = [Select Id, Currency__c, Pricing_Type__c from Pricebook2 where Pricing_Type__c in :pricingTypes and IsActive = true];
        if(pbs != null) {
	    	for(Opportunity opp : Trigger.new) {
	        	string oppsPricingType = oppAndPricingType.get(opp.Id);
	           	for(Pricebook2 pb : pbs) {
	           		if(pb.Pricing_Type__c == oppsPricingType && pb.Currency__c == opp.CurrencyIsoCode) {
	           			opp.Pricebook2Id = pb.Id;
	           			system.debug('setting opportunity with Id ' + opp.Id + ' pricebook id to ' + pb.Id);
	           			break;
	           		}
	           	}
	           	if(opp.Pricebook2Id == null) {
	           		system.debug('no matching pricebook entries for opportunity ' + opp.Id);
	           	}
	    	}
        }
        else {
        	system.debug('There are no matching pricebook entries');
        }              
   }   
}