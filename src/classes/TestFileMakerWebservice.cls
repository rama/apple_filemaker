public class TestFileMakerWebservice{

    public static void testWebService() {
     FileMakerEstimatedTaxService.FMI_ESTIMATED_TAX service = new FileMakerEstimatedTaxService.FMI_ESTIMATED_TAX();
     service.timeout_x = 120000;
    
     service.inputHttpHeaders_x = new Map<String, String>();
        
     String header = '<wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" soapenv:mustUnderstand="1">      <wsu:Timestamp wsu:Id="TS-2">        <wsu:Created>2013-10-13T19:19:49.594Z</wsu:Created>        <wsu:Expires>2013-10-13T19:19:59.594Z</wsu:Expires>      </wsu:Timestamp>      <wsse:UsernameToken wsu:Id="UsernameToken-1">        <wsse:Username>apache</wsse:Username>        <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">password</wsse:Password>      </wsse:UsernameToken>    </wsse:Security> ';
        
     //service.inputHttpHeaders_x.put('Header', header);
     
       
     FileMakerEstimatedTaxService.ESTIMATED_TAX_IN taxIn = new FileMakerEstimatedTaxService.ESTIMATED_TAX_IN();
     
        
     FileMakerEstimatedTaxService.TABLE_OF_ESTIMATED_TAX_IN input = new FileMakerEstimatedTaxService.TABLE_OF_ESTIMATED_TAX_IN();
     input.item = new FileMakerEstimatedTaxService.ESTIMATED_TAX_IN[] { taxIn };
        
     System.debug(service.FMI_GET_EXTIMATED_TAX(input));
        
    }
    
}