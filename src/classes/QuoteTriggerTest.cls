@isTest (SeeAllData=true)
private class QuoteTriggerTest {

    @isTest
    static void setupData() {
        Account account = new Account();
        account.Industry = 'Test industry';
        account.Name = 'Test';
        insert account;
        
       Contact ct = new Contact();
       ct.AccountId = account.Id;
       ct.FirstName = 'Test';
       ct.LastName = 'Contact';
       insert ct;        
        
        Pricebook2 standardPB = [select id from Pricebook2 where isStandard=true];
        
        PriceBook2 pb = new PriceBook2();
        pb.Name = 'test pricebook';
        insert pb;
        
        Product2 prod = new Product2();
        prod.Name = 'test product';
        prod.filemaker_external_id__c = 'test external id';
        prod.CurrencyIsoCode = 'USD';  
        prod.ProductCode = 'ABC';
        insert prod;
        

        PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = standardPB.Id, Product2Id = prod.Id, UnitPrice = 10000, IsActive = true, UseStandardPrice = false);
        insert standardPrice;
        
        PricebookEntry pbe = new PricebookEntry();
        pbe.Product2Id = prod.Id;
        pbe.Pricebook2Id = pb.Id;
        pbe.UnitPrice = 100;
        pbe.UseStandardPrice = false;
        pbe.IsActive=true;
        insert pbe;
                        
        Opportunity opportunity = new Opportunity();
        opportunity.AccountId = account.Id;
        opportunity.Name = 'TestOpp';
        opportunity.StageName = 'Qualification';
        opportunity.CloseDate = Date.today();
        insert opportunity;

        Annual_eAccept__c eAccept = new Annual_eAccept__c();
        eAccept.Name = 'Test eAccept';
        eAccept.Licensee_Contact__c = ct.Id;
        eAccept.Licensee_Account__c = account.Id;
        insert eAccept;

        Quote q = new Quote();
        q.Name = 'TestQuote';
        q.OpportunityId = opportunity.Id;
        q.Pricebook2Id = pb.Id;
        q.License_To_Contact__c = ct.Id;
        q.eAccept__c = eAccept.Id;
        insert q;

        Annual_eAccept__c eAccept2 = new Annual_eAccept__c();
        eAccept2.Name = 'Test eAccept2';
        eAccept2.Licensee_Contact__c = ct.Id;
        eAccept2.Licensee_Account__c = account.Id;
        insert eAccept2;

        q.eAccept__c = eAccept2.Id;
        update q;
        
        List<QuoteLineItem> qlis = new List<QuoteLineItem>(); 
        
        QuoteLineItem qli = new QuoteLineItem();
        qli.QuoteId = q.Id;
        qli.Quantity = 5;
        qli.PricebookEntryId = pbe.Id;
        qli.UnitPrice = 50;
        qli.Tax__c = 5;
        qlis.add(qli);
        
        QuoteLineItem qli1 = new QuoteLineItem();
        qli1.QuoteId = q.Id;
        qli1.Quantity = 3;
        qli1.PricebookEntryId = pbe.Id;
        qli1.UnitPrice = 100;
        qli1.Tax__c = 10;
        qlis.add(qli1);
        
        insert qlis;
    }
    
    
    @isTest
    static void setupUpdateQuoteData() {
        Account account = new Account();
        account.Industry = 'Test industry';
        account.Name = 'Test';
        insert account;
        
       Contact ct = new Contact();
       ct.AccountId = account.Id;
       ct.FirstName = 'Test';
       ct.LastName = 'Contact';
       insert ct;        
        
        Pricebook2 standardPB = [select id from Pricebook2 where isStandard=true];
        
        PriceBook2 pb = new PriceBook2();
        pb.Name = 'test pricebook';
        insert pb;
        
        Product2 prod = new Product2();
        prod.Name = 'test product';
        prod.filemaker_external_id__c = 'test external id';
        prod.CurrencyIsoCode = 'USD';  
        prod.ProductCode = 'ABC';
        insert prod;
        

        PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = standardPB.Id, Product2Id = prod.Id, UnitPrice = 10000, IsActive = true, UseStandardPrice = false);
        insert standardPrice;
        
        PricebookEntry pbe = new PricebookEntry();
        pbe.Product2Id = prod.Id;
        pbe.Pricebook2Id = pb.Id;
        pbe.UnitPrice = 100;
        pbe.UseStandardPrice = false;
        pbe.IsActive=true;
        insert pbe;
                        
        Opportunity opportunity = new Opportunity();
        opportunity.AccountId = account.Id;
        opportunity.Name = 'TestOpp';
        opportunity.StageName = 'Qualification';
        opportunity.CloseDate = Date.today();
        insert opportunity;
        
        Quote q = new Quote();
        q.Name = 'TestQuote';
        q.OpportunityId = opportunity.Id;
        q.Pricebook2Id = pb.Id;
        insert q;
        
        List<QuoteLineItem> qlis = new List<QuoteLineItem>(); 
        
        QuoteLineItem qli = new QuoteLineItem();
        qli.QuoteId = q.Id;
        qli.Quantity = 5;
        qli.PricebookEntryId = pbe.Id;
        qli.UnitPrice = 50;
        qli.Tax__c = 5;
        qlis.add(qli);
        
        QuoteLineItem qli1 = new QuoteLineItem();
        qli1.QuoteId = q.Id;
        qli1.Quantity = 3;
        qli1.PricebookEntryId = pbe.Id;
        qli1.UnitPrice = 100;
        qli1.Tax__c = 10;
        qlis.add(qli1);
        
        insert qlis;
        
        Annual_eAccept__c eAccept = new Annual_eAccept__c();
        eAccept.Name = 'Test eAccept';
        eAccept.Licensee_Contact__c = ct.Id;
        eAccept.Licensee_Account__c = account.Id;
        insert eAccept;
        
        
        System.debug('In test class quote id: ' + q.Id);
        List<QuoteLineItem> quotelineitemsRetrieved = [select Id, QuoteId, MAS_Contract_End_Date__c, Contract_End_Date__c 
                                 from QuoteLineItem where QuoteId = :q.Id];
        
        System.debug('In test class quote line items : ' + quotelineitemsRetrieved);

		q.License_To_Contact__c = ct.Id;        
        q.Status = 'Exception Authorized';
        q.eAccept_ID__c = 'Test eAccept';
        update q;
            
        
    }    
}