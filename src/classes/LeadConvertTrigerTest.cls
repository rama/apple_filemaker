@isTest
private class LeadConvertTrigerTest {

    static testMethod void myUnitTest() {
    	
       Account a = new Account();
       a.Name = 'Test Account';
       a.Industry = 'Test';
       insert a;

       Contact ct = new Contact();
       ct.AccountId = a.Id;
       ct.FirstName = 'Test';
       ct.LastName = 'Contact';
       insert ct;
       
       System.assertNotEquals(a.Id, null);
     
       Opportunity o = new Opportunity();
       o.Name = 'Test Opp';
       o.AccountId = a.Id;
       o.StageName = 'Purchase';
       o.CloseDate = System.today();
     
       insert o;
    
       System.assertNotEquals(o.Id, null);
           	
        Lead l = new Lead();
        l.LastName = 'who cares';
        l.Company = 'Dream company';

        l.Status = 'Open - Lead Untouched';
        l.CountryCode = 'CA';
        l.Group__c = 'NDIR Non Profit';
        
        insert l;
        
        Lead queriedLead = [select Id , CurrencyIsoCode from Lead where Id =: l.Id];
        system.assertEquals('CAD', queriedlead.CurrencyIsoCode, 'Currency must match to CAD');
        
        
        l.CountryCode = 'BR';
        update l;
        
		Database.LeadConvert lc = new database.LeadConvert();
		lc.setLeadId(l.id);
		lc.setAccountId(a.Id);
		lc.setOpportunityName(o.Name);
		lc.setContactId(ct.Id);
		
		LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
		lc.setConvertedStatus(convertStatus.MasterLabel);
		
        Database.convertLead(lc);
        
        Lead requeriedLead = [select Id, CurrencyIsoCode from Lead where Id =: l.Id];
        system.assertEquals('BRL',requeriedLead.CurrencyIsoCode );
    }
}