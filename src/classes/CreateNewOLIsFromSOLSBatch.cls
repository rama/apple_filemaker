global class CreateNewOLIsFromSOLSBatch implements Database.Batchable<Sobject> {

	private boolean isTestRunning = false;
	
	public CreateNewOLIsFromSOLSBatch() {
		this(false);
	}
	
	public CreateNewOLIsFromSOLSBatch(boolean isTest) {
		IsTestRunning = isTest;
	}
	
	
	global Database.QueryLocator start(Database.BatchableContext BC){     
     	string query = 'SELECT Id, Opportunity__c, Price_Book__c FROM Sales_Order__c WHERE Status__c != \'Reconciled\' and Order_Type__c != \'MS\' and Opportunity__c != null and Price_Book__c != null';
     	return Database.getQueryLocator(query);
   	}
   	
   	global void execute(Database.BatchableContext BC, List<Sales_Order__c> scope){
		system.debug('second batch is started');
		system.debug('scope size is:' + scope.size());
		autoReconcile(scope);
	}
   	
   	global void finish(Database.BatchableContext BC){
   		
   	}
   	
   	public void autoReconcile(List<Sales_Order__c> salesOrders) {
   		if(salesOrders != null) {
   			system.debug('sales orders that need to be reconciled'+ salesorders.size());  
   			Map<Id,Opportunity> soIdToOpportunityMap = getRelatedOpportunities(salesOrders); 			
   			system.debug('Related opportunities are retrieved');
   			List<Id> oppIdsWithChangedPbs = new List<Id>(); 
   			
   			Map<Id,Sales_Order__c> oppsTobeUpdatedWithSO = new Map<Id, Sales_Order__c>();
   			List<Opportunity> oppstoUpdate = new List<Opportunity>();
   			Map<Id,List<Sales_Order_Line__c>> solsToCreateNewOlis = new Map<Id,List<Sales_Order_Line__c>>();
   			   			
   			for(Sales_Order__c so : salesOrders) {   				
   				Opportunity opp = soIdToOpportunityMap.get(so.Id);
   				List<OpportunityLineItem> olis = [select Id from OpportunityLineItem where OpportunityId =: opp.Id];
   				List<Sales_Order_Line__c> sols = [Select Id from Sales_Order_Line__c where Sales_Order__c =:so.Id ];
   				//Verify if the pricebook has changed external to salesforce
   				if(so.Price_Book__c == opp.Pricebook2Id && !(olis.size() == 0 && sols.size() > 0) ) {
   					system.debug('no price book change for this so:' + so.Id);
   					//As the price book hasn't changed, no specific action is necessary and we can proceed to comparing line items
   				}
   				else {
   					//As the pricebook has changed external to salesforce, delete all opplineitems on the opp, update the pricebook and recreate olis from sols
   					oppIdsWithChangedPbs.add(opp.Id);
   					oppsTobeUpdatedWithSO.put(opp.Id, so);
   					oppstoUpdate.add(opp);
   				}
   			}
   			
   			system.debug('number of opps to update as a result of external change:' + oppstoUpdate.size());
   			if(oppIdsWithChangedPbs.size() > 0) {   				
   				   				
   				//Update the pricebook on the opportunities to match that of corresponding sales orders
   				for(Opportunity opp: oppstoUpdate) {
   					Sales_Order__c so = oppsToBeUpdatedWithSO.get(opp.Id);
   					if(so != null) {
   						List<Sales_Order_Line__c> sols = [Select ID, Sales_Order__c, filemaker_external_id__c, Product_ID_SKU__c, Quantity__c, Sales_Price__c from Sales_Order_Line__c where Sales_Order__c =: so.Id];
   						solsToCreateNewOlis.put(so.Id,sols);
   					}
   				}
   				
   				//Create new OLIs from SOLs
   				List<Sales_Order_Line__c> solsToBeUpdated = new List<Sales_Order_Line__c>();
   				for(Id oppId: oppsTobeUpdatedWithSO.keyset()) {   					
   					Sales_Order__c so = oppsTobeUpdatedWithSO.get(oppId);
   					if(so != null) {
   						Opportunity queriedopp = [select Id,Pricebook2Id from Opportunity where Id=: oppId limit 1];
   						system.debug('queriedopp pricebook is:' + queriedopp.Pricebook2Id);
   						List<Sales_Order_Line__c> sols = solsToCreateNewOlis.get(so.Id);   						
   						if(sols != null) {
   							system.debug('sales order with id ' + so.Id + 'has ' + sols.size() + ' sols to create olis from ');
   							for(Sales_Order_Line__c sol : sols) {
   								if(sol.Quantity__c != null) {
   									OpportunityLineItem oli = new OpportunityLineItem();
	   								Id pbeId = [select Id from PricebookEntry where Product2Id =:sol.Product_ID_SKU__c and Pricebook2Id =:so.Price_Book__c and isactive = true limit 1].Id; 
	   								oli.PricebookEntryId = pbeId;
	   								system.debug('olis price book entry is in price book' + so.Price_Book__c);
	   								
	   								system.debug('opps price book id is:' + queriedOpp.Pricebook2Id);
	   								oli.Quantity = sol.Quantity__c;
	   								oli.UnitPrice = sol.Sales_Price__c; 
	   								oli.OpportunityId = queriedopp.Id;
	   								oli.filemaker_external_id__c = sol.filemaker_external_id__c; 
	   								oli.Sales_Order_Line__c = sol.Id;	   									   								
	   								insert oli;
	   								system.debug('new oli inserted:' + oli.Id);
	   								sol.Opportunity_Line_Item_ID__c = oli.Id;
	   								solsToBeUpdated.add(sol);   
   								}
   																
   							}
   						}
   						else { system.debug('for so: ' + so.Id + 'no sols to create olis from'); }
   					}
   				}

				//update opportunity line item field on SOLs to correspond to newly inserted OLIs. 
   				if(solsToBeUpdated != null) {
   					update solsToBeUpdated;	
   				}   							
   			}   			
   		}
   		else { system.debug('sales orders collection is null'); }
   	} 
   	
   	 	
   	public Map<Id, Opportunity> getRelatedOpportunities(List<Sales_Order__c> sos) {
   		Map<Id,Opportunity> soIdToOpportunityMap = new Map<Id, Opportunity>();
   		for(Sales_Order__c so: sos) {
   			if(so.Opportunity__c != null) {
   				soIdToOpportunityMap.put(so.Id, [select Id,Pricebook2Id from Opportunity where Id =: so.Opportunity__c limit 1]);
   			}
   		}
   		return soIdToOpportunityMap;
   	}
   	
}