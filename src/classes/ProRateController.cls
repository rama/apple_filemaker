public with sharing class ProRateController {

    public Boolean errorPanel { get; set; }
    
    public String errorMessage { get; set; }

    public List<Period> periods {
        get {
            if(periods == null) {
                periods = new List<Period>();
            }
            
            return periods;
        }
        set;
    }

    public List<FiscalYearSettings> fySettings {
        get {
            if(fySettings == null) {
                fySettings = new List<FiscalYearSettings>();
            }
            
            return fySettings;
        }
        set;
    }
    
    
    
    public Quote quote {
     get;
     set;
    }

    public String accountName { get; set; }

    public Id accountId { get; set; }

    public String notes { get; set; }

    public List<QuoteLineItemWrapper> quoteLineItems {
      get {
       if(quoteLineItems == null) {
         quoteLineItems = new List<QuoteLineItemWrapper>();
       }
       
       return quoteLineItems;
      }
      set;
    } 
    
    public Map<String, Date> contractEndDateMap = new Map<String, Date>(); 
 
    public List<QuoteLineItemWrapper> activeQuoteLineItems {
      get {
       activeQuoteLineItems = new List<QuoteLineItemWrapper>();
       
       for(QuoteLineItemWrapper quoteLineItem : quoteLineItems) {
         if(quoteLineItem.deleted) {
           continue;
         }
         activeQuoteLineItems.add(quoteLineItem);
       }       
       return activeQuoteLineItems;
      }
      set;
    } 
    
    public String selectedQLI {
      get;
      set;
    }
    
    public String totalToCustomer {
     get {
       Double total = 0.00;
       for(QuoteLineItemWrapper quoteLineItem : quoteLineItems) {
         if(quoteLineItem.deleted == false) {
           total += quoteLineItem.TotalPrice;
         }
       }
     
       return '$' + total;
     }
     
     set;
    
    }
    
    public String customerOrderDate {
      get { 
           if(customerOrderDate == null) {
               if(quote.ExpirationDate != null) {
                 return quote.ExpirationDate.format();
               } else {
                 return (System.today() + 30).format();
               }
            }
            
            System.debug('In Customer Order Date variable ### ' + customerOrderDate);
            return customerOrderDate;
       } 
       
       set;
    }
    
    public String newContractEndDate {
      get { 
          if(newContractEndDate == null) {
           Date customerOrderDateComputed = Date.parse(customerOrderDate);
           
           Boolean pc24 = false;
           Date maxMASContractEndDate = null;
           for(QuoteLineItemWrapper wrapper : activeQuoteLineItems) {
            if(maxMASContractEndDate == null || maxMASContractEndDate < wrapper.qli.MAS_Contract_End_Date__c) {
                maxMASContractEndDate = wrapper.qli.MAS_Contract_End_Date__c;
                System.debug('maxMASContractEndDate &&& : ' + maxMASContractEndDate);
            }
            
            String pc = wrapper.qli.PricebookEntry.Product2.Program_Code__c;
            if(pc == 'C24') {
                pc24 = true;
            }
           }
           
           Date maxDate = maxMASContractEndDate;
           if(maxMASContractEndDate == null || customerOrderDateComputed > maxMASContractEndDate) {
            maxDate = customerOrderDateComputed;
           }
           
           Date endDate = null;
           if(pc24) {
                endDate = maxDate.addYears(2);
           } else {
                endDate = maxDate.addYears(1);
           }

           return endDate.format();
          } 
          return newContractEndDate;
       }
       
       set;
    }    
    
    private ApexPages.StandardController controller;
     
    public ProRateController(ApexPages.StandardController controller) {
         try {
               this.controller = controller;
               
               this.quote = [Select Id, Name, ExpirationDate, QuoteNumber, Ship_To_Contact__r.AccountId from Quote where Id= :controller.getRecord().Id];
               
               Opportunity opp = [SELECT AccountId FROM Opportunity where Id IN (select OpportunityId from Quote where Id = :controller.getRecord().Id)];
             try {
                   Account account = [Select Id, Name from Account where Id = :opp.AccountId];
                   this.accountName = account.Name;
                   this.accountId = account.Id;
              } catch(Exception ex) {
                //Ignore, consume
              }
               
               System.debug('quote.Id %%% ' + quote.Id);
               //NOTE: Hemang change
               List<QuoteLineItem> allQuoteLineItems = [Select Id, Product_Type__c, Prorate_Price__c, Quantity, MAS_Contract_End_Date__c, MAS_Contract_Number__c, MAS_Maintenance_Contract_Number__c, PricebookEntry.Product2.Description,  
                                                         PricebookEntry.UnitPrice, PricebookEntry.Product2.Program_Code__c, PricebookEntry.Product2.Name, PricebookEntry.Product2.Filemaker_External_ID__c, 
                                                         PricebookEntry.Product2.Deferred_Revenue_Code__c, ListPrice, UnitPrice, TotalPrice, PC__c, DPC__c  
                                                         from QuoteLineItem 
                                                         where QuoteId = :quote.Id and (Product_Type__c = 'MNT' OR (Product_Type__c = 'NEW' AND (Package_Format__c ='SBA' OR Package_Format__c = 'SBE' OR Mat_Grp_5__c = 'AVL' OR Mat_Grp_5__c = 'ASL')))];
               											
               for(QuoteLineItem aQuoteLineItem : allQuoteLineItems ) {
               	 System.debug('aQuoteLineItem.Product_Type__c &&& ' + aQuoteLineItem.Product_Type__c);
                 QuoteLineItemWrapper wrapper = new QuoteLineItemWrapper(this, aQuoteLineItem);
                 quoteLineItems.add(wrapper);
               }
               
               List<Contract> contracts = [SELECT EndDate, filemaker_external_id__c FROM Contract];
               for(Contract aContract : contracts) {
                 contractEndDateMap.put(aContract.filemaker_external_id__c, aContract.EndDate);
               }
               
              this.periods = [Select Number, StartDate, EndDate, Type from Period where Type IN ('Month', 'Quarter', 'Year') order by Type, StartDate];
              this.fySettings = [SELECT PeriodId, Name FROM FiscalYearSettings];

          } catch(Exception ex) {
                errorPanel = true;
                errorMessage = 'Unexpected exception occurred: ' + ex.getMessage();
                return;     
          }
          
          if(quoteLineItems.size() == 0) {
            errorPanel = true;
            errorMessage = 'No proratable Quote Line Items found.';
            return;
          }
          
          errorPanel = false;
          errorMessage = '';        
    }

    public PageReference deleteSelectedQLI() {
      
      for(QuoteLineItemWrapper quoteLineItem : quoteLineItems) {
         System.debug('%%% quoteLineItem.qli.Id: ' + quoteLineItem.qli.Id);
         System.debug('%%% selectedQLI: ' + selectedQLI);
         if(quoteLineItem.qli.Id == selectedQLI) {
           quoteLineItem.deleted = true;
         }
      }
      
      return null;
    }
    
    public Integer newContractEndDatePeriod {
      get {
        return getPeriod(newContractEndDate);
      }
      
      set;
    }

    public Integer newContractEndDateQuarter {
      get {
        return getQuarter(newContractEndDate);
      }
      
      set;
    }

    public Integer newContractEndDateYear {
      get {
        return getYear(newContractEndDate);
      }
      
      set;
    }


    public Integer customerOrderDatePeriod {
      get {
        return getPeriod(customerOrderDate);
      }
      
      set;
    }

    public Integer customerOrderDateQuarter {
      get {
        return getQuarter(customerOrderDate);
      }
      
      set;
    }

    public Integer customerOrderDateYear {
      get {
        return getYear(customerOrderDate);
      }
      
      set;
    }

    public Integer getPeriod(Date aDateObject) {
        return Util.getPeriod(periods, fySettings, aDateObject, 'Month');
    }

    public Integer getPeriod(String aDate) {
      Date aDateObject = Date.parse(aDate);
      return getPeriod(aDateObject);
    }
    
    public Integer getQuarter(Date aDateObject) {
        return Util.getPeriod(periods, fySettings, aDateObject, 'Quarter');
    }

    public Integer getQuarter(String aDate) {
      Date aDateObject = Date.parse(aDate);
      return getQuarter(aDateObject);
    }

    public Integer getYear(Date aDateObject) {
      return Util.getPeriod(periods, fySettings, aDateObject, 'Year');
    }

    public Integer getYear(String aDate) {
      Date aDateObject = Date.parse(aDate);
      return getYear(aDateObject);
    }
    
    

    public PageReference recalculate() {
      errorPanel = false;
      errorPanel = null;
      return null;
    }

    public PageReference cancel() {
      return new PageReference('/' + quote.Id);
    }
    

    public PageReference loadDateIntoQuote() {
      errorPanel = false;
      errorPanel = null;
        
      try {
          List<QuoteLineItem> qlisToUpdate = new List<QuoteLineItem>();
    
          for(QuoteLineItemWrapper wrapper : activeQuoteLineItems) {
            wrapper.qli.PC__c = wrapper.pcCode;
            wrapper.qli.FD__c = String.valueOf(wrapper.fd);
            wrapper.qli.DPC__c = wrapper.dpcCode;
            wrapper.qli.Lapse_Periods__c =  String.valueOf(wrapper.lapsePeriod);
            wrapper.qli.Prorate_Price__c = wrapper.perUnitPrice;
            wrapper.qli.UnitPrice = wrapper.perUnitPrice;
            wrapper.qli.Effective_Date__c = Date.parse(wrapper.newContractEffectiveDate);
            wrapper.qli.Contract_End_Date__c = Date.parse(newContractEndDate);
            qlisToUpdate.add(wrapper.qli);
          }      
          
          update qlisToUpdate;
        
          this.quote.Notes_From_OPS__c = this.notes;
          this.quote.Prorated_Maintenance__c = true;
          update this.quote;
      } catch(Exception ex) {
        errorPanel = true;
        String exceptionMessage = ex.getMessage();
        Integer indexOfComma = exceptionMessage.indexOf(',');
        if(indexOfComma > 0) {
            errorMessage = 'Error prorating the Quote: ' + exceptionMessage.substring(indexOfComma + 1);
        } else {
            errorMessage = 'Error prorating the Quote: ' + exceptionMessage;
        }
        return null;
      }
      
      errorPanel = false;
      errorMessage = null;
      return new PageReference('/' + quote.Id);
    }
    
    public Integer fiscalPeriodDifference(Date date1, Date date2) {
          System.debug('date1 ^^^ ' + date1);
          System.debug('date2 ^^^ ' + date2);
          
          Integer yearDifference = getYear(date1) - getYear(date2);
          System.debug('yearDifference ^^^ ' + yearDifference);
          
          Integer fiscalPeriods = (yearDifference * 12) + getPeriod(date1) - getPeriod(date2);
          System.debug('getPeriod(date1) ^^^ ' + getPeriod(date1));
          System.debug('getPeriod(date2) ^^^ ' + getPeriod(date2));
          System.debug('fiscalPeriods ^^^ ' + fiscalPeriods);
          
          return fiscalPeriods;
    }
    
    public static Integer daysBetween(Date d1, Date d2){
             DateTime d1DateTime = DateTime.newInstance(d1.year(), d1.month(), d1.day());
             DateTime d2DateTime = DateTime.newInstance(d2.year(), d2.month(), d2.day());
             
             return (Integer)( (d2DateTime.getTime() - d1DateTime.getTime()) / (1000 * 60 * 60 * 24));
     }     
 

    public class QuoteLineItemWrapper {
    
      public QuoteLineItem qli { get; set; }

      public String contractNumber {
            get {
                if(qli.MAS_Contract_Number__c != null) {
                    return  qli.MAS_Contract_Number__c;
                }
                
                return qli.MAS_Maintenance_Contract_Number__c;
            }
            
            set;
            
      }

        //NOTE: Hemang change
      public String newContractEffectiveDate { 
        get { 
          if(newContractEffectiveDate == null) {
            if(qli.MAS_Contract_End_Date__c != null && qli.MAS_Contract_End_Date__c >= System.today()) {
                    return qli.MAS_Contract_End_Date__c.format();
            } else if(qli.MAS_Contract_Number__c != null) {
                Date endDateFromContract = controller.contractEndDateMap.get(String.valueOf(qli.MAS_Contract_Number__c));
                if(endDateFromContract != null && endDateFromContract >= System.today()) {
                    return endDateFromContract.format();
                }
            } 
            System.debug('returning controller.customerOrderDate ^^^ ' + controller.customerOrderDate);
            return controller.customerOrderDate;
          } else {
          	 System.debug('contractNumber : ' + contractNumber);
          	 System.debug('qli.MAS_Contract_End_Date__c : ' + qli.MAS_Contract_End_Date__c);
          	 if(contractNumber != null && qli.MAS_Contract_End_Date__c < System.today()) {
          	 	System.debug('returning controller.customerOrderDate ^^^ ' + controller.customerOrderDate);
          	 	return controller.customerOrderDate;
          	 }
            
             if(Date.parse(newContractEffectiveDate) < System.today()) {
                return controller.customerOrderDate; 
            } 
          } 
          return newContractEffectiveDate;
        }  
        set;
      }

      public Double quantity { get { return (Integer) qli.Quantity; } set; }

      public Integer fd { 
        get { 
          Date newContractEffectiveDateObject = Date.parse(newContractEffectiveDate);
          Date customerOrderDateObject = Date.parse(controller.customerOrderDate);
        
          System.debug('fd calculation ^^^ ' + newContractEffectiveDateObject + ' - ' + customerOrderDateObject);
          Integer fd = controller.fiscalPeriodDifference(newContractEffectiveDateObject, customerOrderDateObject); 
          if (fd < 0){
              return 0;
          }else {
              return fd;
          }
        } 
        
        set; 
      }      
      
      public String dpcCode {
        get {
            return 'M' + dpc;
        }
        
        set;
      }
      
      public String dpc {
        get {
         return Util.getDpc(qli, pc, perUnitPrice);
        }
        set;
      }
      
      public Integer pc { 
      
        get { 
          Date newContractEffectiveDateObject = Date.parse(newContractEffectiveDate);
          Date newContractEndDateObject = Date.parse(controller.newContractEndDate);
          return controller.fiscalPeriodDifference(newContractEndDateObject, newContractEffectiveDateObject); 
        } 
        
        set; 
      }  
      
      public String pcCode { 
      
        get { 
          return 'C' + pc;
        } 
        
        set; 
      }            
      
      public Decimal totalPrice {
       get {
           Decimal finalPriceNotRounded = quantity * perUnitPrice;
           return finalPriceNotRounded.divide(1, 2, System.RoundingMode.HALF_UP);
       }
       
       set;
      
      }

     public Integer numberOfDays {
        get {
           return Util.getNumberOfDays(newContractEffectiveDate, controller.newContractEndDate);
        }
        set;
     }

     public Decimal perUnitPrice {
        get {
           return Util.getPerUnitPrice(qli, newContractEffectiveDate, controller.customerOrderDate, controller.newContractEndDate, lapsePeriod);
        }
        set;
     }

      public Integer lapsePeriod { 
      
       get  { 
         Date customerOrderDateObject = Date.parse(controller.customerOrderDate);
         if(qli.MAS_Contract_End_Date__c == null) {
          return 0;
         }
         
         if(qli.MAS_Contract_End_Date__c > customerOrderDateObject) {
          return 0;
         }

        return controller.fiscalPeriodDifference(customerOrderDateObject,qli.MAS_Contract_End_Date__c); //NOTE: Hemang change
       } 
       
       set;
      } 

      public Boolean deleted {
        get {
          if(deleted == null) {
           return false;
          }
          
          return deleted; 
        }
        set;
      }   
      
      public ProRateController controller {
        get;
        set;
      }
      
      public QuoteLineItemWrapper(ProRateController controller, QuoteLineItem qli) {
        this.controller = controller;
        this.qli = qli;
      }
    }

}