global class AutoReconcileBatchSchedule implements Schedulable {
  global void execute(SchedulableContext sc) {
    AutoReconcileBatch batch = new AutoReconcileBatch(false);
    Id batchId = database.executeBatch(batch);
  }
}