@isTest
private class CreateOppFromSOControllerTest {
    @isTest(seeAllData=true)
    static void TestOppCreation() {
        Account acc = new Account();
         acc.Industry = 'OTHR Other';
        acc.Name = 'Test Account';
        insert acc;
        
        User u = [select Id from User where IsActive = true limit 1];
        
               
        Sales_Order__c so = new Sales_Order__c();
        so.Account__c = acc.Id;
        so.Name = 'test SO';
        so.CurrencyIsoCode = 'USD';
        so.Channel__c = false;
        so.OwnerId = u.Id;
        so.SAP_Customer__c = 'test customer';
        insert so;  
        
        Sales_Order__c queriedSo = [Select Id, Price_Book__c from Sales_Order__c where Id=: so.Id];
        
        PricebookEntry pbe = [select Id, Product2Id from PricebookEntry where PriceBook2Id =: queriedSo.Price_Book__c and IsActive = true Limit 1 ]; 
        
        Sales_Order_Line__c sol = new Sales_Order_Line__c();
        sol.Name = 'Test sales order Line';
        sol.Sales_Order__c = so.Id;
        sol.CurrencyIsoCode = 'USD';
        sol.Product_ID_SKU__c = pbe.Product2Id;
        sol.Owner__c = u.Id;
        sol.Quantity__c = 5;
        sol.Amount__c = 50;
        insert sol;    
        
        CreateOppFromSOController obj1 = new CreateOppFromSOController();
        system.assertEquals(null,obj1.initPage());
        system.assertEquals('No Sales Order Id is sent', obj1.ErrorMessage);       
        
        test.StartTest();
        ApexPages.currentPage().getParameters().put('soId', so.Id);    
        CreateOppFromSOController obj = new CreateOppFromSOController();
        system.assertEquals(null, obj.initPage());
        system.assertEquals(null,obj.ErrorMessage);
        
        Opportunity opp = [Select Id, AccountId, Channel__c, CurrencyIsoCode, Reconciled__c, StageName, CloseDate from Opportunity where Sales_Order__c =: so.Id];
        system.assertNotEquals(null,opp.Id);
        system.assertEquals(acc.Id, opp.AccountId);
        system.assertEquals(so.Channel__c, opp.Channel__c);
        system.assertEquals(so.CurrencyIsoCode, opp.CurrencyIsoCode);
        system.assertEquals(true, opp.Reconciled__c);
        system.assertEquals('Closed Won', opp.StageName);
        system.assertEquals(date.today(), opp.CloseDate);
        
        List<OpportunityLineItem> olis = [select Id from OpportunityLineItem where OpportunityId =: opp.Id];
        system.assertEquals(1,olis.size());
        
        test.stopTest();
        
    }
}