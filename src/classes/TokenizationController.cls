public class TokenizationController {

    public Boolean errorPanel { get; set; }
    
    public String errorMessage { get; set; }
    

    public Opportunity opp {
      get;
      set;
    }
    
    public String oppMailingStreet {
    	get {
    		String mailingStreet = opp.Bill_To_Contact__r.MailingStreet;
    		return sanitize(mailingStreet);
    	}
    	set;
    }
    
    public String sanitize(String input) {
    	if(input == null || input.trim().equals('')) {
    		return '';
    	}
    	return input.replace('\n', ' ').replace('\r', ' ');
    }

    public TokenizationController(ApexPages.StandardController controller) {
       opp = [select Id, Bill_To_Contact__c, Bill_To_Contact__r.FirstName,  
               Bill_To_Contact__r.LastName, Bill_To_Contact__r.Account.Name, Bill_To_Contact__r.Title,Bill_To_Contact__r.Email,Bill_To_Contact__r.Phone,
               Bill_To_Contact__r.Fax,Bill_To_Contact__r.MailingCity,Bill_To_Contact__r.MailingStateCode,Bill_To_Contact__r.MailingCountryCode,
               Bill_To_Contact__r.MailingStreet, Bill_To_Contact__r.MailingPostalCode 
               from Opportunity where Id =:controller.getRecord().Id];
       System.debug('opp in tokenization page: ' + opp);  
              
       if(opp.Bill_To_Contact__c == null) { 
         errorPanel = true;
         errorMessage = 'Bill To Contact on the Opportunity cannot be blank. This page will be automatically redirected to Opportunity page in 10 seconds.';
         return;
       }

       if(opp.Bill_To_Contact__r.FirstName ==null ||
            opp.Bill_To_Contact__r.LastName ==null || 
            opp.Bill_To_Contact__r.Account.Name ==null || 
            opp.Bill_To_Contact__r.Email ==null ||
            opp.Bill_To_Contact__r.Phone ==null ||
            opp.Bill_To_Contact__r.MailingCity ==null ||
            //opp.Bill_To_Contact__r.MailingStateCode ==null ||
            opp.Bill_To_Contact__r.MailingStreet ==null ||
            opp.Bill_To_Contact__r.MailingCountryCode ==null
            // || opp.Bill_To_Contact__r.MailingPostalCode == null
            ) {  
         errorPanel = true;
         errorMessage = 'Bill To Contact\'s First Name, Last Name, Company Name, Email, Phone, Address, City, State, Country, Zip are required on the Opportunity. This page will be automatically redirected to Opportunity page in 10 seconds.';
         return;
       }       
       
       errorPanel = false;
       errorMessage = null;            
    }


    public PageReference cancel() {
      return new PageReference('/' + opp.Id);
    }

}