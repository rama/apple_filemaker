@isTest
private class ConcurrencyControllerTest {

    static testMethod void controllerTest() {
        Account acc = new Account();
        acc.CurrencyIsoCode = 'USD';
        acc.Name = 'test account';
        acc.Industry = 'Test';
        //acc.SAP_Contract__c = '12345';
        insert acc;
        
        Opportunity opp1 = new Opportunity();
        opp1.AccountId = acc.Id;
        opp1.Name = 'first test opp';
        opp1.CloseDate = date.today();
        opp1.StageName = 'Evaluate';
        opp1.CurrencyIsoCode = 'USD';
        insert opp1;
        
        Quote q1 = new Quote();
        q1.OpportunityId = opp1.Id;
        q1.Name = 'first test quote';
        insert q1;

       Test.startTest();
       Test.setCurrentPage(Page.Concurrency);        
       
       ConcurrencyController ctlr = new ConcurrencyController(new ApexPages.StandardController(q1));
       ctlr.cancel();
       
       System.assertEquals(ctlr.errorMessage, 'Please specify a License Key to add a concurrency line item. This page will be automatically redirected to Quote page in 10 seconds.');
       System.assert(ctlr.errorPanel);
       
       q1.License_Key__c = 'test';
       update q1;
       ctlr = new ConcurrencyController(new ApexPages.StandardController(q1));
       System.assertEquals(ctlr.errorMessage, 'Please set Is Concurrency Quote to true in order to create a Concurrency Quote. This page will be automatically redirected to Quote page in 10 seconds.');
    }
    
    static testMethod void controllerWithLicenseTest() {
        Account acc = new Account();
        acc.CurrencyIsoCode = 'USD';
        acc.Name = 'test account';
        acc.Industry = 'Test';
       // acc.SAP_Contract__c = '12345';
        insert acc;
        
        Opportunity opp1 = new Opportunity();
        opp1.AccountId = acc.Id;
        opp1.Name = 'first test opp';
        opp1.CloseDate = date.today();
        opp1.StageName = 'Evaluate';
        opp1.CurrencyIsoCode = 'USD';
        insert opp1;
        
        Quote q1 = new Quote();
        q1.OpportunityId = opp1.Id;
        q1.Name = 'first test quote';
 		q1.License_Key__c = 'test';
        insert q1;    	

       Test.startTest();
       Test.setCurrentPage(Page.Concurrency);        
       
       ConcurrencyController ctlr = new ConcurrencyController(new ApexPages.StandardController(q1));
       ctlr.cancel();
       
       System.assertEquals(ctlr.errorMessage, 'Please set Is Concurrency Quote to true in order to create a Concurrency Quote. This page will be automatically redirected to Quote page in 10 seconds.');
    }    
}