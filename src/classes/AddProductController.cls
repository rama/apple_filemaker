public with sharing class AddProductController {

	private String recordId;
	
	public String productFamily {get; set;}
	public String productType {get; set;}


	public AddProductController(ApexPages.StandardController controller) {
		recordId = controller.getId();
	}
	
	public PageReference addProduct() {
		if (String.isBlank(productFamily) || String.isBlank(productType)) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select Product Family and Product Type values.'));
			return null;
		}

		String customerGroup;
		String pageUrl;
		
		if (recordId.substring(0, 3) == '006') {
			Opportunity opportunity = [select Group_Code__c from Opportunity where Id = :recordId];
			customerGroup = opportunity.Group_Code__c;
			pageUrl = '/p/opp/SelectSearch';
		} else {
			Quote quote = [select Opportunity.Group_Code__c from Quote where Id = :recordId];
			customerGroup = quote.Opportunity.Group_Code__c;
			pageUrl = '/_ui/sales/quote/SelectSearch';
		}

		PageReference addProductsPage = new PageReference(pageUrl);
		addProductsPage.getParameters().put('addTo', recordId);
		addProductsPage.getParameters().put('nooverride', '1');
		addProductsPage.getParameters().put('PricebookEntrycol0', 'PRODUCT2.FAMILY_ENUM');
		addProductsPage.getParameters().put('PricebookEntryoper0', 'e');
		addProductsPage.getParameters().put('PricebookEntryfval0', productFamily);
		addProductsPage.getParameters().put('PricebookEntrycol1', '00Ni000000Ajagt');
		addProductsPage.getParameters().put('PricebookEntryoper1', 'e');
		addProductsPage.getParameters().put('PricebookEntryfval1', productType);
		addProductsPage.getParameters().put('PricebookEntrycol2', '00Ni0000009slWw');
		addProductsPage.getParameters().put('PricebookEntryoper2', 'e');

		if (customerGroup == 'HDIR' || customerGroup == 'KDIR' || customerGroup == 'NDIR') {
			if (productType == 'NEW') {
				addProductsPage.getParameters().put('PricebookEntryfval2', 'SLE,SBE');
			} else if (productType == 'MNT') {
				addProductsPage.getParameters().put('PricebookEntryfval2', 'SLE,SBE,MNT');
			} else {
				addProductsPage.getParameters().put('PricebookEntryfval2', 'SEU');
			}
		} else {
			if (productType == 'NEW') {
				addProductsPage.getParameters().put('PricebookEntryfval2', 'SL,SBA');
			} else if (productType == 'MNT') {
				addProductsPage.getParameters().put('PricebookEntryfval2', 'SL,SBA,MNT');
			} else {
				addProductsPage.getParameters().put('PricebookEntryfval2', 'SLU');
			}
		}

		// Version
		addProductsPage.getParameters().put('PricebookEntrycol3', '00Ni0000009slWr');
		addProductsPage.getParameters().put('PricebookEntryoper3', 'e');
		if (productType == 'MNT') {
			addProductsPage.getParameters().put('PricebookEntryfval3', 'RMNT');
		} else {
			addProductsPage.getParameters().put('PricebookEntryfval3', 'R13');
		}
		
		return addProductsPage;
	}

	public List<SelectOption> productFamilyOptions {
		get {
			if (productFamilyOptions == null) {
				Set<String> uniqueProductFamilies = new Set<String>();

				for (Product2 product : [select Product_Family_Long_Name__c from Product2 where IsActive = true and Family != null]) {
					uniqueProductFamilies.add(product.Product_Family_Long_Name__c);
				}

				List<String> productFamilies = new List<String>();
				productFamilies.addAll(uniqueProductFamilies);
				productFamilies.sort();

				productFamilyOptions = new List<SelectOption>();

				for (String productFamily : productFamilies) {
					productFamilyOptions.add(new SelectOption(productFamily.substring(0, 3), productFamily));
				}
			}
			
			return productFamilyOptions;
		}
		
		private set;
	}
	
	public List<SelectOption> productTypeOptions {
		get {
			if (productTypeOptions == null) {
				List<String> productTypes = new List<String>{'NEW', 'MNT', 'UPG'};
				
				productTypeOptions = new List<SelectOption>();
				
				for (String productType : productTypes) {
					productTypeOptions.add(new SelectOption(productType, productType));
				}
			}
			
			return productTypeOptions;
		}
		
		private set;
	}

}