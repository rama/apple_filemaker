@isTest
private class OpportunityTriggerTest {

	@isTest
	static void testMaintainGroupOnOpportunity() {
		Account account = new Account();
		account.Name = 'Test';
		account.Group__c = 'Test';
		account.Industry = 'OTHR Other';
		insert account;
		
		Opportunity opportunity = new Opportunity();
		opportunity.AccountId = account.Id;
		opportunity.Name = 'Test';
		opportunity.StageName = 'Qualification';
		opportunity.CloseDate = Date.today();
		insert opportunity;
		
		// Validate insert.
		System.assertEquals(account.Group__c, [select Group__c from Opportunity where Id = :opportunity.Id].Group__c);
		
		Account account2 = new Account();
		account2.Name = 'Test2';
		account2.Group__c = 'Test2';
		account2.Industry = 'OTHR Other';
		insert account2;
		
		opportunity.AccountId = account2.Id;
		update opportunity;
		
		// Validate update with another Account.
		System.assertEquals(account2.Group__c, [select Group__c from Opportunity where Id = :opportunity.Id].Group__c);
		
		opportunity.AccountId = null;
		update opportunity;
		
		// Validate update with Account removed.
		System.assertEquals(null, [select Group__c from Opportunity where Id = :opportunity.Id].Group__c);
	}

    @isTest(seeAllData = true)
    static void testSalesOrderandPricebookUpdate() {
        
        Account acc1 = new Account();
        acc1.CurrencyIsoCode = 'USD';
        acc1.Name = 'test account';
        acc1.Industry = 'OTHR Other';
        insert acc1;
        
         Sales_Order__c so = new Sales_Order__c();
        so.Name = 'test SO';
        so.SAP_Customer__c = 'test customer';
        so.CurrencyIsoCode = 'USD';
        insert so;
       
        Opportunity opp1 = new Opportunity();
        opp1.AccountId = acc1.Id;
        opp1.Name = 'first test opp';
        opp1.CloseDate = date.today();
        opp1.StageName = 'Evaluate';
        opp1.CurrencyIsoCode = 'USD';
        opp1.Sales_Order__c = so.Id;
        opp1.Channel__c = true;
        insert opp1;
        
        system.assertEquals(null,so.Account__c);
                
        Opportunity queriedOpp = [select Id, Pricebook2Id from Opportunity where Id =: opp1.Id];
        List<Pricebook2> pbs = [Select Id from Pricebook2 where Pricing_Type__c = 'Distribution' and Currency__c = :opp1.CurrencyIsoCode ];
        if(pbs != null && pbs.size() == 1) {
        	system.assertEquals(pbs[0].Id,queriedOpp.Pricebook2Id );
        }       
        
        Account acc2 = new Account();
        acc2.CurrencyIsoCode = 'USD';
        acc2.Name = 'test account';
        acc2.Industry = 'OTHR Other';
        insert acc2;       
        
        opp1.AccountId = acc2.Id;
        update opp1;
        
        Sales_Order__c requeriedso = [select Id, Account__c, Contract__c, SAP_Contract__c from Sales_Order__c where Id =: so.Id limit 1];
        
        system.assertEquals(acc2.Id,requeriedso.Account__c);
    }
}