@isTest (SeeAllData=true)
private class ImportMNTLinesControllerTest {

     static List<ImportMNTLinesController.ContractWrapper> sampleContracts = new List<ImportMNTLinesController.ContractWrapper>();


     static testMethod void setupData() {
        
        
       Account a = new Account();
       a.Name = 'Test Account';
       a.Industry = 'Test';
       insert a;

       Contact ct = new Contact();
       ct.AccountId = a.Id;
       ct.FirstName = 'Test';
       ct.LastName = 'Contact';
       insert ct;
       
       System.assertNotEquals(a.Id, null);
     
       Opportunity o = new Opportunity();
       o.Name = 'Test Opp';
       o.AccountId = a.Id;
       o.StageName = 'Purchase';
       o.CloseDate = System.today();
     
       insert o;
    
       System.assertNotEquals(o.Id, null);

       Contract c = new Contract();
       c.Name = 'C1';
       c.filemaker_external_id__c = 'test1234';
       c.AccountId = a.Id;
       insert c;
       
       Sales_Order__c so = new Sales_Order__c();
       so.Name = 'so1';
       so.Contract__c = c.Id;
       so.SAP_Customer__c = 'sap1';
       so.Account__c = a.Id;
       so.License_To_Customer_Name__c = 'test';
       so.License_To_Customer_Name2__c = 'test';
       so.License_To_Customer_Name3__c = 'test';
       so.License_To_Customer_Name4__c = 'test';
       
       so.License_To_Address1__c = 'test';
       so.License_To_Address2__c = 'test';
       so.License_To_Address3__c = 'test';
       so.License_To_Address4__c = 'test';
       insert so;   

    
       
       Product2 pr = new Product2();
       pr.Name = 'p1';
       pr.Deferred_Revenue_Code__c = 'M5';
       pr.filemaker_external_id__c = 'test2345';
       insert pr;

       
       Sales_Order_Line__c sol1 = new Sales_Order_Line__c();
       sol1.Name = 'Sol1';
       sol1.Sales_Order__c = so.Id;
       sol1.Renewed__c = false;
       sol1.Product_ID_SKU__c = pr.Id;
       insert sol1;
       
        
		Pricebook2 pb2 = [Select Id, Name, IsActive From Pricebook2 where IsStandard = true LIMIT 1];
        if (!pb2.isActive) {
            pb2.isActive = true;
            update pb2;
        }
      
       PricebookEntry pbe = new PricebookEntry();
       pbe.Product2Id = pr.Id;
       pbe.Pricebook2Id = pb2.Id;
       pbe.UnitPrice = 50.00;
       pbe.IsActive = true;
       insert pbe;

       Quote q = new Quote();
       q.Name = 'Test Quote';
       q.License_To_Contact__c = ct.Id;
	   q.OpportunityId = o.Id;
	   q.Pricebook2Id = pb2.Id;
       insert q;
       System.assertNotEquals(q.Id, null);

        Annual_eAccept__c eAccept = new Annual_eAccept__c();
        eAccept.Name = 'Test eAccept';
        eAccept.Annual_Quote__c = q.Id;
        eAccept.Licensee_Contact__c = ct.Id;
        eAccept.Licensee_Account__c = a.Id;
        insert eAccept;
             
       QuoteLineItem qli = new QuoteLineItem();
       qli.QuoteId = q.Id;
       qli.Prorate_Price__c = 100.00;
       qli.UnitPrice = 50.00;
       qli.DPC__c = 'M00';
       qli.Quantity = 10;
       qli.PricebookEntryId = pbe.Id;
       insert qli;
       
       List<SelectOption> options = new List<SelectOption>();
       options.add(new SelectOption('test', 'test'));
       ImportMNTLinesController.QLIWrapper qliWrapper = new ImportMNTLinesController.QLIWrapper(qli, options, 'p1', 'FMP');

        
	     ImportMNTLinesController.ContractWrapper wrapper = new ImportMNTLinesController.ContractWrapper(sol1);
	     sampleContracts.add(wrapper);
     }
  
  @isTest (SeeAllData=true)
  static void setupPriceBookData() {
		Pricebook2 pb2 = [Select Id, Name, IsActive From Pricebook2 where IsStandard = true LIMIT 1];
        if (!pb2.isActive) {
            pb2.isActive = true;
            update pb2;
        }
        
        Product2 pr = [select id from product2 limit 1];
      
       PricebookEntry pbe = new PricebookEntry();
       pbe.Product2Id = pr.Id;
       pbe.Pricebook2Id = pb2.Id;
       pbe.UnitPrice = 50.00;
       pbe.IsActive = true;
       //insert pbe;
  	
  }

 
   static testMethod void constructorTest() {
     setupData();
     
     Quote aQuote = [select Id, Name from Quote limit 1];
   
     ImportMNTLinesController controller = new ImportMNTLinesController(new ApexPages.StandardController(aQuote));
     List<ImportMNTLinesController.ContractWrapper> contracts = controller.contracts;
     for(ImportMNTLinesController.ContractWrapper c : contracts) {
     	c.selected = true;
     	System.assertEquals(c.contractNumber, 'test1234');
     	System.assertEquals(c.name, 'test test test test');
     	System.assertEquals(c.address, 'test test test test');
     }
     controller.manualContractNumber = 'test1234';
     controller.continueToImport();
     controller.importMntLines();
     
     System.assertEquals(controller.errorPanel, true);
     System.assertNotEquals(controller.errorMessage, null);
     
     controller.cancel();

     Boolean value = controller.contains('test1234');   
     System.assertEquals(value, true);
     
     ImportMNTLinesController.ContractWrapper aWrapper = controller.getContract('test1234');
     System.assertNotEquals(aWrapper, null);   

   } 

   @isTest (SeeAllData=true)
   static void constructorWithPriceBookTest() {
     setupData();
     setupPriceBookData();
     
     Quote aQuote = [select Id, Name from Quote limit 1];
   
     ImportMNTLinesController controller = new ImportMNTLinesController(new ApexPages.StandardController(aQuote));
     List<ImportMNTLinesController.ContractWrapper> contracts = controller.contracts;
     for(ImportMNTLinesController.ContractWrapper c : contracts) {
     	c.selected = true;
     }
     
     controller.manualContractNumber = 'test1234';
     controller.continueToImport();
     controller.importMntLines();
     
     System.assertEquals(controller.errorPanel, true);
     System.assertNotEquals(controller.errorMessage, null);
   } 

 
   static testMethod void constructorNoDataTest() {
        Account a = new Account();
       a.Name = 'Test Account';
       a.Industry = 'Test';
       insert a;

       Contact ct = new Contact();
       ct.AccountId = a.Id;
       ct.FirstName = 'Test';
       ct.LastName = 'Contact';
       insert ct;
       
       System.assertNotEquals(a.Id, null);
     
       Opportunity o = new Opportunity();
       o.Name = 'Test Opp';
       o.AccountId = a.Id;
       o.StageName = 'Purchase';
       o.CloseDate = System.today();
     
       insert o;
    
       System.assertNotEquals(o.Id, null);

       Contract c = new Contract();
       c.Name = 'C1';
       c.filemaker_external_id__c = 'test1234';
       c.AccountId = a.Id;
       insert c;
       

    
       Quote q = new Quote();
       q.Name = 'Test Quote';
       q.License_To_Contact__c = ct.Id;
       q.OpportunityId = o.Id;
       insert q;
    
     Quote aQuote = [select Id, Name from Quote limit 1];
   
     ImportMNTLinesController controller = new ImportMNTLinesController(new ApexPages.StandardController(aQuote));
     controller.manualContractNumber = 'testabcd';
     controller.continueToImport();
     
     controller.importMntLines();
     
     System.assertEquals(controller.errorPanel, true);
     System.assertNotEquals(controller.errorMessage, null);
  }  
    
}