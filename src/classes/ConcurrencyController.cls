public class ConcurrencyController {

    public Boolean errorPanel { get; set; }
    
    public String errorMessage { get; set; }
    
    public Quote q {
      get;
      set;
    }

    public ConcurrencyController(ApexPages.StandardController controller) {
       q = [select Id, License_Key__c, Is_Concurrency_Quote__c from Quote where Id =:controller.getRecord().Id];
       if(q.License_Key__c == null) {
       	 errorPanel = true;
       	 errorMessage = 'Please specify a License Key to add a concurrency line item. This page will be automatically redirected to Quote page in 10 seconds.';
       	 return;
       }
       
       if(!q.Is_Concurrency_Quote__c) {
       	 errorPanel = true;
       	 errorMessage = 'Please set Is Concurrency Quote to true in order to create a Concurrency Quote. This page will be automatically redirected to Quote page in 10 seconds.';
       	 return;
       }
       
       
       errorPanel = false;
       errorMessage = null;
    }

    public PageReference cancel() {
      return new PageReference('/' + q.Id);
    }
}