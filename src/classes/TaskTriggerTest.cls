@isTest
private class TaskTriggerTest {

	static testMethod void testMaintainFirstActivityDate() {
		Lead lead = new Lead();
		lead.LastName = 'Test';
		lead.Company = 'Test';
		insert lead;

        System.assertEquals(null, [select First_Activity_Date__c from Lead where Id = :lead.Id].First_Activity_Date__c);

		Task call = new Task();
		call.Type = 'Call';
		call.WhoId = lead.Id;
		call.Status = 'Completed';
		insert call;
		
		System.assertNotEquals(null, [select First_Activity_Date__c from Lead where Id = :lead.Id].First_Activity_Date__c);
	}

}