@isTest
private class SalesOrderLineItemTriggerTest {

    @ isTest (SeeAllData = true) 
    static void setupAndTest() {
        
        Account acc = new Account();
        acc.CurrencyIsoCode = 'USD';
        acc.Name = 'test account';
        acc.Industry = 'OTHR Other';
        insert acc;         
        
        Pricebook2 standardPB = [select id from Pricebook2 where isStandard=true];
        
        PriceBook2 pb = new PriceBook2();
        pb.Name = 'test pricebook';
        insert pb;
        
        Product2 prod = new Product2();
        prod.Name = 'test product';
        prod.filemaker_external_id__c = 'test external id';
        prod.CurrencyIsoCode = 'USD';        
        insert prod;
        
        PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = standardPB.Id, Product2Id = prod.Id, UnitPrice = 10000, IsActive = true, UseStandardPrice = false);
        insert standardPrice;
        
        PricebookEntry pbe = new PricebookEntry();
        pbe.Product2Id = prod.Id;
        pbe.Pricebook2Id = pb.Id;
        pbe.UnitPrice = 100;
        pbe.UseStandardPrice = false;
        pbe.IsActive=true;
        insert pbe;
        
        Opportunity opp1 = new Opportunity();
        opp1.AccountId = acc.Id;
        opp1.Name = 'first test opp';
        opp1.CloseDate = date.today();
        opp1.StageName = 'Evaluate';
        opp1.CurrencyIsoCode = 'USD';
        opp1.Pricebook2Id = pb.Id;
        insert opp1;
        
        system.debug('opps pricebook is:' + opp1.Pricebook2Id);
        
      /*OpportunityLineItem oli = new OpportunityLineItem();
        oli.OpportunityId = opp1.Id;
        oli.Quantity = 2;
        oli.TotalPrice = 100;
        system.debug('olis pricebook entry id is:' + pbe.ID);
        system.debug('olis entrys pricebook is is ' + pbe.Pricebook2Id);
        oli.PricebookEntryId = pbe.Id;
        insert oli;*/
        
        //system.assertEquals(null, oli.Sales_Order_Line__c);
        
        Sales_Order__c so = new Sales_Order__c();
        so.Name = 'test SO';
        so.SAP_Customer__c = 'test customer';
        so.CurrencyIsoCode = 'USD';
        insert so;
        
        Sales_Order_Line__c sol = new Sales_Order_Line__c();
        sol.Name = 'Test sales order Line';
        sol.Sales_Order__c = so.Id;
        sol.CurrencyIsoCode = 'USD';
       // sol.Opportunity_Line_Item_ID__c = oli.Id;
        sol.Product_ID_SKU__c = prod.Id;
        insert sol;
        
       // OpportunityLineItem requeriedOli = [select Id, Sales_Order_Line__c from OpportunityLineItem where Id =: oli.Id LIMIT 1];
        //system.assertEquals(sol.Id, requeriedOli.Sales_Order_Line__c);
    }
}