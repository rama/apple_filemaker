@isTest
public class AutoReconcileBatchTest {
	 
    static testMethod void myUnitTest() {
        AutoReconcileBatch batch = new AutoReconcileBatch(true);
        test.startTest();
	  	Id batchId = database.executeBatch(batch);    
	  	test.stopTest();
    }
    
   @isTest(seeAllData = true)
    static void testAutoReconcile() {
    
    	Account acc = new Account();
        acc.CurrencyIsoCode = 'USD';
        acc.Name = 'test account';
        acc.Industry = 'OTHR Other';
        insert acc;
        
        Opportunity opp1 = new Opportunity();
        opp1.AccountId = acc.Id;
        opp1.Name = 'first test opp';
        opp1.CloseDate = date.today();
        opp1.StageName = 'Evaluate';
        opp1.CurrencyIsoCode = 'USD';
       // opp1.Pricebook2Id = pb.Id;
        opp1.Channel__c = true;
        insert opp1;           
              
        Sales_Order__c so = new Sales_Order__c();
        so.Name = 'test SO';
        so.SAP_Customer__c = 'test customer';
        so.CurrencyIsoCode = 'USD';
        so.Opportunity__c = opp1.Id;
        so.Channel__c = true;
        so.Account__c = acc.Id;
        so.Opportunity__c = opp1.Id;
        insert so; 
        
        Opportunity opp2 = new Opportunity();
        opp2.AccountId = acc.Id;
        opp2.Name = 'second test opp';
        opp2.CloseDate = date.today();
        opp2.StageName = 'Evaluate';
        opp2.CurrencyIsoCode = 'USD';
        opp2.Channel__c = false;
        insert opp2;   
        
        Sales_Order__c so2 = new Sales_Order__c();
        so2.Name = 'second test SO';
        so2.SAP_Customer__c = 'test customer';
        so2.CurrencyIsoCode = 'USD';
        so2.Opportunity__c = opp1.Id;
        so2.Channel__c = true;
        so2.Account__c = acc.Id;
        so2.Opportunity__c = opp2.Id;
        insert so2; 
        
        List<Sales_Order__c> salesOrders = new List<Sales_Order__c>();
                        
        User activeUser = [select Id from User where IsActive = true limit 1];
        Sales_Order__c requeriedSo = [select Id, Price_Book__c, Name, SAP_Customer__c, CurrencyIsoCode, Opportunity__c, Channel__c, Account__c from Sales_Order__c where Id =: so.Id limit 1]; 
        Sales_Order__c secondqueriedSo = [select Id, Price_Book__c, Name, SAP_Customer__c, CurrencyIsoCode, Opportunity__c, Channel__c, Account__c from Sales_Order__c where Id =: so2.Id limit 1]; 
           
        salesOrders.add(requeriedSo);
        salesOrders.add(secondqueriedSo);
        system.debug('pricebook id is: ' + requeriedSo.Price_Book__c);
        system.debug('sales order inserted is: ' + requeriedSo.Id);
        PricebookEntry pbentry = [select Id, Product2Id, Pricebook2Id from PricebookEntry where Pricebook2Id =:requeriedSo.Price_Book__c limit 1];
   		
   		system.debug('price book entry id: ' + pbentry.Id);
   		system.debug('PB ENTRYS PRODUCT ID: ' + pbentry.Product2Id );
   		
        Sales_Order_Line__c sol = new Sales_Order_Line__c();
        sol.Name = 'Test sales order Line';
        sol.Sales_Order__c = so.Id;
        sol.CurrencyIsoCode = 'USD';
        sol.Quantity__c = 5;
        sol.Product_ID_SKU__c = pbentry.Product2Id;
        sol.Owner__c = activeUser.Id;
        sol.Amount__c = 45;
        insert sol;
        
        Sales_Order_Line__c sol2 = new Sales_Order_Line__c();
        sol2.Name = 'Test sales order Line';
        sol2.Sales_Order__c = so2.Id;
        sol2.CurrencyIsoCode = 'USD';
        sol2.Quantity__c =15;
        sol2.Product_ID_SKU__c = pbentry.Product2Id;
        sol2.Owner__c = activeUser.Id;
        sol2.Amount__c = 5;
        insert sol2;
             
        
        OpportunityLineItem oli1 = new OpportunityLineItem();
        oli1.OpportunityId = opp1.Id;
        oli1.Quantity = 2;
        oli1.TotalPrice = 100;
        oli1.PricebookEntryId = pbentry.Id;
        insert oli1;
        
        Sales_Order_Line__c queriedSol = [select Id, Product_ID_SKU__c, Sales_Price__c from Sales_Order_Line__c where Id =: sol.Id];
        system.debug('sols product after inserted: ' + queriedsol.Product_ID_SKU__c);
        
        AutoReconcileBatch batch = new AutoReconcileBatch();
  
        batch.autoReconcile(salesOrders);
       
        //test the change of quantity and unit price on opportunity line item
        List<OpportunityLineItem> olis = [select Id, Quantity, UnitPrice from OpportunityLineItem where OpportunityId = :opp1.Id];
        system.assertEquals(1, olis.size());
        OpportunityLineItem oli = olis[0];
        system.assertEquals(5,oli.Quantity,'Line items quantity must be updated with that of sales order line');
        system.assertEquals(queriedsol.Sales_Price__c, oli.UnitPrice, 'unit price on opportunity line item must be updated with that of sales order lines sales price');
        
        //Test the pricebook change in opportunity external to salesforce
         Opportunity queriedopp2 = [select Id, Pricebook2Id, Sales_Order__c from Opportunity where Id=: opp2.Id];
         List<OpportunityLineItem> olis2 = [select Id, Quantity, UnitPrice, Sales_Order_Line__c from OpportunityLineItem where OpportunityId = :opp2.Id];
         system.assertEquals(1, olis2.size());
         system.assertEquals(requeriedSo.Price_Book__c, queriedopp2.Pricebook2Id);
         system.assertEquals(so2.Id, queriedOpp2.Sales_Order__c);
         system.assertEquals(sol2.Id, olis2[0].Sales_Order_Line__c);
    }
}