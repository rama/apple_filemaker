public class ImportMNTLinesController {

    //Quote for which we are importing MNT lines
    public Quote quote {
     get;
     set;
    }

    public Boolean errorPanel { get; set; }
    
    public String errorMessage { get; set; }

    public List<Sales_Order_Line__c> salesOrderLines {
        get {
            if(salesOrderLines == null) {
                salesOrderLines = new List<Sales_Order_Line__c>();
            }
            
            return salesOrderLines;
        }
        set;
    }
    
    public List<ContractWrapper> contracts {
        get {
        	if(contracts == null) {
                contracts = new List<ContractWrapper>();
            }
         	return contracts;   
        }
        
        set;
    }

    public List<QLIWrapper> qlis {
    	get {
    		if(qlis == null) {
    			qlis = new List<QLIWrapper>();
    		}	
    		return qlis;
    	}
    	
    	set;
    }

    public String manualContractNumber { get; set; }

	public Contract manualContract {
		get;
		set;
	}

    // Get all Sales Order Lines with 'MNT' product type with the account as quote and display
    public ImportMNTLinesController(ApexPages.StandardController controller) {
       this.quote = [Select Id, Name, Opportunity.Group_Code__c, Pricebook2ID, ExpirationDate, QuoteNumber, License_To_Contact__r.AccountId, License_To_Contact__r.Account.Name, Ship_To_Contact__r.AccountId from Quote where Id= :controller.getRecord().Id];
       
       System.debug('quote.License_To_Contact__r.AccountId %%% ' + quote.License_To_Contact__r.AccountId);
       List<Sales_Order_Line__c> sols = [SELECT Id, Name, Sales_Order__r.Name, Sales_Order__r.Contract__r.FileMaker_External_ID__c,  Sales_Order__r.Contract__r.EndDate, Sales_Order__r.License_To_Customer_Name__c, 
                          Sales_Order__r.License_To_Customer_Name2__c, Sales_Order__r.License_To_Customer_Name3__c, Sales_Order__r.License_To_Customer_Name4__c, Sales_Order__r.License_To_Address1__c, Sales_Order__r.License_To_Address2__c,
                          Sales_Order__r.License_To_Address3__c, Sales_Order__r.License_To_Address4__c, Sales_Order__r.License_To_City__c, LastModifiedDate, Product_ID_SKU__c, Product_ID_SKU__r.Name, Product_ID_SKU__r.Family, Quantity__c, Product_Family__c  
                          FROM Sales_Order_Line__c where Sales_Order__r.Account__c = : quote.License_To_Contact__r.AccountId AND 
                          Product_ID_SKU__r.DPC_Short_Code__c = 'M' AND Renewed__c = false order by LastModifiedDate desc];
       System.debug('sols %%% ' + sols);                          
       for(Sales_Order_Line__c sol : sols) {
         salesOrderLines.add(sol);
         String contractExternalId = sol.Sales_Order__r.Contract__r.FileMaker_External_ID__c; 
         if(!contains(contractExternalId)) {
            contracts.add(new ContractWrapper(sol));
         }         
       }
       
       if(quote.License_To_Contact__r.AccountId == null) {
            errorPanel = true;
            errorMessage = 'Please select License to Contact on Quote to Import MNT lines.';
       } else if(sols.size() == 0) {
            errorPanel = true;
            errorMessage = 'No contracts found for License to Contact ' + quote.License_To_Contact__r.Account.Name + ' on Quote.';
       } else {
            errorPanel = false;
            errorMessage = null;
       }
       
    }
    
    public Boolean contains(String contractExternalId) {
    	for(ContractWrapper wrapper : contracts) {
    		if(wrapper.contractNumber == contractExternalId) {
    			return true;
    		}
    	}
    	
    	return false;
    }

    public ContractWrapper getContract(String contractExternalId) {
    	for(ContractWrapper wrapper : contracts) {
    		if(wrapper.contractNumber == contractExternalId) {
    			return wrapper;
    		}
    	}
    	
    	return null;
    }


    //Go back to Quote detail page when cancelled
    public PageReference cancel() {
      return new PageReference('/' + quote.Id);
    }
    
    //Continue to import mnt lines
    public PageReference continueToImport() {
		
	  try {
	  	if(manualContractNumber != null && !manualContractNumber.trim().equals('')) {
	  		manualContract = [Select Id, FileMaker_External_ID__c from Contract where FileMaker_External_ID__c = :manualContractNumber];
	  	}
	  } catch(Exception ex) {
	  	
	  }
	  
	  if(manualContractNumber != null && !manualContractNumber.trim().equals('') && manualContract == null) {
            errorPanel = true;
            errorMessage = 'Manual Contract Added ' + manualContractNumber + ' is not valid. Please enter valid Contract Number to Continue.';
            return null;
      } else {
      	 if(manualContract != null) {
				List<Sales_Order_Line__c> manualSOLs = [SELECT Id, Name, Sales_Order__r.Name, Sales_Order__r.Contract__r.FileMaker_External_ID__c,  Sales_Order__r.Contract__r.EndDate, Sales_Order__r.License_To_Customer_Name__c, 
		                          Sales_Order__r.License_To_Customer_Name2__c, Sales_Order__r.License_To_Customer_Name3__c, Sales_Order__r.License_To_Customer_Name4__c, Sales_Order__r.License_To_Address1__c, Sales_Order__r.License_To_Address2__c,
		                          Sales_Order__r.License_To_Address3__c, Sales_Order__r.License_To_Address4__c, Sales_Order__r.License_To_City__c, LastModifiedDate, Product_ID_SKU__c, Product_ID_SKU__r.Name, Product_ID_SKU__r.Family, Quantity__c, Product_Family__c  
		                          FROM Sales_Order_Line__c where Sales_Order__r.Contract__r.FileMaker_External_ID__c =: manualContract.filemaker_external_id__c AND 
		                          Product_ID_SKU__r.DPC_Short_Code__c = 'M' AND Renewed__c = false order by LastModifiedDate desc];
		       System.debug('manualSOLs %%% ' + manualSOLs);                          
		       for(Sales_Order_Line__c sol : manualSOLs) {
		         salesOrderLines.add(sol);
		         String contractExternalId = sol.Sales_Order__r.Contract__r.FileMaker_External_ID__c; 
		         if(!contains(contractExternalId)) {
		         	ContractWrapper wrapper = new ContractWrapper(sol);
		         	wrapper.manual = true;
		            contracts.add(wrapper);
		         } else {
		         	ContractWrapper wrapper = getContract(contractExternalId);
		         	wrapper.manual = true;
		         }         
		       }
      	 }      	
      }
	  

      try {
          Boolean atleastOneSelected = false;
          String priceBookSOQL = 
          'Select Id, UnitPrice, Product2.Family, Product2Id, Pricebook2ID, Product2.Description from PriceBookEntry where Product2.Product_Type__c = \'MNT\' and IsActive = true and Pricebook2ID = '; 
          priceBookSOQL +=  '\'' + quote.Pricebook2Id + '\' ';

		  String groupCode = quote.Opportunity.Group_Code__c;
		  if(groupCode == 'HDIR' ||  groupCode == 'KDIR' || groupCode == 'NDIR') {          
          	priceBookSOQL +=  ' and Product2.Package_Format__c IN (\'SLE\', \'SBE\', \'MNT\')';
		  } else {
          	priceBookSOQL +=  ' and Product2.Package_Format__c IN (\'SL\', \'SBA\', \'MNT\')';
		  }
          
          //and Product2.Family = 'FMP' sol.Product_Family__c
          
          List<PriceBookEntry> priceBookEntries = Database.Query(priceBookSOQL);
          Map<Id, PriceBookEntry> IdToPBEMap = new Map<Id, PriceBookEntry>(); 
          for(PriceBookEntry priceBookEntry : priceBookEntries) {
            IdToPBEMap.put(priceBookEntry.Id, priceBookEntry);
          }
          
          for(ContractWrapper contract : contracts) {
          	System.debug('contract.manual %%% : ' + contract.manual);
          	System.debug('contract.selected %%% : ' + contract.selected);
          	System.debug('contract.contractNumber %%% : ' + contract.contractNumber);
            if((contract.selected != null && contract.selected == true) || contract.manual == true) {
                atleastOneSelected = true;
                for(Sales_Order_Line__c sol : salesOrderLines) {
                	System.debug('sol.Sales_Order__r.Contract__r.FileMaker_External_ID__c: ' + sol.Sales_Order__r.Contract__r.FileMaker_External_ID__c);
                	System.debug('contract.sol.Sales_Order__r.Contract__r.FileMaker_External_ID__c: ' + contract.sol.Sales_Order__r.Contract__r.FileMaker_External_ID__c);
                	if(sol.Sales_Order__r.Contract__r.FileMaker_External_ID__c == contract.sol.Sales_Order__r.Contract__r.FileMaker_External_ID__c) {
		                QuoteLineItem qlItem = new QuoteLineItem();
		                qlItem.MAS_Maintenance_Contract_Number__c = sol.Sales_Order__r.Contract__r.FileMaker_External_ID__c;
		                qlItem.MAS_Contract_End_Date__c = sol.Sales_Order__r.Contract__r.EndDate;
		                qlItem.Quantity = sol.Quantity__c;
		                qlItem.Sales_Order_Line__c = sol.Id;   
		                qlItem.QuoteId = this.quote.Id;
		                
		                /*
		                try {
		                    System.debug('contract.sol.Product_ID_SKU__c ### : ' + contract.sol.Product_ID_SKU__c);
		                    System.debug('quote.Pricebook2ID ### : ' + quote.Pricebook2ID);
		                    for(PriceBookEntry priceBookEntry : priceBookEntries) {
		                        if(priceBookEntry.Product2Id == contract.sol.Product_ID_SKU__c && priceBookEntry.Pricebook2ID == quote.Pricebook2ID) {
		                            qlItem.PricebookEntryID = priceBookEntry.Id; 
		                            qlItem.UnitPrice = priceBookEntry.UnitPrice;
		                            break;                          
		                        }
		                    }
		                } catch(Exception ex) {
		                    errorPanel = true;
		                    errorMessage = 'Error importing MNT lines. Not able to retrieve Price book details for some Sales Order Line products.';
		                    return null;
		                } */
		                
		                List<SelectOption> skus = new List<SelectOption>();
						for(PriceBookEntry priceBookEntry : priceBookEntries) {
		                        if(priceBookEntry.Product2.Family == sol.Product_Family__c) {
									System.debug('priceBookEntry.Product2.Family %%%: ' + priceBookEntry.Product2.Family);
								 	System.debug('sol.Product_Family__c %%%: ' + sol.Product_Family__c);
		                            skus.add(new SelectOption(priceBookEntry.Id, priceBookEntry.Product2.Description));
		                        }
		                }                
		                        
		                System.debug('skus ### : ' + skus);     
						QLIWrapper qliw = new QLIWrapper(qlItem, skus, sol.Product_ID_SKU__r.Name, sol.Product_Family__c);                                    
		                qlis.add(qliw);
		             }
	            }
            }
          }      
          
          if(!atleastOneSelected && (manualContractNumber == null || manualContractNumber.trim().equals(''))) {
            errorPanel = true;
            errorMessage = 'Select at least one contract or Add Contract Manually to Continue.';
            return null;
          } 
      } catch(Exception ex) {
        errorPanel = true;
        errorMessage = 'Unexpected Error Occuured. Error Message: ' + ex.getMessage();
        System.debug(ex.getMessage() + ' ' + ex.getStackTraceString());
        return null;
      } 
      
      errorPanel = false;
      errorMessage = null;
      PageReference pr = new PageReference('/apex/ImportMNTLinesPage2');
      return pr;
    }
    
    // Import selected MNT lines 
    public PageReference importMntLines() {
      try {
          List<QuoteLineItem> qlisToCreate = new List<QuoteLineItem>();
          Boolean atleastOneSelected = false;
          List<PriceBookEntry> priceBookEntries = [Select Id, UnitPrice, Product2Id, Pricebook2ID from PriceBookEntry];
          Map<Id, PriceBookEntry> IdToPBEMap = new Map<Id, PriceBookEntry>(); 
          for(PriceBookEntry priceBookEntry : priceBookEntries) {
            IdToPBEMap.put(priceBookEntry.Id, priceBookEntry);
          }

		  if(qlis.size() == 0) {
	        errorPanel = true;
	        errorMessage = 'No Quote Line Items to Import.';
			return null;		  	
		  }          
          
          for(QLIWrapper qliw : qlis) {
          	PriceBookEntry pbe = IdToPBEMap.get(qliw.selectedSKU);
          	qliw.qli.PricebookEntryID = pbe.Id;
          	qliw.qli.UnitPrice = pbe.UnitPrice;
          	qlisToCreate.add(qliw.qli);
          }
          
          insert qlisToCreate;
      } catch(Exception ex) {
        errorPanel = true;
        errorMessage = 'Error importing MNT lines. Error Message: ' + ex.getMessage();
        System.debug(ex.getMessage() + ' ' + ex.getStackTraceString());
        return null;
      } 
      
      errorPanel = false;
      errorMessage = null;
      return new PageReference('/' + quote.Id);
    }
    
    
    
    /**
    * Wrapper class for Contract so we can identify selected items using checkbox and also create variables for computed columns
    */
    public class ContractWrapper { 
        public Boolean selected { get; set; }

        public Boolean manual { get; set; }

        public Sales_Order_Line__c sol { get; set; }
        
        public String contractNumber {
            get {
                return String.valueOf(sol.Sales_Order__r.Contract__r.filemaker_external_id__c);
            }
            
            set;
        }

        public String name { 
            get {
                String concatName = '';
                
                if(sol.Sales_Order__r.License_To_Customer_Name__c != null) {
                    concatName += sol.Sales_Order__r.License_To_Customer_Name__c + ' ';
                }
                
                if(sol.Sales_Order__r.License_To_Customer_Name2__c != null) {
                    concatName += sol.Sales_Order__r.License_To_Customer_Name2__c + ' ';
                }
                
                if(sol.Sales_Order__r.License_To_Customer_Name3__c != null) {
                    concatName += sol.Sales_Order__r.License_To_Customer_Name3__c + ' ';
                }
                
                if(sol.Sales_Order__r.License_To_Customer_Name4__c != null) {
                    concatName += sol.Sales_Order__r.License_To_Customer_Name4__c;
                } 
                       
                return concatName;              
            } 
            
            set; 
        }
    
        public String address { 
            get {
                String concatAddress = '';
                
                if(sol.Sales_Order__r.License_To_Address1__c != null) {
                    concatAddress += sol.Sales_Order__r.License_To_Address1__c + ' ';
                }
                
                if(sol.Sales_Order__r.License_To_Address2__c != null) {
                    concatAddress += sol.Sales_Order__r.License_To_Address2__c + ' ';
                }
                
                if(sol.Sales_Order__r.License_To_Address3__c != null) {
                    concatAddress += sol.Sales_Order__r.License_To_Address3__c + ' ';
                }
                
                if(sol.Sales_Order__r.License_To_Address4__c != null) {
                    concatAddress += sol.Sales_Order__r.License_To_Address4__c;
                } 
                       
                return concatAddress;               
            } 
            
            set; 
        }
        
        public ContractWrapper(Sales_Order_Line__c sol) {
            this.sol = sol;
        }
    }
    


    /**
    * Wrapper class for Quote Line Item so we can create variables for computed/ selected columns
    */
    public class QLIWrapper { 
 		public QuoteLineItem qli { get; set; }
 		
 		public Id priceBookEntryId { get; set; }
 		
 		public String product { get; set; }
 		
 		public String productFamily { get; set; }
 		
 		public List<SelectOption> skus {
 			get {
 				if(skus == null) {
 					skus = new List<SelectOption>();
 				}
 				
 				return skus;
 			}
 			set;
 		}
 		
 		public String selectedSKU {
 			get;
 			set;
 		}
 		
 		public QLIWrapper(QuoteLineItem qli, List<SelectOption> skus, String product, String productFamily) {
 			this.qli = qli;
 			this.skus = skus;
 			this.product = product;
 			this.productFamily = productFamily;
 		}  		
    }
}