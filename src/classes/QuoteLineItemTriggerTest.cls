@isTest
private class QuoteLineItemTriggerTest {

    @isTest(seealldata = true)
    static void testQuotesTaxUpdate() {
        Account account = new Account();
        account.Industry = 'Test industry';
        account.Name = 'Test';
        insert account;
        
        Pricebook2 standardPB = [select id from Pricebook2 where isStandard=true];
        
        PriceBook2 pb = new PriceBook2();
        pb.Name = 'test pricebook';
        insert pb;
        
        Product2 prod = new Product2();
        prod.Name = 'test product';
        prod.filemaker_external_id__c = 'test external id';
        prod.CurrencyIsoCode = 'USD';  
        prod.ProductCode = 'ABC';
        insert prod;
              
        PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = standardPB.Id, Product2Id = prod.Id, UnitPrice = 10000, IsActive = true, UseStandardPrice = false);
        insert standardPrice;
        
        PricebookEntry pbe = new PricebookEntry();
        pbe.Product2Id = prod.Id;
        pbe.Pricebook2Id = pb.Id;
        pbe.UnitPrice = 100;
        pbe.UseStandardPrice = false;
        pbe.IsActive=true;
        insert pbe;
                        
        Opportunity opportunity = new Opportunity();
        opportunity.AccountId = account.Id;
        opportunity.Name = 'TestOpp';
        opportunity.StageName = 'Qualification';
        opportunity.CloseDate = Date.today();
        insert opportunity;
        
        Quote q = new Quote();
        q.Name = 'TestQuote';
        q.OpportunityId = opportunity.Id;
        q.Pricebook2Id = pb.Id;
        insert q;
        
        List<QuoteLineItem> qlis = new List<QuoteLineItem>(); 
        
        QuoteLineItem qli = new QuoteLineItem();
        qli.QuoteId = q.Id;
        qli.Quantity = 5;
        qli.PricebookEntryId = pbe.Id;
        qli.UnitPrice = 50;
        qli.Tax__c = 5;
        qlis.add(qli);
        
        QuoteLineItem qli1 = new QuoteLineItem();
        qli1.QuoteId = q.Id;
        qli1.Quantity = 3;
        qli1.PricebookEntryId = pbe.Id;
        qli1.UnitPrice = 100;
        qli1.Tax__c = 10;
        qlis.add(qli1);
        
        insert qlis;
        
        Quote queriedQuote = [select Id, Tax from Quote where Id =:q.Id];
        
        system.assertEquals(15,queriedQuote.Tax,'Tax on quote must equals sum of Tax on quote line items'); 
        
    }
}