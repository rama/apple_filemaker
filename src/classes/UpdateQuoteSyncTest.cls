@isTest
private class UpdateQuoteSyncTest {

    static testMethod void TestSyncQuotes() {
        Account acc = new Account();
        acc.CurrencyIsoCode = 'USD';
        acc.Name = 'test account';
        acc.Industry = 'OTHR Other';
        insert acc;
        
        Opportunity opp1 = new Opportunity();
        opp1.AccountId = acc.Id;
        opp1.Name = 'first test opp';
        opp1.CloseDate = date.today();
        opp1.StageName = 'Evaluate';
        opp1.CurrencyIsoCode = 'USD';
        insert opp1;
        
        Quote q1 = new Quote();
        q1.OpportunityId = opp1.Id;
        q1.Name = 'first test quote';
        insert q1;
        
        opp1.SyncedQuoteId = q1.Id; 
        update opp1;
        
        Map<String,String> oppAndQuote = new Map<String,String>();
        oppAndQuote.put(opp1.Id,null);
        UpdateQuoteSync.syncQuotes(oppAndQuote);
        
        System.assertEquals(1,Limits.getFutureCalls());
        
    }
}