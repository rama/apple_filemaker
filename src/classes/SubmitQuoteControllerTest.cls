@isTest
private class SubmitQuoteControllerTest {

    static testMethod void testSubmitQuoteController() {
        Account acc = new Account();
        acc.CurrencyIsoCode = 'USD';
        acc.Industry = 'Test';
        acc.Name = 'test account';
        insert acc;
        
        Opportunity opp1 = new Opportunity();
        opp1.AccountId = acc.Id;
        opp1.Name = 'first test opp';
        opp1.CloseDate = date.today();
        opp1.StageName = 'Evaluate';
        opp1.CurrencyIsoCode = 'USD';
        insert opp1;
        
        Quote q1 = new Quote();
        q1.OpportunityId = opp1.Id;
        q1.Name = 'first test quote';
        q1.Close_Date__c = System.today();
        insert q1;

       Test.startTest();
       Test.setCurrentPage(Page.SubmitQuotePage);        
       
       SubmitQuoteController ctlr = new SubmitQuoteController(new ApexPages.StandardController(q1));
       ctlr.changeStatus();
              
       System.assertEquals(ctlr.errorMessage, 'You are only allowed to submit a quote that is ready to be Sent to Customer. This page will be automatically redirected to Quote page in 10 seconds.');
       
       q1.Status = 'eAccept Completed';
	   q1.Prorated_Maintenance__c = true;		       
       update q1;
       
       ctlr = new SubmitQuoteController(new ApexPages.StandardController(q1));
       ctlr.changeStatus();
              
       System.assertNotEquals(ctlr.errorMessage, null); 
       
	   q1.Status = 'Exception Authorized';
	   q1.Prorated_Maintenance__c = true;		       
       update q1;
       
       ctlr = new SubmitQuoteController(new ApexPages.StandardController(q1));
       ctlr.changeStatus();
              
       System.assertNotEquals(ctlr.errorMessage, null);            
    }
}