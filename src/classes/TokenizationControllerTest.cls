@isTest
private class TokenizationControllerTest {

    static testMethod void controllerTest() {
        Account acc = new Account();
        acc.CurrencyIsoCode = 'USD';
        acc.Name = 'test account';
        acc.Industry = 'Test';
        insert acc;
        
        Opportunity opp1 = new Opportunity();
        opp1.AccountId = acc.Id;
        opp1.Name = 'first test opp';
        opp1.CloseDate = date.today();
        opp1.StageName = 'Evaluate';
        opp1.CurrencyIsoCode = 'USD';
        insert opp1;
        
       Test.startTest();
       Test.setCurrentPage(Page.Tokenization);        
       
       TokenizationController ctlr = new TokenizationController(new ApexPages.StandardController(opp1));
       ctlr.cancel();
       
       System.assertEquals(ctlr.errorMessage, 'Bill To Contact on the Opportunity cannot be blank. This page will be automatically redirected to Opportunity page in 10 seconds.');
       System.assert(ctlr.errorPanel);
    }
    
    static testMethod void controllerWithLicenseTest() {
        Contact c = new Contact();
        c.FirstName = 'Test';
        c.LastName = 'Test';     
        insert c;
    
        Account acc = new Account();
        acc.CurrencyIsoCode = 'USD';
        acc.Name = 'test account';
        acc.Industry = 'Test';
        insert acc;
        
        Opportunity opp1 = new Opportunity();
        opp1.AccountId = acc.Id;
        opp1.Name = 'first test opp';
        opp1.CloseDate = date.today();
        opp1.StageName = 'Evaluate';
        opp1.CurrencyIsoCode = 'USD';
        opp1.Bill_To_Contact__c = c.Id;
        insert opp1;
        
      
       Test.startTest();
       Test.setCurrentPage(Page.Tokenization);        
       
       TokenizationController ctlr = new TokenizationController(new ApexPages.StandardController(opp1));
       ctlr.cancel();
       
       System.assert(ctlr.errorMessage.contains('This page will be automatically redirected to Opportunity page in 10 seconds.'));
       System.assert(ctlr.errorPanel);
        
       String value = ctlr.sanitize('test');
       System.assertEquals(value, 'test'); 
        
       System.assertEquals(ctlr.oppMailingStreet, ''); 
    }    
}