// NOTE: SeeAllData=true is necessary to "see" the data in the standard price book.
@isTest(SeeAllData=true)
private class AddProductControllerTest {

	static testMethod void testProductFamilyMissing() {
		AddProductController controller = new AddProductController(new ApexPages.StandardController(new Opportunity()));
		controller.productType = 'NEW';
		PageReference addProductsPage = controller.addProduct();
		
		System.assertEquals(null, addProductsPage);
		System.assertEquals('Please select Product Family and Product Type values.', ApexPages.getMessages().get(0).getDetail());
	}
	
	static testMethod void testProductTypeMissing() {
		AddProductController controller = new AddProductController(new ApexPages.StandardController(new Opportunity()));
		controller.productFamily = 'TEST';
		PageReference addProductsPage = controller.addProduct();
		
		System.assertEquals(null, addProductsPage);
		System.assertEquals('Please select Product Family and Product Type values.', ApexPages.getMessages().get(0).getDetail());
	}

	static testMethod void testProductFamilyAndTypeOptions() {
		AddProductController controller = new AddProductController(new ApexPages.StandardController(new Opportunity()));
		
		// Validate Product Families.
		List<SelectOption> productFamilyOptions = controller.productFamilyOptions;
		System.assertNotEquals(null, productFamilyOptions);
		
		// Validate Product Types.
		List<SelectOption> productTypeOptions = controller.productTypeOptions;
		System.assertEquals('NEW', productTypeOptions[0].getValue());
		System.assertEquals('MNT', productTypeOptions[1].getValue());
		System.assertEquals('UPG', productTypeOptions[2].getValue());
	}

	static testMethod void testOpportunityControllerProductTypeNew() {
		Account account = new Account();
		account.Name = 'Test';
		account.Group__c = 'HDIR';
		account.Industry = 'FIN  Finance';
		insert account;
		
		Opportunity opportunity = new Opportunity();
		opportunity.AccountId = account.Id;
		opportunity.Name = 'Test';
		opportunity.StageName = 'Prospecting';
		opportunity.CloseDate = Date.today().addDays(1);
		insert opportunity;

		AddProductController controller = new AddProductController(new ApexPages.StandardController(opportunity));
		controller.productFamily = 'TEST';
		controller.productType = 'NEW';
		PageReference addProductsPage = controller.addProduct();

		System.assertEquals(true, addProductsPage.getUrl().contains('PricebookEntryfval2=SLE%2CSBE'));
	}

	static testMethod void testOpportunityControllerProductTypeMaintenance() {
		Account account = new Account();
		account.Name = 'Test';
		account.Group__c = 'KDIR';
		account.Industry = 'FIN  Finance';
		insert account;
		
		Opportunity opportunity = new Opportunity();
		opportunity.AccountId = account.Id;
		opportunity.Name = 'Test';
		opportunity.StageName = 'Prospecting';
		opportunity.CloseDate = Date.today().addDays(1);
		insert opportunity;

		AddProductController controller = new AddProductController(new ApexPages.StandardController(opportunity));
		controller.productFamily = 'TEST';
		controller.productType = 'MNT';
		PageReference addProductsPage = controller.addProduct();

		System.assertEquals(true, addProductsPage.getUrl().contains('PricebookEntryfval2=SLE%2CSBE%2CMNT'));
	}
	
	static testMethod void testOpportunityControllerProductTypeUpgrade() {
		Account account = new Account();
		account.Name = 'Test';
		account.Group__c = 'NDIR';
		account.Industry = 'FIN  Finance';
		insert account;
		
		Opportunity opportunity = new Opportunity();
		opportunity.AccountId = account.Id;
		opportunity.Name = 'Test';
		opportunity.StageName = 'Prospecting';
		opportunity.CloseDate = Date.today().addDays(1);
		insert opportunity;

		AddProductController controller = new AddProductController(new ApexPages.StandardController(opportunity));
		controller.productFamily = 'TEST';
		controller.productType = 'UPG';
		PageReference addProductsPage = controller.addProduct();

		System.assertEquals(true, addProductsPage.getUrl().contains('PricebookEntryfval2=SEU'));
	}
	
	static testMethod void testQuoteControllerProductTypeNew() {
		Account account = new Account();
		account.Name = 'Test';
		account.Group__c = 'HOME';
		account.Industry = 'FIN  Finance';
		insert account;
		
		Opportunity opportunity = new Opportunity();
		opportunity.AccountId = account.Id;
		opportunity.Name = 'Test';
		opportunity.StageName = 'Prospecting';
		opportunity.CloseDate = Date.today().addDays(1);
		insert opportunity;

		Quote quote = new Quote();
		quote.OpportunityId = opportunity.Id;
		quote.Name = 'Test';
		insert quote;

		AddProductController controller = new AddProductController(new ApexPages.StandardController(quote));
		controller.productFamily = 'TEST';
		controller.productType = 'NEW';
		PageReference addProductsPage = controller.addProduct();

		System.assertEquals(true, addProductsPage.getUrl().contains('PricebookEntryfval2=SL%2CSBA'));
	}

	static testMethod void testQuoteControllerProductTypeMaintenance() {
		Account account = new Account();
		account.Name = 'Test';
		account.Group__c = 'HOME';
		account.Industry = 'FIN  Finance';
		insert account;
		
		Opportunity opportunity = new Opportunity();
		opportunity.AccountId = account.Id;
		opportunity.Name = 'Test';
		opportunity.StageName = 'Prospecting';
		opportunity.CloseDate = Date.today().addDays(1);
		insert opportunity;

		Quote quote = new Quote();
		quote.OpportunityId = opportunity.Id;
		quote.Name = 'Test';
		insert quote;

		AddProductController controller = new AddProductController(new ApexPages.StandardController(quote));
		controller.productFamily = 'TEST';
		controller.productType = 'MNT';
		PageReference addProductsPage = controller.addProduct();

		System.assertEquals(true, addProductsPage.getUrl().contains('PricebookEntryfval2=SL%2CSBA%2CMNT'));
	}
	
	static testMethod void testQuoteControllerProductTypeUpgrade() {
		Account account = new Account();
		account.Name = 'Test';
		account.Group__c = 'HOME';
		account.Industry = 'FIN  Finance';
		insert account;
		
		Opportunity opportunity = new Opportunity();
		opportunity.AccountId = account.Id;
		opportunity.Name = 'Test';
		opportunity.StageName = 'Prospecting';
		opportunity.CloseDate = Date.today().addDays(1);
		insert opportunity;

		Quote quote = new Quote();
		quote.OpportunityId = opportunity.Id;
		quote.Name = 'Test';
		insert quote;

		AddProductController controller = new AddProductController(new ApexPages.StandardController(quote));
		controller.productFamily = 'TEST';
		controller.productType = 'UPG';
		PageReference addProductsPage = controller.addProduct();

		System.assertEquals(true, addProductsPage.getUrl().contains('PricebookEntryfval2=SLU'));
	}

}