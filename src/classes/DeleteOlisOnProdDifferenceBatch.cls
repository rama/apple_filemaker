global class DeleteOlisOnProdDifferenceBatch implements Database.Batchable<Sobject> {
	
	private boolean isTestRunning = false;
	
	public DeleteOlisonProdDifferenceBatch() {
		this(false);
	}
	
	public DeleteOlisonProdDifferenceBatch(boolean isTest) {
		IsTestRunning = isTest;
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC){
     
     	//select Sales order records for this batch that are not reconciled yet and satisfied a few conditions
     	string query = 'SELECT Id, Opportunity__c,Channel__c,CurrencyIsoCode, Price_Book__c, Price_Book__r.Name FROM Sales_Order__c WHERE Status__c != \'Reconciled\' and Order_Type__c != \'MS\' and Opportunity__c != null and Price_Book__c != null';
     	return Database.getQueryLocator(query);
   	}

   	global void execute(Database.BatchableContext BC, List<Sales_Order__c> scope){
		system.debug('scope size is:' + scope.size());
		autoReconcile(scope);
	}

   	global void finish(Database.BatchableContext BC){
   		//CreateNewOLIsFromSOLsBatch batch = new CreateNewOLIsFromSOLsBatch(isTestRunning);
   		//Database.ExecuteBatch(batch);
   	}
   	
   	public void autoReconcile(List<Sales_Order__c> salesOrders) {
   		if(salesOrders != null) {
   			system.debug('sales orders that need to be reconciled '+ salesorders.size());  						   			
   			Map<Id,string> oppChatterMessages = new Map<Id,string>();
   			Map<Id,string> SalesOrderChatterMessages = new Map<Id,string>();
   			List<Sales_Order__c> validSalesOrders = new List<Sales_Order__c>();
   			Map<Id,Sales_Order__c> validSOMap = new Map<Id,Sales_Order__c>();
   			Map<Id,List<Sales_Order_Line__c>> allSOLines = getAllSalesOrderLines(salesOrders);  
   			Map<Id,Map<Id,string>> allPricebookEntries =  getPricebookEntryIds(salesOrders, allSOLines);
   			Map<Id,List<OpportunityLineItem>> allOppLineItems = getAllOpportunityLineItems(salesOrders);
   			   			   			
   			//Before proceeding with any business logic and updating opps and OLIs, make sure product on sales order line is validfor salesorder's pricebook 
   			List<Sales_Order__c> sosToUpdate = new List<Sales_Order__c>();
   			for(Sales_Order__c so: salesOrders) {
   				List<Sales_Order_Line__c> sols = allSOLines.get(so.Id);
   				List<Sales_Order_Line__c> invalidPBSols = new List<Sales_Order_Line__c>();
   				List<Sales_Order_Line__c> invalidQtySols = new List<Sales_Order_Line__c>();
   				List<Sales_Order_Line__c> invalidOwnerSols = new List<Sales_Order_Line__c>();
   				Map<Id,String> solPBEntries = allPricebookEntries.get(so.Id);
   				for(Sales_Order_Line__c sol : sols) {   					
   					string pbe = solPBEntries.get(sol.Id);
   					if(string.IsBlank(pbe)) {
   						invalidPBSols.add(sol);
   					}
   					else if(sol.Quantity__c == null || sol.Quantity__c == 0) {
   						invalidQtySols.add(sol);
   					} 
   					else if(sol.Owner__r.IsActive == false) {
   						invalidOwnerSols.add(sol);
   					}	   								
   				}
   				//based on the condition that made this sales order reconciliation fail, update the chatter feed
   				if(invalidPBSols.size() > 0 ) {
   					so.Status__c = 'Automatic Reconciliation Failed';
   					//Build a chatter post on the Sales Order
   					system.debug('pb problem');
   					string failureMessage =  '';
   					for(Sales_Order_Line__c sol : invalidPBSols) {
   						failureMessage += 'Reconciliation failed because the ' + so.Price_Book__r.Name + ' Price Book does not contain an entry for ' + sol.Product_ID_SKU__r.Name;
   					}
   					SalesOrderChatterMessages.put(so.Id, failureMessage); 
   					sosToUpdate.add(so);					
   				}
   				else if(invalidQtySols.size() > 0) {
   					system.debug('quantity problem');
   					so.Status__c = 'Automatic Reconciliation Failed';
   					string failureMessage = 'Reconciliation error:  Cannot update Opportunity Products when Quantity on one or more Sales Order Lines is 0 or null.';
   					SalesOrderChatterMessages.put(so.Id,failureMessage);
   					oppChatterMessages.put(so.Opportunity__c, failureMessage);
   					sosToUpdate.add(so);
   				}
   				else if(invalidOwnerSols.size() > 0) {
   					system.debug('owner issue');
   					so.Status__c = 'Automatic Reconciliation Failed';
   					string failureMessage = 'Please change the Sales Order Line Owner(s) to an Active User.';
   					SalesOrderChatterMessages.put(so.Id, failureMessage);
   					sosToUpdate.add(so);
   				}
   				//If there are no issues with sales order and its lines, proceed with the program execution 
   				else {
   					validSalesOrders.add(so);
   					validSoMap.put(so.Id,so);
   				}
   			}
   			if(sosToUpdate.size() > 0) {
   				system.debug('updating sales orders');
   				update sosToUpdate;
   			}
   			
   			if(validSalesOrders.size() > 0) {
   				
   				Map<Id,Opportunity> soIdToOpportunityMap = getRelatedOpportunities(salesOrders); 			
	   			List<Id> oppIdsWithChangedPbs = new List<Id>(); 
	   			List<OpportunityLineItem> olisTobeDeleted = new List<OpportunityLineItem>();
	   			Map<Id,Sales_Order__c> oppsTobeUpdatedWithSO = new Map<Id, Sales_Order__c>();
	   			List<Opportunity> oppstoUpdate = new List<Opportunity>();
	   			Map<Id,List<Sales_Order_Line__c>> solsToCreateNewOlis = new Map<Id,List<Sales_Order_Line__c>>(); 
   			
	   			for(Sales_Order__c so : validSalesOrders) {   				
	   				Opportunity opp = soIdToOpportunityMap.get(so.Id);
	   				//Verify if the pricebook has changed external to salesforce
	   				if(so.Price_Book__c == opp.Pricebook2Id ) {
	   					system.debug('no price book change for this so:' + so.Id);
	   					//As the price book hasn't changed, no specific action is necessary and we can proceed to comparing line items
	   				}
	   				else {
	   					//As the pricebook has changed external to salesforce, delete all opplineitems on the opp, update the pricebook and recreate olis from sols
	   					oppIdsWithChangedPbs.add(opp.Id);
	   					oppsTobeUpdatedWithSO.put(opp.Id, so);
	   					oppstoUpdate.add(opp);
	   				}
	   			}
	   			
	   			system.debug('number of opps to update as a result of external change:' + oppstoUpdate.size());
	   			//If there are any opps that has pricebook change external to salesforce, deal with them.
	   			if(oppIdsWithChangedPbs.size() > 0) {
	   				
	   				//Delete all opportunity line items from the opps whose pricebook has changed external to salesforce
	   				olisToBeDeleted = [Select Id, OpportunityId, Quantity, UnitPrice, PricebookEntry.Name from OpportunityLineItem where OpportunityId in : oppIdsWithChangedPbs];
	   				for(OpportunityLineItem oli : olisToBeDeleted) {
	   					 string newMessage = 'Delete Line Item for ' + oli.Quantity + oli.PricebookEntry.Name  + ' at ' + oli.UnitPrice + '\n';
	   					 string failureMessage = buildFailureMessage(newMessage, oli.OpportunityId, OppChatterMessages);
	   					 oppChatterMessages.put(oli.OpportunityId, failureMessage);
	   				}
	   				
	   				if(olisTobeDeleted != null && olisToBeDeleted.size() > 0) {
	   					delete olisToBeDeleted;
	   				}
	   				
	   				//Update the pricebook on the opportunities to match that of corresponding sales orders
	   				for(Opportunity opp: oppstoUpdate) {
	   					Sales_Order__c so = oppsToBeUpdatedWithSO.get(opp.Id);
	   					if(so != null) {
	   						system.debug('opps pricebookid is being changed from ' + opp.Pricebook2Id + ' to ' + so.Price_Book__c );
	   						opp.Channel__c = so.Channel__c;
	   						opp.CurrencyIsoCode = so.CurrencyIsoCode;
	   						string newMessage = 'Reconciled with Price Book changed from ' + opp.PriceBook2.Name + ' to ' + so.Price_Book__r.Name +'. \n';
	   						string failureMessage = buildFailureMessage(newMessage, opp.Id, oppChatterMessages);
	   						oppChatterMessages.put(opp.Id, failureMessage);
	   						//List<Sales_Order_Line__c> sols = [Select ID, Sales_Order__c, filemaker_external_id__c, Product_ID_SKU__c, Quantity__c, Sales_Price__c from Sales_Order_Line__c where Sales_Order__c =: so.Id];
	   						List<Sales_Order_Line__c> sols = allSOLines.get(so.Id);
	   						solsToCreateNewOlis.put(so.Id,sols);
	   					}
	   				}   				
	   				update oppstoUpdate;  	
	   				 
	   				//Create new OLIs from SOLs
	   				List<Sales_Order_Line__c> solsToBeUpdated = new List<Sales_Order_Line__c>();
	   				for(Id oppId: oppsTobeUpdatedWithSO.keyset()) {   					
	   					Sales_Order__c so = oppsTobeUpdatedWithSO.get(oppId);
	   					if(so != null) {
	   						//Opportunity queriedopp = [select Id,Pricebook2Id from Opportunity where Id=: oppId limit 1];
	   						//system.debug('queriedopp pricebook is:' + queriedopp.Pricebook2Id);
	   						List<Sales_Order_Line__c> sols = solsToCreateNewOlis.get(so.Id);   						
	   						if(sols != null) {
	   							Map<Id,String> solPbMap = allPricebookEntries.get(so.Id);
	   							system.debug('sales order with id ' + so.Id + 'has ' + sols.size() + ' sols to create olis from ');
	   							for(Sales_Order_Line__c sol : sols) {
	   								if(sol.Quantity__c != null) {
	   									OpportunityLineItem oli = new OpportunityLineItem();
		   								
		   								string pbeId = solPbMap.get(sol.Id); 
		   								oli.PricebookEntryId = pbeId;
		   								system.debug('olis price book entry is in price book' + so.Price_Book__c);
		   								
		   								oli.Quantity = sol.Quantity__c;
		   								oli.UnitPrice = sol.Sales_Price__c; 
		   								oli.OpportunityId = oppId;
		   								oli.filemaker_external_id__c = sol.filemaker_external_id__c; 
		   								oli.Sales_Order_Line__c = sol.Id;	
		   								oli.Reconciliation_Details__c = 'Created by reconciliation when price book was changed.';	   								   									   								
		   								insert oli;
		   								system.debug('new oli inserted:' + oli.Id);
		   								string newMessage = 'Added Line Item for ' + sol.Quantity__c +' ' +oli.PricebookEntryId + ' at ' + sol.Sales_Price__c + '. \n';
		   								string failureMessage = buildFailureMessage(newMessage, oppId, oppChatterMessages);
		   								oppChatterMessages.put(oppId, failureMessage);
		   								sol.Opportunity_Line_Item_ID__c = oli.Id;
		   								solsToBeUpdated.add(sol);   
	   								}	   																
	   							}
	   						}
	   						else { system.debug('for so: ' + so.Id + 'no sols to create olis from'); }
	   					}
	   				}
	
					//update opportunity line item field on SOLs to correspond to newly inserted OLIs. 
	   				if(solsToBeUpdated != null) {
	   					update solsToBeUpdated;	
	   				}  				   					
	   			}
	   			
	   			//Compare SalesOrderlines with OpportunityLineItemsMap
	   			//Map<Id,OpportunityLineItem> perfectMatchesMap = new Map<Id, OpportunityLineItem>();
	   			//Map<Id, OpportunityLineItem> quantityDifferedMap = new Map<Id,OpportunityLineItem>();
	   			//Map<Id,OpportunityLineItem> priceDifferedMap = new Map<Id,OpportunityLineItem>();
	   			//Map<Id, OpportunityLineItem> bothPriceandQtyDiffered = new Map<Id, OpportunityLineItem>();
	   			Map<Id, Sales_Order_Line__c> solsWithNoMatches = new Map<Id, Sales_Order_Line__c>();
	   			
	   			Set<Id> OlisWithMatches = new Set<Id>();
	   			
	   			List<OpportunityLineItem> olisToDelete = new List<OpportunityLineItem>();
	   			List<OpportunityLineItem> olisToUpdate = new List<OpportunityLineItem>();
	   			
	   			for(Sales_Order__c so: validsalesOrders) {	   				 			
	   				List<OpportunityLineItem> olis = allOppLineItems.get(so.Opportunity__c);
	   				List<Sales_Order_Line__c> sols = allSOlines.get(so.Id);
	   				Opportunity opp = soIdToOpportunityMap.get(so.Id);
	   				
	   				if(olis != null && sols != null) {
	   					Map<Id,String> solPbMap = allPricebookEntries.get(so.Id);
	   					for(Sales_Order_Line__c sol : sols) {
	   					//for(Id solId : sols.keyset()) {
	   						//Sales_Order_Line__c sol = sols.get(solId);
	   						system.debug('sols product id is:' + sol.Product_ID_SKU__c);
	   						system.debug('sos price book id is:' + so.Price_Book__c);		
	   						string pbeId = 	solPbMap.get(sol.Id);
	   						Set<Id> solIdsWithMatches = new Set<Id>();
		   					//for(Id oliId : olis.keyset()) {
		   					for(OpportunityLineItem oli: olis ) {
		   						//OpportunityLineItem oli = olis.get(oliId);
		   						
		   							if(pbeId == oli.PricebookEntryId) {
		   								system.debug(' theres a product match for sol ' + sol.Id + 'with an oli ' +oli.Id);
		   								olisWithMatches.add(oli.Id);
		   								boolean quantityMatched = (sol.Quantity__c == oli.Quantity);
		   								boolean priceMatched = (sol.Sales_Price__c == oli.ListPrice);
		   								if(quantityMatched && priceMatched) {
		   									//perfectMatchesMap.put(sol.Id, oli);
		   									solIdswithMatches.add(sol.Id);
		   									oli.Sales_Order_Line__c = sol.Id;
		   									olisToUpdate.add(oli);
		   								} 
		   								else if(quantityMatched && !priceMatched) {
		   									//priceDifferedMap.put(sol.Id, oli);
		   									solIdswithMatches.add(sol.Id);
		   									oli.Reconciliation_Details__c = 'Sales Price changed from ' + oli.UnitPrice + ' to ' + sol.Sales_Price__c;
		   									oli.UnitPrice = sol.Sales_Price__c;
		   									oli.Sales_Order_Line__c = sol.Id;
		   									olisToUpdate.add(oli);
		   									
		   								}
		   								else if(!quantityMatched && priceMatched) {
		   									//quantityDifferedMap.put(sol.Id, oli);
		   									solIdswithMatches.add(sol.Id);
		   									oli.Reconciliation_Details__c = 'Quantity changed from ' + oli.Quantity  + ' to ' + sol.Quantity__c ;
		   									oli.Quantity = sol.Quantity__c;
		   									oli.Sales_Order_Line__c = sol.Id;	   									
		   									olisToUpdate.add(oli);
		   									
		   								}
		   								else {
		   									//bothPriceandQtyDiffered.put(sol.Id, oli);
		   									solIdswithMatches.add(sol.Id);
		   									oli.Reconciliation_Details__c = 'Quantity changed from ' + oli.Quantity  + ' to ' + sol.Quantity__c + '. Sales Price changed from ' + oli.UnitPrice + ' to ' + sol.Sales_Price__c;
		   									oli.Sales_Order_Line__c = sol.Id;
		   									oli.Quantity = sol.Quantity__c;
		   									oli.UnitPrice = sol.Sales_Price__c;
		   									olisToUpdate.add(oli);
		   									
		   								}
		   							}
		   								
		   						
		   							
		   					}	
		   					if (!solIdsWithMatches.contains(sol.Id)) {
		   						system.debug('sol with this is does not have a matching oli' + sol.Id);
		   						solsWithNoMatches.put(sol.Id, sol);
		   					}   						
	   					}
	   				 	for(OpportunityLineItem oli : olis) {
	   				 		if(!OlisWithMatches.contains(oli.Id) ) {
	   				 			system.debug('oli has to be deleted ' + oli.Id );
	   				 			olisToDelete.add(oli);
	   				 		}
	   				 	}  					
	   				}
	   				
	   			}
	   			if(olisToUpdate.size() > 0) {
	   				system.debug('number of olis being updateded: ' + olisToUpdate.size());
	   				update olisToUpdate;	
	   			}
	   			
	   			//handle non product matches
	   			//delete the odious Opportunity line items
	   			if(olisToDelete.size() > 0) {
	   				system.debug('number of olis being deleted ' + olisToDelete.size());
	   				delete olisToDelete;
	   			}
	   			
	   			// Add new Opportunity Line Items from sales order lines   			  						
	   			if(solsWithNoMatches.size() > 0) {   				 							
	   				List<Sales_Order_Line__c> solstoUpdate = new List<Sales_Order_Line__c>();
	   				for(Id solId : solsWithNoMatches.keyset()) {
	   					Sales_Order_Line__c sol = solsWithNoMatches.get(solId);
	   					Sales_Order__c so = validSoMap.get(sol.Sales_Order__c);
	   					Map<Id,String> solPbMap = allPricebookEntries.get(so.Id);
	   					string pbeId;
	   					if(sol.Quantity__c != null && so.Opportunity__c != null) {
			   				OpportunityLineItem oli = new OpportunityLineItem();
				   			
				   			oli.PricebookEntryId = pbeId;
				   			system.debug('olis price book entry is in price book' + so.Price_Book__c);
				   			
				   			oli.Quantity = sol.Quantity__c;
				   			oli.UnitPrice = sol.Sales_Price__c; 
				   			oli.OpportunityId = so.Opportunity__c;
				   			oli.filemaker_external_id__c = sol.filemaker_external_id__c; 
				   			oli.Sales_Order_Line__c = sol.Id;	
				   			oli.Reconciliation_Details__c = 'Record added by reconciliation process';	   								   									   								
				   			insert oli;
				   			system.debug('new oli inserted:' + oli.Id);
				   			sol.Opportunity_Line_Item_ID__c = oli.Id;
				   			solstoUpdate.add(sol);   
	   					}   																
	   				}
	   				
	   				if(solstoUpdate.size() > 0) {
	   					update solstoUpdate;
	   				}
	   			}
	   			List<Sales_Order__c> sosReconciled = new List<Sales_Order__c>();
	   			List<Opportunity> oppsReconciled = new List<Opportunity>();
	   			for(Sales_Order__c so: validsalesOrders ) {
	   				if(so.Opportunity__c != null) {
	   					so.Status__c = 'Reconciled';
	   					sosReconciled.add(so);
	   					Opportunity opp = soIdToOpportunityMap.get(so.Id);
	   					opp.Reconciled__c = true;
	   					oppsReconciled.add(opp);
	   				}
	   			}
	   			
   				update sosReconciled;
   				update oppsReconciled;   			
   			}
   			
   			List<FeedItem> feedItems = buildFeedItems(salesOrderChatterMessages);
   			feedItems.addAll(buildFeedItems(OppChatterMessages));
   			
   			insert feedItems;
   			   			
   		}
   		else { system.debug('sales orders collection is null'); }
   	} 
   	
   	
   	public List<FeedItem> buildFeedItems(Map<Id,String> msgMap) {
   		List<FeedItem> feedItems = new List<FeedItem>();
   		for(Id objId : msgMap.keyset()) {
   			FeedItem fi = new FeedItem();
   			fi.ParentId = objId;
   			fi.body = msgMap.get(objId);
   			feedItems.add(fi);
   		}
   		return feedItems;
   	}
   	
   	public Map<Id,Map<Id,string>> getPricebookEntryIds(List<Sales_Order__c> sos, Map<Id,List<Sales_Order_Line__c>> solsMap) {
   		
   		Map<Id,Map<Id,String>> result = new Map<Id,Map<Id,String>>();
   		Map<Id,Sales_Order__c> soMap = new Map<Id,Sales_Order__c>();
   		Set<Id> pbIds = new Set<Id>();
   		for(Sales_Order__c so : sos) {
   			if(so.Price_Book__c != null) {
   				pbIds.add(so.Price_Book__c);
   			}
   			soMap.put(so.Id, so);
   		}   		
   		List<PricebookEntry> pbes = [select Id, Product2Id, Pricebook2Id from PricebookEntry where Pricebook2Id in :pbIds and isactive = true];
   		
   		for(Id soId : solsMap.keyset()) {
   			Sales_Order__c so = soMap.get(soId);
   			Map<Id, string> solPbeMap = new Map<Id,string>();
   			List<Sales_Order_Line__c> sols = solsMap.get(soId);
   			for(Sales_Order_Line__c sol : sols) {
   				string pbeId;
   				for(PricebookEntry pbe : pbes) {
   					if(pbe.Pricebook2Id == so.Price_Book__c && pbe.Product2Id == sol.Product_ID_SKU__c ) {
   						pbeId = pbe.Id;
   						break;
   					}
   				}
   				solPbeMap.put(sol.Id,pbeId);
   			}
   			result.put(soId,solPbeMap);
   		}
   		return result;
   	}
   		
   	public String buildFailureMessage(string newMessage, Id parentId, Map<Id,String> msgMap) {
   		string result;
   		if(msgMap.containsKey(parentId)) {
	   		string currentMessage = msgMap.get(parentId);
	   		if(!String.IsBlank(currentMessage)) {
	   			result = currentMessage + newMessage;	 		
	   		}
	   	}
	   	if(String.IsBlank(result)) {
	   		result = newMessage;
	   	}
	   	return result;
   	}
   	
   	public Map<Id, Opportunity> getRelatedOpportunities(List<Sales_Order__c> sos) {
   		Map<Id,Opportunity> soIdToOpportunityMap = new Map<Id, Opportunity>();
   		Set<Id> soOppIds = new Set<Id>();
   		for(Sales_Order__c so: sos) {
   			soOppIds.add(so.Opportunity__c);
   		}
   		Map<Id, Opportunity> opps = new Map<Id,Opportunity>([select Id, Pricebook2Id, Pricebook2.Name from Opportunity where Id in :soOppIds]);   		
   		for(Sales_Order__c so: sos) {
   			if(so.Opportunity__c != null) {
   				soIdToOpportunityMap.put(so.Id, opps.get(so.Opportunity__c));
   			}
   		}
   		return soIdToOpportunityMap;
   	}
   	
   	public Map<Id,OpportunityLineItem> getOpportunityLineItems(string oppId) {
   		Map<Id, OpportunityLineItem> result = new Map<Id,OpportunityLineItem>();
   		List<OpportunityLineItem> olis = [select Id, PricebookEntryId, Quantity, ListPrice, UnitPrice from OpportunityLineItem where OpportunityId =: oppId];
   		if(olis != null) {
   			for(OpportunityLineItem oli: olis) {
   				result.put(oli.Id, oli);
   			}
   		}
   		return result;
   	}
   	
   	public Map<Id,Sales_Order_Line__c> getSalesOrderLines(string soId) {
   		Map<Id, Sales_Order_Line__c> result = new Map<Id, Sales_Order_Line__c>(); 		
   		List<Sales_Order_Line__c> sols = [select Id, Product_ID_SKU__c, Quantity__c, Sales_Price__c, filemaker_external_id__c, Sales_Order__c, Product_ID_SKU__r.Name from Sales_Order_Line__c where Sales_Order__c =: soId];
   		for(Sales_Order_Line__c sol: sols) {
   			result.put(sol.Id, sol);
   		}
   		return result;
   	}
   	
   	public Map<Id,List<OpportunityLineItem>> getAllOpportunityLineItems(List<Sales_Order__c> salesOrders) {
   		Map<Id,List<OpportunityLineItem>> oppOlisMap = new Map<Id,List<OpportunityLineItem>>();
   		Set<Id> oppIds = new Set<Id>();
   		for(Sales_Order__c so : salesOrders) {
   			if(so.Opportunity__c != null) {
   				oppIds.add(so.Opportunity__c);	
   			}	
   		} 
   		
   		List<OpportunityLineItem> olis = [select Id, PricebookEntryId, Quantity, ListPrice, UnitPrice, OpportunityId from OpportunityLineItem where OpportunityId in :oppIds];
   		if(olis != null) {
   			for(OpportunityLineItem oli : olis) {
   				if(oppOlisMap.containsKey(oli.OpportunityId)) {
   					List<OpportunityLineItem> lineItems = oppOlisMap.get(oli.OpportunityId);
   					lineItems.add(oli);
   					oppOlisMap.put(oli.OpportunityId, lineItems);
   				}
   				else {
   					List<OpportunityLineItem> lineItems = new List<OpportunityLineItem>();
   					lineItems.add(oli);
   					oppOlisMap.put(oli.OpportunityId, lineItems);
   				}
   			}	
   		}
   		return oppOlisMap;
   	}
   	
   	public Map<Id,List<Sales_Order_Line__c>> getAllSalesOrderLines(List<Sales_Order__C> salesOrders) {
   		Map<Id,List<Sales_Order_Line__c>> solMap = new Map<Id,List<Sales_Order_line__c>> ();
   		Set<Id> salesOrderIds = new Set<Id>();
   		for(Sales_Order__c so : salesOrders) {
   			salesOrderIds.add(so.Id);
   		}	
   		List<Sales_Order_Line__c> sols = [select Id, Product_ID_SKU__c, Quantity__c, Sales_Price__c, filemaker_external_id__c, Sales_Order__c, Owner__r.IsActive, Product_ID_SKU__r.Name  from Sales_Order_Line__c where Sales_Order__c in :salesOrderIds];
   		if(sols != null) {
   			for(Sales_Order_Line__c sol : sols) {
   				if(solMap.containsKey(sol.Sales_Order__c)) {
   					List<Sales_Order_Line__c> newsol =solMap.get(sol.Sales_Order__c);
   					newsol.add(sol);
   					solMap.put(sol.Sales_Order__c, newsol);
   				}
   				else {
   					List<Sales_Order_Line__c> newsol = new List<Sales_Order_Line__c>();
   					newsol.add(sol);
   					solMap.put(sol.Sales_Order__c, newsol);
   				}
   			}
   		}
   		return solMap;
   	}
}