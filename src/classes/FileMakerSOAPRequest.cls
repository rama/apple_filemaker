public class FileMakerSOAPRequest {
	
	//TODO take it to custom settings as we have different URL's for different environments
	public static String endPoint = 'https://esttaxdev.filemaker.com/axis2/services/FileMakerEstimatedTaxService';
	
    public static String callFileMakerService(){

		DateTime asOfNow = getGMT(DateTime.now());
    	String asOfNowFormatted = asOfNow.format('YYYY-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
		String laterFormatted = asOfNow.addMinutes(5).format('YYYY-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
		//TODO add it to custom settings
		String password = 'p$wB6!FEqE';     	

        String xml = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">  <soapenv:Header>    <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" soapenv:mustUnderstand="1">      <wsu:Timestamp wsu:Id="TS-2">        <wsu:Created>' + asOfNowFormatted + '</wsu:Created>        <wsu:Expires>' + laterFormatted + '</wsu:Expires>      </wsu:Timestamp>      <wsse:UsernameToken wsu:Id="UsernameToken-1">        <wsse:Username>apache</wsse:Username>        <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">' + password + '</wsse:Password>      </wsse:UsernameToken>    </wsse:Security>  </soapenv:Header>  <soapenv:Body>    <ns1:FMI_GET_EXTIMATED_TAX xmlns:ns1="http://tempuri.org/">      <I_LINE>        <item>          <MATERIAL>TTL/A23455</MATERIAL>          <EXTENDED_AMOUNT>0005500</EXTENDED_AMOUNT>          <COUNTRY>US</COUNTRY>          <STATE>CA</STATE>          <COUNTY>ALAMEDA</COUNTY>          <CITY>UNION CITY</CITY>          <ZIPCODE>94587</ZIPCODE>          <ZIPCODE_EXTN>0000</ZIPCODE_EXTN>        </item>      </I_LINE>    </ns1:FMI_GET_EXTIMATED_TAX>  </soapenv:Body></soapenv:Envelope>';
        //String xml = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">  <soapenv:Header>    <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" soapenv:mustUnderstand="1">      <wsu:Timestamp wsu:Id="TS-2">        <wsu:Created>2013-10-15T09:33:49.594Z</wsu:Created>        <wsu:Expires>2013-10-15T09:35:49.594Z</wsu:Expires>      </wsu:Timestamp>      <wsse:UsernameToken wsu:Id="UsernameToken-1">        <wsse:Username>apache</wsse:Username>        <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">p$wB6!FEqE</wsse:Password>      </wsse:UsernameToken>    </wsse:Security>  </soapenv:Header>  <soapenv:Body>    <ns1:FMI_GET_EXTIMATED_TAX xmlns:ns1="http://tempuri.org/">      <I_LINE>        <item>          <MATERIAL>TTL/A23455</MATERIAL>          <EXTENDED_AMOUNT>0005500</EXTENDED_AMOUNT>          <COUNTRY>US</COUNTRY>          <STATE>CA</STATE>          <COUNTY>ALAMEDA</COUNTY>          <CITY>UNION CITY</CITY>          <ZIPCODE>94587</ZIPCODE>          <ZIPCODE_EXTN>0000</ZIPCODE_EXTN>        </item>      </I_LINE>    </ns1:FMI_GET_EXTIMATED_TAX>  </soapenv:Body></soapenv:Envelope>';

        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setTimeout(120000);
        req.setMethod('POST');
        req.setHeader('SOAPAction','Retrieve');
        req.setHeader('Accept-Encoding','gzip,deflate');
        req.setHeader('Content-Type','text/xml;charset=UTF-8');
        req.setHeader('User-Agent','Jakarta Commons-HttpClient/3.1');
        req.setBody(xml);
 
        req.setEndpoint(endPoint);
 
        string bodyRes = '';
 
        try {
            HttpResponse res = h.send(req);
            bodyRes = res.getBody();
            } 
            catch(System.CalloutException e) {
            System.debug('Callout error: '+ + e.getMessage() + ' ' + e.getStackTraceString());
            //ApexPages.addMessage(new ApexPages.Message(Ap//exPages.Severity.FATAL, e.getMessage()));
            }
        System.debug('Soap request:' + xml);
        System.debug('Soap response:' + bodyRes);
		return bodyRes;
    }
    
 	public static Datetime getGMT(Datetime l)
    {    
        Date d = l.dateGmt();
        Time t = l.timeGmt();
        return Datetime.newInstance(d,t);
    }    
  }