global class UpdateQuoteSync {
	@future
	public static void syncQuotes(Map< String, string > mOppIdtoQuoteId)
	{
	    List < Opportunity > lOppsToUpdate = [SELECT Id,
	                                                SyncedQuoteId
	                                           FROM Opportunity
	                                          WHERE Id in:mOppIdToQuoteId.keySet()];
	    for ( Opportunity op :lOppsToUpdate)
	    {
	        op.SyncedQuoteId = mOppIdToQuoteId.get(op.Id);
	        
	    }	   
	    update lOppsToUpdate;
	}

}