@isTest (SeeAllData=true)
private class ProRateControllerTest {
	
	 
     static testMethod void setupData() {
        
        
       Account a = new Account();
       a.Name = 'Test Account';
       a.Industry = 'Test';
       insert a;

       Contact ct = new Contact();
       ct.AccountId = a.Id;
       ct.FirstName = 'Test';
       ct.LastName = 'Contact';
       insert ct;
       
       System.assertNotEquals(a.Id, null);
     
       Opportunity o = new Opportunity();
       o.Name = 'Test Opp';
       o.AccountId = a.Id;
       o.StageName = 'Purchase';
       o.CloseDate = System.today();
     
       insert o;
    
       System.assertNotEquals(o.Id, null);

       Contract c = new Contract();
       c.Name = 'C1';
       c.filemaker_external_id__c = 'test1234';
       c.AccountId = a.Id;
       insert c;
       
       Sales_Order__c so = new Sales_Order__c();
       so.Name = 'so1';
       so.Contract__c = c.Id;
       so.SAP_Customer__c = 'sap1';
       insert so;   

		
		/*
       String jsonValue = '{"Name": "p1", "IsActive": "true", "Deferred_Revenue_Code__c" : "M5", "filemaker_external_id__c" : "test2345", "Program_Code__c" : "C10", "Product_Type__c" : "MNT"}';

	   Product2 pr = (Product2) JSON.deserialize(jsonValue, Type.forName('Product2'));
	   insert pr;
	   
    
		      
       Product2 pr = new Product2();
       pr.Name = 'p1';
       pr.IsActive = true;
       pr.Deferred_Revenue_Code__c = 'M5';
       pr.filemaker_external_id__c = 'test2345';
       pr.Program_Code__c = 'C10';
       pr.Product_Type__c = 'MNT';
       insert pr;
	  */

       
       //String jsonValue2 = '{"Name": "Standard Price Book", "IsStandard": "true", "IsActive" : "true"}';

	   //Pricebook2 pb2 = (Pricebook2) JSON.deserialize(jsonValue2, Type.forName('Pricebook2'));
	   //insert pb2;
		
		Product2 pr = [select Id, Name, IsActive, Deferred_Revenue_Code__c, Program_Code__c, Product_Type__c from Product2 where Product_Type__c = 'MNT' limit 1];
		
		Pricebook2 pb2 = [Select Id, Name, IsActive From Pricebook2 where IsStandard = true LIMIT 1];
        if (!pb2.isActive) {
          pb2.isActive = true;
          update pb2;
        }
      
      
       PricebookEntry pbe = new PricebookEntry();
       pbe.Product2Id = pr.Id;
       pbe.Pricebook2Id = pb2.Id;
       pbe.UnitPrice = 50.00;
       pbe.IsActive = true;
       upsert pbe;

       Quote q = new Quote();
       q.Name = 'Test Quote - 123456';
       q.License_To_Contact__c = ct.Id;
	   q.OpportunityId = o.Id;
	   q.Pricebook2Id = pb2.Id;
       insert q;

      
       QuoteLineItem qli = new QuoteLineItem();
       qli.QuoteId = q.Id;
       qli.Prorate_Price__c = 100.00;
       qli.UnitPrice = 50.00;
       qli.DPC__c = 'M00';
       qli.Quantity = 10;
       qli.PricebookEntryId = pbe.Id;
       insert qli;

     System.debug('q.Id %%% ' + q.Id);
   
     ProRateController controller = new ProRateController(new ApexPages.StandardController(q));
     controller.loadDateIntoQuote();
     
     controller = new ProRateController(new ApexPages.StandardController(q));
     controller.selectedQLI = qli.Id;
     controller.deleteSelectedQLI();
     
     controller.recalculate();
     
     Integer diff = controller.fiscalPeriodDifference(System.today(), System.today() + 10);
	 System.assertNotEquals(diff, null);
	 
     Integer daysBetween = ProRateController.daysBetween(System.today(), System.today() + 10);
	 System.assertNotEquals(daysBetween, null);
	 
	 System.assertNotEquals(controller.getPeriod('10/10/2013'), null);
	 System.assertNotEquals(controller.getQuarter('10/10/2013'), null);
	 System.assertNotEquals(controller.getYear('10/10/2013'), null);

   }

    

 
 static testMethod void setupBasicData() {
   Account a = new Account();
   a.Name = 'Test Account';
   a.Industry = 'Test';
   insert a;
   
   System.assertNotEquals(a.Id, null);
 
   Opportunity o = new Opportunity();
   o.Name = 'Test Opp';
   o.AccountId = a.Id;
   o.StageName = 'Purchase';
   o.CloseDate = System.today();
 
   insert o;

   System.assertNotEquals(o.Id, null);

   Quote q = new Quote();
   q.Name = 'Test Quote';
   q.OpportunityId = o.Id;
   insert q;
   
   System.assertNotEquals(q.Id, null);
 }
 
   static testMethod void constructorTest() {
     setupBasicData();
     
     Quote aQuote = [select Id, Name from Quote limit 1];
   
     ProRateController controller = new ProRateController(new ApexPages.StandardController(aQuote));
     controller.loadDateIntoQuote();
     controller.cancel();
   } 
 

	 
}