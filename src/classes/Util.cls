public class Util {
 
  //Static variable to make sure the Quote Trigger runs only once  
  public static Boolean QUOTE_TRIGGER_FIRED = false;

  public static Boolean contains(List<String> productCodes, String productCode) {
    for(String aProductCode : productCodes) {
        if(aProductCode.equals(productCode)) {
            return true;
        }
    }
    return false;
  }

  public static Integer getPeriod(List<Period> periods, List<FiscalYearSettings> fySettings, Date aDateObject, String aType) {
    if(periods != null && periods.size() > 0) {
        for(Period p : periods) {
            if(p.StartDate <= aDateObject && p.EndDate >= aDateObject && p.Type == aType) {
                if(aType == 'Month' || aType == 'Quarter') {
                    return p.Number;
                } else if(aType == 'Year'){
                    for(FiscalYearSettings fySetting : fySettings) {
                        if(fySetting.PeriodId == p.Id) {
                            return Integer.valueOf(fySetting.Name);
                        }
                    }
                }
            }
        }
    }
    
    return -1000;
  }
  
  public static String getDpc(QuoteLineItem qli, Integer pc, Decimal perUnitPrice) {
         String dpc = '';
        
         Integer mntPercent = 0;
         String drcCode = null;
         
         //If prorate price available and sales price not equal to prorate price
         if(qli.Prorate_Price__c != null && qli.UnitPrice != qli.Prorate_Price__c) {
            drcCode = qli.DPC__c; // Use the calculated DPC
         } else {
            drcCode = qli.PricebookEntry.Product2.Deferred_Revenue_Code__c; //Else use the dpc on the pricebook entry for the QLI
         }
         
         System.debug('DPC code on Product = ' + drcCode);
         
         if(drcCode != null && drcCode != 'M00') { // If non renewal SKU (not M00)
           if(drcCode.length() == 3 && drcCode.startsWith('M')) { // that starts with M
            mntPercent = Integer.valueOf(drcCode.substring(1, 3)); //maintenance percent = last two digits of the DPC code (same as drc)
           }
         } else {
            mntPercent = 100;
            return '00'; //NOTE: Hemang change
         }
         
           String programCode = null;
           if(qli.Prorate_Price__c != null && qli.UnitPrice != qli.Prorate_Price__c) {
            programCode = qli.PC__c;
           } else {
            programCode = qli.PricebookEntry.Product2.Program_Code__c;
           }
           if(programCode == null || !programCode.startsWith('C') || programCode.length() != 3) {
             return '-Error';
           }
           
           Integer programCodeFromProduct = -1000;
           
           try {
               programCodeFromProduct = Integer.valueOf(programCode.substring(1));
           } catch(Exception ex) {
               System.debug(ex.getMessage() + ' ' +  ex.getStackTraceString());
           }
           
           System.debug('programCodeFromProduct = ' + programCodeFromProduct);
         
         Decimal calculatedMaintenancePerMonth = (qli.ListPrice * mntPercent) / programCodeFromProduct;
         calculatedMaintenancePerMonth  = calculatedMaintenancePerMonth.divide(1, 5, System.RoundingMode.HALF_UP);
         
          System.debug('pc = ' + pc);
         Decimal newMaintenancePortion = 0.0;
         if(qli.Prorate_Price__c != null && qli.UnitPrice != qli.Prorate_Price__c) {
            newMaintenancePortion = calculatedMaintenancePerMonth * programCodeFromProduct;
         } else {
            newMaintenancePortion = calculatedMaintenancePerMonth * pc;
         }
         
         Integer discountedMaintenancePortion = 0;
         
         if(perUnitPrice != 0) {
            if((qli.Prorate_Price__c != null && qli.UnitPrice != qli.Prorate_Price__c) || 
                    (qli.Prorate_Price__c == null && qli.UnitPrice != qli.ListPrice) ) {
             discountedMaintenancePortion = (Integer) (newMaintenancePortion / qli.UnitPrice);  
            } else {
             discountedMaintenancePortion = (Integer) (newMaintenancePortion / perUnitPrice);
            }
         } 
         
         System.debug('perUnitPrice = ' + perUnitPrice);
         System.debug('discountedMaintenancePortion = ' + discountedMaintenancePortion);
         
          if(discountedMaintenancePortion >= 100 || discountedMaintenancePortion == 0) {
            return '00';
          }
          return String.valueOf(discountedMaintenancePortion);
  }
  
  public static Decimal getPerUnitPrice(QuoteLineItem qli, String newContractEffectiveDate, String customerOrderDate, String newContractEndDate, Integer lapsePeriod) {
      if(lapsePeriod > 0 && (qli.PricebookEntry.Product2.Description != null && qli.PricebookEntry.Product2.Description.contains('EXP')) 
               && qli.PricebookEntry.Product2.Version__c == 'RMNT') {  
             return qli.PricebookEntry.UnitPrice;
      }

      
       Date newContractEffectiveDateObject = Date.parse(newContractEffectiveDate);
       Date customerOrderDateObject = Date.parse(customerOrderDate);
       
       System.debug('newContractEffectiveDateObject = ' + newContractEffectiveDateObject);
       System.debug('customerOrderDateObject = ' + customerOrderDateObject);
       
       /*
       if(newContractEffectiveDateObject == customerOrderDateObject) {
        return qli.ListPrice;
       }
       */
       
       
       String programCode = qli.PricebookEntry.Product2.Program_Code__c.substring(1);
       Integer programCodeFromProduct = -1;
       
       try {
           programCodeFromProduct = Integer.valueOf(programCode);
       } catch(Exception ex) {
           System.debug(ex.getMessage() + ' ' +  ex.getStackTraceString());
       }
       
       if(programCodeFromProduct <= 0) {
        return -1000;
       }
       
       Integer numberOfDays = getNumberOfDays(newContractEffectiveDate, newContractEndDate);
       
       Decimal dailyPrice = qli.ListPrice * 12 / (365 * programCodeFromProduct); //NOTE: Hemang change
       Decimal price = dailyPrice * numberOfDays;
       
       System.debug('price = ' + price);
       return price.divide(1, 2, System.RoundingMode.HALF_UP); 
  }
  
  public static Integer getNumberOfDays(String newContractEffectiveDate, String newContractEndDate) {
       Date newContractEffectiveDateObject = Date.parse(newContractEffectiveDate);
       Date newContractEndDateObject = Date.parse(newContractEndDate);
       
       Integer daysBetween = ProRateController.daysBetween(newContractEffectiveDateObject, newContractEndDateObject);
       return daysBetween + 1;
  }

    @future(callout=true)
    public static void getTaxes(Set<Id> quoteLineItemIds) {
    	getTaxesSync(quoteLineItemIds);
    }
    
    public static void getTaxesSync(Set<Id> quoteLineItemIds) {
        Org_Settings__c orgSettings = Org_Settings__c.getInstance(UserInfo.getOrganizationId());
        
        List<QuoteLineItem> quoteLineItems =
            [select Id, Quote.ShippingCity, Quote.ShippingStateCode, Quote.ShippingPostalCode,
                    Quote.ShippingCountryCode, PricebookEntry.ProductCode, PricebookEntry.Product2.US_Taxware_Code__c, TotalPrice
             from QuoteLineItem
             where Id in :quoteLineItemIds];

        String xml = '';
        xml +=  '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">';
        xml +=      '<soapenv:Header>';
        xml +=          '<wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" soapenv:mustUnderstand="1">';
        xml +=              '<wsu:Timestamp wsu:Id="TS-2">';
        xml +=                  '<wsu:Created>' + DateTime.now().formatGmt('YYYY-MM-dd\'T\'HH:mm:ss.SSS\'Z\'') + '</wsu:Created>';
        xml +=                  '<wsu:Expires>' + DateTime.now().addMinutes(5).formatGmt('YYYY-MM-dd\'T\'HH:mm:ss.SSS\'Z\'') + '</wsu:Expires>';
        xml +=              '</wsu:Timestamp>';
        xml +=              '<wsse:UsernameToken wsu:Id="UsernameToken-1">';
        xml +=                  '<wsse:Username>' + orgSettings.Tax_Service_Username__c + '</wsse:Username>';
        xml +=                  '<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">' + orgSettings.Tax_Service_Password__c + '</wsse:Password>';
        xml +=              '</wsse:UsernameToken>';
        xml +=          '</wsse:Security>';
        xml +=      '</soapenv:Header>';
        xml +=      '<soapenv:Body>';
        xml +=          '<ns1:FMI_GET_EXTIMATED_TAX xmlns:ns1="http://tempuri.org/">';
        xml +=              '<I_LINE>';
        
        for (QuoteLineItem quoteLineItem : quoteLineItems) {
            xml +=                  '<item>';
            xml +=                      '<MATERIAL>' + quoteLineItem.PricebookEntry.ProductCode + '</MATERIAL>';
            xml +=                      '<PRODUCT_CODE>' + quoteLineItem.PricebookEntry.Product2.US_Taxware_Code__c + '</PRODUCT_CODE>';
            xml +=                      '<EXTENDED_AMOUNT>' + (quoteLineItem.TotalPrice * 100).intValue() + '</EXTENDED_AMOUNT>';
            xml +=                      '<COUNTRY>' + quoteLineItem.Quote.ShippingCountryCode + '</COUNTRY>';
            xml +=                      '<STATE>' + quoteLineItem.Quote.ShippingStateCode + '</STATE>';
            xml +=                      '<CITY>' + quoteLineItem.Quote.ShippingCity + '</CITY>';
            xml +=                      '<ZIPCODE>' + quoteLineItem.Quote.ShippingPostalCode.substring(0, 5) + '</ZIPCODE>';

            if (quoteLineItem.Quote.ShippingPostalCode.length() > 5) {
                xml +=                  '<ZIPCODE_EXTN>' + quoteLineItem.Quote.ShippingPostalCode.substring(6) + '</ZIPCODE_EXTN>';
            }

            xml +=                  '</item>';
        }

        xml +=              '</I_LINE>';
        xml +=          '</ns1:FMI_GET_EXTIMATED_TAX>';
        xml +=      '</soapenv:Body>';
        xml +=  '</soapenv:Envelope>';

        System.debug('####### REQ='+xml);

        HttpRequest httpRequest = new HttpRequest();
        if(orgSettings != null) {
	        httpRequest.setEndpoint(orgSettings.Tax_Service_URL__c);
        }
        httpRequest.setTimeout(120000);
        httpRequest.setMethod('POST');
        httpRequest.setHeader('SOAPAction', 'Retrieve');
        httpRequest.setHeader('Content-Type', 'text/xml;charset=UTF-8');
        httpRequest.setBody(xml);

		try {
			Http http = new Http();
			HttpResponse httpResponse = http.send(httpRequest);

			System.debug('####### RES='+httpResponse.getBody());
			System.debug('####### RES STATUS CODE='+httpResponse.getStatusCode());

			if (httpResponse.getStatusCode() == 200) {
				Dom.Document document = httpResponse.getBodyDocument();
				
				List<Dom.XmlNode> eErrorNodes = getElementsByName(document.getRootElement(), 'E_ERROR');
				List<Dom.XmlNode> eErrorItemNodes = getElementsByName(eErrorNodes[0], 'item');

				Set<Integer> errorIndexes = new Set<Integer>();

				for (Dom.XmlNode eErrorItemNode : eErrorItemNodes) {
					Integer itemNumber = Integer.valueOf(getChildElementValue(eErrorItemNode, 'NUMBER', null));
					quoteLineItems[itemNumber - 1].Tax_Error__c = getChildElementValue(eErrorItemNode, 'MESSAGE', null).trim();
					errorIndexes.add(itemNumber - 1);
				}

				List<QuoteLineItem> successfulQuoteLineItems = new List<QuoteLineItem>();
				
				for (Integer i = 0; i < quoteLineItems.size(); i++) {
					if (!errorIndexes.contains(i)) {
						successfulQuoteLineItems.add(quoteLineItems[i]);
					}
				}

				System.debug('####### successfulQuoteLineItems='+successfulQuoteLineItems);

				List<Dom.XmlNode> eLineNodes = getElementsByName(document.getRootElement(), 'E_LINE');
				List<Dom.XmlNode> eLineItemNodes = getElementsByName(eLineNodes[0], 'item');
		
				for (Integer i = 0; i < successfulQuoteLineItems.size(); i++) {
					successfulQuoteLineItems[i].Tax__c = Decimal.valueOf(getChildElementValue(eLineItemNodes[i], 'TAX', null)) / 100;
				}
			} else {
				String error = 'Error contacting web service - response code: ' + httpResponse.getStatusCode();
				
				for (QuoteLineItem quoteLineItem : quoteLineItems) {
					quoteLineItem.Tax_Error__c = error;
				}
				
				sendEmail(new List<String>{'jim_bricker@filemaker.com'}, error, error);
			}
		} catch (Exception e) {
			for (QuoteLineItem quoteLineItem : quoteLineItems) {
				quoteLineItem.Tax_Error__c = e.getMessage();
			}
		}
		
		for (QuoteLineItem quoteLineItem : quoteLineItems) {
			quoteLineItem.Calculating_Tax__c = false;
		}
		
		update quoteLineItems;
    }

    private static List<Dom.XmlNode> getElementsByName(Dom.XmlNode element, String name) {  
        List<Dom.XmlNode> elements = new List<Dom.XmlNode>();

        if (name == element.getName()) {
            elements.add(element);
        }

        for (Dom.XmlNode childElement : element.getChildElements()) { 
            elements.addAll(getElementsByName(childElement, name));
        }

        return elements;
    }
    
    public static String getChildElementValue(Dom.XmlNode element, String name, String namespace) {  
        Dom.XmlNode childElement = element.getChildElement(name, namespace);
        if (childElement == null) {
            return null;
        } else {
            return childElement.getText();
        }
    }

	public static void sendEmail(List<String> recipients, String subject, String message){
		Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
		email.setToAddresses(recipients);
		email.setSubject(subject);
		email.setPlainTextBody(message);
		email.setHtmlBody(message);

		Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
	}

}