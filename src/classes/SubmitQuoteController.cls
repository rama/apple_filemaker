public class SubmitQuoteController {

    public Boolean errorPanel { get; set; }
    
    public String errorMessage { get; set; }
    
    public List<Period> periods {
    	get {
    		if(periods == null) {
    			periods = new List<Period>();
    		}
    		
    		return periods;
    	}
    	set;
    }

    public List<FiscalYearSettings> fySettings {
    	get {
    		if(fySettings == null) {
    			fySettings = new List<FiscalYearSettings>();
    		}
    		
    		return fySettings;
    	}
    	set;
    }
        

    private ApexPages.StandardController controller;

    public Quote quote {
     get;
     set;
    }

    public SubmitQuoteController(ApexPages.StandardController controller) {
        this.controller = controller;
        this.quote = [Select Id, Name, Status, Prorated_Maintenance__c, Close_Date__c from Quote where Id= :controller.getRecord().Id];
        this.periods = [Select Number, StartDate, EndDate, Type from Period where Type IN ('Month', 'Quarter', 'Year') order by Type, StartDate];
        this.fySettings = [SELECT PeriodId, Name FROM FiscalYearSettings];
    }

    public pagereference changeStatus() {
      if(this.quote.Prorated_Maintenance__c) {
      	Integer closeDatePeriod = Util.getPeriod(periods, fySettings, this.quote.Close_Date__c, 'Month');
      	Integer closeDateYear = Util.getPeriod(periods, fySettings, this.quote.Close_Date__c, 'Year');

      	Integer todayPeriod = Util.getPeriod(periods, fySettings, System.today(), 'Month');
      	Integer todayYear = Util.getPeriod(periods, fySettings, System.today(), 'Year');

		if((closeDatePeriod != todayPeriod) || (closeDateYear != todayYear)) {
			errorPanel = true;
	        errorMessage = 'A consolidated quote scheduled for a fiscal period other than the current one cannot be submitted to TANK or Store. This page will be redirected to Quote page in 10 seconds.';
	        return null;
		}		      	
      }	
    	
      if(this.quote.Status == 'Send to Customer' || this.quote.Status == 'Exception Authorized' || this.quote.Status == 'eAccept Completed') {
      	try {
	        this.quote.Status = 'Quote Submitted';
	        update quote;
	        return new PageReference('/' + quote.Id);
      	} catch(Exception ex) {
	        errorPanel = true;
	        errorMessage = 'Error Occurred while updating quote. Error Message: ' + ex.getMessage() + '. This page will be redirected to Quote page in 10 seconds.';
	        return null;    
      	}
      } else {
        errorPanel = true;
        errorMessage = 'You are only allowed to submit a quote that is ready to be Sent to Customer. This page will be automatically redirected to Quote page in 10 seconds.';
        return null;    
      }
      
    }
    
    public PageReference cancel() {
      return new PageReference('/' + quote.Id);
    }
}