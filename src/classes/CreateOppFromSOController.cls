public with sharing class CreateOppFromSOController {
    
    public string ErrorMessage{get;set;}
    
    public string SoId{get;set;}
    
    public string OppId{get;set;}
    
    public PageReference initPage() {
        string salesOrderId = ApexPages.currentPage().getParameters().get('soId');
        SoId = salesOrderId;
        if(!String.IsBlank(soId)) {
        	try {
	        	Sales_Order__c so = [SELECT Id, Account__c,Account__r.Name, Opportunity__c,Channel__c,CurrencyIsoCode, Price_Book__c,Owner.IsActive, Price_Book__r.Name, Invoice_Date__c, Name FROM Sales_Order__c WHERE Order_Type__c != 'MS' and Id=: salesOrderId limit 1];
	     		if(so == null) { 
	     			ErrorMessage = 'No Sales order can be found with the given Id';
	     			return null; 
	     		}
	     		else if(so.Owner.IsActive == false) {
	     			ErrorMessage = 'Please change the Sales Order Owner to an Active User.';
	     			return null;
	     		}
	     		else { 
	     			List<Sales_Order_Line__c> sols = [select Id, Product_ID_SKU__c, Quantity__c, Sales_Price__c, filemaker_external_id__c, Sales_Order__c, Owner__r.IsActive, Product_ID_SKU__r.Name, CurrencyIsoCode  from Sales_Order_Line__c where Sales_Order__c =:salesOrderId];
	   	   			List<PricebookEntry> pbes = [select Id, Product2Id, Pricebook2Id from PricebookEntry where Pricebook2Id =:so.Price_Book__c and isactive = true];
	   		        Map<Id,String> solsToPbesMap = new Map<Id,String>();
	   		        if(sols != null && sols.size() > 0) {
	   	   				for(Sales_Order_Line__c sol : sols ) {
	   	   					string pbeId;
			   				for(PricebookEntry pbe : pbes) {   					
			   					if(pbe.Pricebook2Id == so.Price_Book__c && pbe.Product2Id == sol.Product_ID_SKU__c ) {
			   		      			pbeId = pbe.Id;
			   		      			solsToPbesMap.put(sol.Id,pbeId);
			   						break;
			   					}
			   				}
	   	   					if(sol.Owner__r.IsActive == false) {
	   	   						ErrorMessage = 'Please change the Sales Order Line Owner(s) to an Active User.';
	   	   						return null;
	   	   					}
	   	   					else if(sol.Quantity__c == null || sol.Quantity__c == 0) {
	   	   						ErrorMessage = 'Cannot update Opportunity Products when Sales Order Quantity is 0 or null.';
	   	   						return null;
	   	   					}
	   	   					else if(sol.Sales_Price__c == null || sol.Sales_Price__c == 0) {
	   	   						ErrorMessage = 'Cannot Create Opportunity Products without sales price';
	   	   						return null;
	   	   					}   	   					
			   				else if(String.IsBlank(pbeId)) {
			   					ErrorMessage = 'The ' + sol.Product_ID_SKU__r.Name + ' Product does not have an entry in the ' + so.Price_Book__r.Name +'.';
			   					return null;
			   				}
	   	   				}
	   	   				CreateOppandLineItems(so,sols, solsToPbesMap);
	   	   			}
	   	   			else {
	   	   				// No SOLs for this SO. Just create an opportunity
	   	   				CreateOppandLineItems(so,null,null);
	   	   			}   	   			
	   	   			return null;
	   	   		}
        	}
        	catch(Exception ex) {
        		ErrorMessage = 'Exception occured. Message: ' + ex.getMessage();
        		return null;
        	}	
    	}
        else {
        	ErrorMessage = 'No Sales Order Id is sent';
        	return null;
        }
    }
    
    private void CreateOppandLineItems(Sales_Order__c so, List<Sales_Order_Line__c> sols, Map<Id,String> solsToPbesMap) {
    	
    	Opportunity opp = new Opportunity();
    	opp.AccountId = so.Account__c;
    	opp.Channel__c = so.Channel__c;
    	opp.CurrencyIsoCode = so.CurrencyIsoCode;
    	opp.Sales_Order__c = so.Id;
    	if(so.Invoice_Date__c == null) {
    		opp.CloseDate = date.Today();
    	}
    	else {
    		opp.CloseDate = so.Invoice_Date__c;
    	}
    	opp.StageName = 'Closed Won';
    	opp.Reconciled__c = true;
    	opp.Name = so.Account__r.Name + '-' + so.Name;
    	opp.OwnerId = so.OwnerId;
    	insert opp;
    	
    	so.Opportunity__c = opp.Id;
    	so.Status__c = 'Reconciled';
    	update so;
    	
    	OppId = opp.Id;
    
    	if(sols != null && solsToPbesMap != null) {
    		
    		for(Sales_Order_Line__c sol : sols) {
    			OpportunityLineItem oli = new OpportunityLineItem();		   								
		   		string pbeId = solsToPbesMap.get(sol.Id); 
		   		oli.PricebookEntryId = pbeId;
		   		oli.Quantity = sol.Quantity__c;
		   		oli.UnitPrice = sol.Sales_Price__c; 
		   		oli.OpportunityId = opp.Id;
		   		oli.filemaker_external_id__c = sol.filemaker_external_id__c; 
		   		oli.Sales_Order_Line__c = sol.Id;
		   		oli.Reconciled__c = true;
		   		insert oli;
		   		
		   		sol.Opportunity_Line_Item_ID__c = oli.Id;
		   		sol.Reconciled__c = true;
    		}
    		
    		update sols;
    	}    	
    }
}