@isTest
global class FileMakerSOAPRequestMock implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        System.assertEquals('https://esttaxdev.filemaker.com/axis2/services/FileMakerEstimatedTaxService', req.getEndpoint());
        System.assertEquals('POST', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type','text/xml;charset=UTF-8');
        res.setBody('<soapenv:Body></soapenv:Body>');
        res.setStatusCode(200);
        return res;
    }
}