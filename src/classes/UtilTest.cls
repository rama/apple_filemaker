@isTest (seeAllData=true)
private class UtilTest {

    static testMethod void containsTest() {
        List<String> productCodes = new List<String>();
        productCodes.add('ABC');
        productCodes.add('XYZ');
        
        System.assertEquals(Util.contains(productCodes, 'ABC'), true);
        System.assertEquals(Util.contains(productCodes, 'ABCD'), false);
    }

	static testMethod void periodTest() {
        List<Period> periods = [Select Number, StartDate, EndDate, Type from Period where Type IN ('Month', 'Quarter', 'Year') order by Type, StartDate];
		List<FiscalYearSettings> fySettings = [SELECT PeriodId, Name FROM FiscalYearSettings];
		
		Integer period = Util.getPeriod(periods, fySettings, System.today(), 'Month');
		System.assertNotEquals(period, null);
		
		period = Util.getPeriod(periods, fySettings, System.today(), 'Year');
		System.assertNotEquals(period, null);		
	}

	static testMethod void getNumberOfDaysTest() {
		Integer numberOfDays = Util.getNumberOfDays('10/10/2013', '10/15/2013');
		System.assertNotEquals(numberOfDays, null);
	}

     static testMethod void setupData() {
        
        
       Account a = new Account();
       a.Name = 'Test Account';
       a.Industry = 'Test';
       insert a;

       Contact ct = new Contact();
       ct.AccountId = a.Id;
       ct.FirstName = 'Test';
       ct.LastName = 'Contact';
       insert ct;
       
       System.assertNotEquals(a.Id, null);
     
       Opportunity o = new Opportunity();
       o.Name = 'Test Opp';
       o.AccountId = a.Id;
       o.StageName = 'Purchase';
       o.CloseDate = System.today();
     
       insert o;
    
       System.assertNotEquals(o.Id, null);

       Contract c = new Contract();
       c.Name = 'C1';
       c.filemaker_external_id__c = 'test1234';
       c.AccountId = a.Id;
       insert c;
       
       Sales_Order__c so = new Sales_Order__c();
       so.Name = 'so1';
       so.Contract__c = c.Id;
       so.SAP_Customer__c = 'sap1';
       insert so;   

    
      
       Product2 pr = new Product2();
       pr.Name = 'p1';
       pr.IsActive = true;
       pr.Deferred_Revenue_Code__c = 'M5';
       pr.filemaker_external_id__c = 'test2345';
       pr.Program_Code__c = 'C10';
       insert pr;
       
       //String jsonValue = '{"Name": "Standard Price Book", "IsStandard": "true", "IsActive" : "true"}';

	   //Pricebook2 pb2 = (Pricebook2) JSON.deserialize(jsonValue, Type.forName('Pricebook2'));
	   //insert pb2;

		Pricebook2 pb2 = [Select Id, Name, IsActive From Pricebook2 where IsStandard = true LIMIT 1];
        if (!pb2.isActive) {
            pb2.isActive = true;
            update pb2;
        }
      
       PricebookEntry pbe = new PricebookEntry();
       pbe.Product2Id = pr.Id;
       pbe.Pricebook2Id = pb2.Id;
       pbe.UnitPrice = 50.00;
       pbe.IsActive = true;
       insert pbe;

       Quote q = new Quote();
       q.Name = 'Test Quote';
       q.License_To_Contact__c = ct.Id;
	   q.OpportunityId = o.Id;
	   q.Pricebook2Id = pb2.Id;
       insert q;

      
       QuoteLineItem qli = new QuoteLineItem();
       qli.QuoteId = q.Id;
       qli.Prorate_Price__c = 100.00;
       qli.UnitPrice = 50.00;
       qli.DPC__c = 'M00';
       qli.Quantity = 10;
       qli.PricebookEntryId = pbe.Id;
       insert qli;
     }

	static testMethod void drcTest() {
		setupData();
		
		QuoteLineItem qli = [select Id, ListPrice, PricebookEntry.Product2.Deferred_Revenue_Code__c,  PricebookEntry.Product2.Program_Code__c, QuoteId, Prorate_Price__c, UnitPrice, DPC__c from QuoteLineItem limit 1];
		String dpc = Util.getDpc(qli, 10, 25.00);
		System.assertEquals(dpc, '00');
	}

	static testMethod void perUnitPriceTest() {
		setupData();
		
		QuoteLineItem qli = [Select Id, Prorate_Price__c, Quantity, MAS_Contract_End_Date__c, MAS_Contract_Number__c, MAS_Maintenance_Contract_Number__c, PricebookEntry.Product2.Description,  
                                                         PricebookEntry.UnitPrice, PricebookEntry.Product2.Program_Code__c, PricebookEntry.Product2.Name, PricebookEntry.Product2.Filemaker_External_ID__c, 
                                                         PricebookEntry.Product2.Deferred_Revenue_Code__c, ListPrice, UnitPrice, TotalPrice, PC__c, DPC__c  
                                                         from QuoteLineItem limit 1];
		Decimal pup = Util.getPerUnitPrice(qli, '10/10/2013', '10/08/2013', '10/12/2013', 5);
		System.assertNotEquals(pup, null);
	}

	@isTest(SeeAllData=true)
	static void getTaxesTest() {
		Opportunity opportunity = new Opportunity();
		opportunity.Name = 'Test';
		opportunity.StageName = 'Prospecting';
		opportunity.CloseDate = Date.today().addDays(1);
		insert opportunity;

		PricebookEntry pricebookEntry = [select Id, Pricebook2Id from PricebookEntry where IsActive = true and CurrencyISOCode = 'USD' limit 1];
		
		Quote quote = new Quote();
		quote.OpportunityId = opportunity.Id;
		quote.Pricebook2Id = pricebookEntry.Pricebook2Id;
		quote.Name = 'Test';
		quote.ShippingCity = 'Atlanta';
		quote.ShippingStateCode = 'GA';
		quote.ShippingCountryCode = 'US';
		quote.ShippingPostalCode = '30342-1234';
		insert quote;
		
		QuoteLineItem quoteLineItem = new QuoteLineItem();
		quoteLineItem.QuoteId = quote.Id;
		quoteLineItem.PricebookEntryId = pricebookEntry.Id;
		quoteLineItem.Quantity = 1;
		quoteLineItem.UnitPrice = 5;
		
		QuoteLineItem quoteLineItem2 = new QuoteLineItem();
		quoteLineItem2.QuoteId = quote.Id;
		quoteLineItem2.PricebookEntryId = pricebookEntry.Id;
		quoteLineItem2.Quantity = 1;
		quoteLineItem2.UnitPrice = 5;

		insert new List<QuoteLineItem>{quoteLineItem, quoteLineItem2};
		
		StaticResourceCalloutMock staticResourceCalloutMock = new StaticResourceCalloutMock();
		staticResourceCalloutMock.setStaticResource('TaxServiceTestResponse');
		staticResourceCalloutMock.setStatusCode(200);
		
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, staticResourceCalloutMock);
		
		Util.getTaxes(new Set<Id>{quoteLineItem.Id, quoteLineItem2.Id});
		
		Test.stopTest();
		
		quoteLineItem = [select Calculating_Tax__c, Tax__c, Tax_Error__c from QuoteLineItem where Id = :quoteLineItem.Id];
		quoteLineItem2 = [select Calculating_Tax__c, Tax__c, Tax_Error__c from QuoteLineItem where Id = :quoteLineItem2.Id];
		
		System.assertEquals(false, quoteLineItem.Calculating_Tax__c);
		System.assertEquals(5.23, quoteLineItem.Tax__c);
		System.assertEquals(null, quoteLineItem.Tax_Error__c);
		
		System.assertEquals(false, quoteLineItem2.Calculating_Tax__c);
		System.assertEquals(null, quoteLineItem2.Tax__c);
		System.assertEquals('Error Message', quoteLineItem2.Tax_Error__c);
	}

}