@isTest
private class FileMakerSOAPRequestTest {

    static testMethod void testSOAPRequest() {
      Test.setMock(HttpCalloutMock.class, new FileMakerSOAPRequestMock());
    
      String response = FileMakerSOAPRequest.callFileMakerService();
      System.assertNotEquals(response, null);
    }
}