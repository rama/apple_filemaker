@isTest
private class SalesOrderTriggerTest {

    @isTest(seeAllData = true)
    static void setupAndTest() {
        
        Account acc = new Account();
        acc.CurrencyIsoCode = 'USD';
        acc.Name = 'test account';
        acc.Industry = 'OTHR Other';
        acc.District__c = 'LATAM';
        insert acc;
        
        Opportunity opp1 = new Opportunity();
        opp1.AccountId = acc.Id;
        opp1.Name = 'first test opp';
        opp1.CloseDate = date.today();
        opp1.StageName = 'Evaluate';
        opp1.CurrencyIsoCode = 'USD';
        insert opp1;
        
        Quote q1 = new Quote();
        q1.OpportunityId = opp1.Id;
        q1.Name = 'first test quote';
        insert q1;
        
              
        Opportunity opp2 = new Opportunity();
        opp2.AccountId = acc.Id;
        opp2.Name = 'second test opp';
        opp2.CloseDate = date.today();
        opp2.StageName = 'Evaluate';
        opp2.CurrencyIsoCode = 'USD';
        insert opp2;
        
        Quote q2 = new Quote();
        q2.OpportunityId = opp2.Id;
        q2.Name = 'second test quote';
        insert q2;
        
        //create a sales order with first test quote object created
        Sales_Order__c so = new Sales_Order__c();
        so.Name = 'test SO';
        so.SAP_Customer__c = 'test customer';
        so.CurrencyIsoCode = 'BRL';
        so.Opportunity__c = opp1.Id;
        so.Channel__c = false;
        so.Quote__c = q1.Id;
        insert so;        
        
        System.assertEquals(1,Limits.getFutureCalls());
        
        Sales_Order__c requeriedso = [select Id, Account__c, Price_Book__c, Opportunity__c, SAP_Contract__c from Sales_Order__c where Id =: so.Id];
        //verify that the opportunity is set to first test opportunity's ID
        system.assertEquals(opp1.Id, requeriedso.Opportunity__c,'original opportunity ID should be matched');
        //verify that the account details from quote are populated
        system.assertEquals(acc.Id, requeriedso.Account__c, 'account ID from quote must match');
        
        List<Pricebook2> pbes = [select Id from Pricebook2 where Pricing_Type__c = 'Retail' and Currency__c =: so.CurrencyIsoCode];
        
        if(pbes != null && pbes.size() > 0) {
	        system.assertEquals(pbes[0].Id,requeriedso.Price_Book__c);
        }
        
        //Change the quote on the sales order to be second test quote
        so.Quote__c = q2.Id;
        update so;
        
        //Requery Sales order object to get the updated info
        List<Sales_Order__c> queriedSos = [select Id, Opportunity__c from Sales_Order__c where Id =: so.Id];
               
        //Verify that the opportunity on the sales order object is now changed to second test opportuity
        system.assertEquals(opp2.Id, queriedSos[0].Opportunity__c,'opportunity IDs must match');
        
    }
}